<?php

require_once 'Auditing.php';
require_once 'Period.php';
require_once 'OurConstant.php';
require_once 'Employee.php';

class Attendance 
{

    public $id;
    public $attendanceTimeIn;
    public $registerDate;
    public $attendanceDate;
    public $attendanceTime;
    public $idPeriod;
    public $idEmployee;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create attendance
    function registerAttendance()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO attendance VALUES(?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->attendanceTimeIn);
        $prep->bindparam(3, $this->attendanceTimeOut);
        $prep->bindparam(4, $this->registerDate);
        $prep->bindparam(5, $this->attendanceDate);
        $prep->bindparam(6, $this->idPeriod);
        $prep->bindparam(7, $this->idEmployee);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Comparecimento', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all attendance
    function readAttendance() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM attendance";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['attendance_time_in'] = $reg->attendance_time_in;
				$arrayData[$i]['attendance_time_out'] = $reg->attendance_time_out;
                $arrayData[$i]['attendance_date'] = $reg->attendance_date;
                //Instanciate the period class
                $period = new Period($this->dbh);
                $arrayData[$i]['period'] = $period->getDataPeriod($reg->id_period);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined attendance
    function readDeterminedAttendance() 
    {
        $arrayData = [];
        $cons = "SELECT * FROM attendance WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['attendance_time_in'] = $reg->attendance_time_in;
                $arrayData['attendance_time_out'] = $reg->attendance_time_out;
                $arrayData['attendance_date'] = $reg->attendance_date;
                //Instanciate the period class
                $period = new Period($this->dbh);
                $arrayData['period'] = $period->getDataPeriod($reg->id_period);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update attendance
    function updateAttendance() 
    {
        $arrayData = [];
        $cons = "UPDATE attendance SET attendance_time_in = ?,attendance_time_out = ?, attendance_date= ?, id_period = ?, id_employee = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->attendanceTimeIn);
        $prep->bindparam(2, $this->attendanceTimeOut);
        $prep->bindparam(3, $this->attendanceDate);
        $prep->bindparam(4, $this->idPeriod);
        $prep->bindparam(5, $this->idEmployee);
        $prep->bindparam(6, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Comparecimento', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete attendance
    function deleteAttendance() 
    {
        $arrayData = [];
        $cons = "DELETE FROM attendance WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Comparecimento', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// Get attendance of an employee in a determined interval
    function getAttendanceEmployee() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT employee.other_id,employee.employee_code,employee.full_name,attendance.register_date,attendance.attendance_movement,attendance.attendance_date,
				attendance.attendance_time,period.designation AS desi_period,period_type.designation AS desi_period_type,department.acronym AS acronym FROM attendance 
				JOIN period ON attendance.id_period = period.id
				JOIN period_type ON period.id_period_type = period_type.id
				JOIN employee ON attendance.id_employee = employee.id
				JOIN department ON employee.id_department = department.id
				WHERE attendance.attendance_date >= ?
				AND attendance.attendance_date <= ?
				AND attendance.id_employee = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->attendanceDate[0], PDO::PARAM_STR);
		$prep->bindparam(2, $this->attendanceDate[1], PDO::PARAM_STR);
		$prep->bindparam(3, $this->idEmployee, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
				$arrayData[$i]['other_id'] = $reg->other_id;
				$arrayData[$i]['employee_code'] = $reg->employee_code;
				$arrayData[$i]['full_name'] = $reg->full_name;
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['attendance_time_in'] = $reg->attendance_time_in;
                $arrayData[$i]['attendance_time_out'] = $reg->attendance_time_out;
                $arrayData[$i]['attendance_date'] = $reg->attendance_date;
                $arrayData[$i]['desi_period'] = $reg->desi_period;
                $arrayData[$i]['desi_period_type'] = $reg->desi_period_type;
                $arrayData[$i]['acronym'] = $reg->acronym;
                $i++;
            }
			$t = 'Assiduidade'.time().'.csv';
			$filePath = OurConstant::csvFiles();
			$fp = fopen($filePath.$t, 'w');
			$headerArray = array("ID","Código","Nome","Data de registo","Hora Entrada","Hora Saida","Data","Periodo","Tipo periodo","Departamento");
            // Write header of csv data form
			fputcsv($fp,$headerArray);
			// Loop through file pointer and a line
			foreach ($arrayData as $fields) {
					fputcsv($fp, $fields);
			}
			// convert csv file to excel
			$filePathProduction = OurConstant::csvFilesProduction();
			$csvFilePath = $filePath.$t;
			$csvFilePathProduction = $filePathProduction.$t;
			$arrayDataCsv['file_path_excel'] = $this->convertCsvToExcel($csvFilePath,'Assiduidade');
			$arrayDataCsv['file_path_csv'] = $csvFilePathProduction;
			return $arrayDataCsv;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// convert csv file to excel
	function convertCsvToExcel($csvFilePath,$title)
	{
		require_once("../vendor/autoload.php");
		$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Csv');
		
		// If the files uses a delimiter other than a comma (e.g. a tab), then tell the reader
		// $reader->setDelimiter("\t");
		// If the files uses an encoding other than UTF-8 or ASCII, then tell the reader
		// $reader->setInputEncoding('UTF-16LE');
		
		setlocale(LC_NUMERIC, 'en_US');
		$objPHPExcel = $reader->load($csvFilePath);
		//$objPHPExcel->getActiveSheet()->getStyle('A1:A10')->getNumberFormat()->setFormatCode('@');
		//setlocale(LC_ALL, 'pt_BR');
		
		$objWriter = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($objPHPExcel, 'Xlsx');
		$t = $title.time().'.xlsx';
		$filePath = OurConstant::excelFiles();
		$objWriter->save($filePath.$t);
		$filePathProduction = OurConstant::excelFilesProduction();
		$fileConverted = $filePathProduction.$t;
		return $fileConverted;
	}
	
	// Upload attendance file
	function uploadExcelAttendanceFile($fileUploaded)
	{
		//Instanciate the employee class
        $employee = new Employee($this->dbh);
        $period = new Period($this->dbh);
		
		require_once ("../vendor/autoload.php");

		/*$allowedFileType = [
			'application/vnd.ms-excel',
			'text/xls',
			'text/xlsx',
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
		];*/
		
		$Reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
		$spreadSheet = $Reader->load($fileUploaded);
		$excelSheet = $spreadSheet->getActiveSheet();
		$spreadSheetAry = $excelSheet->toArray();
		$sheetCount = count($spreadSheetAry);
		for ($i = 1; $i < $sheetCount; $i++) {
			// Get data employee from other ID
			//$employeeData= $employee->getDataEmployeeFromOtherId($otherId);
			$employeeData = $employee->getDataEmployeeFromOtherId($spreadSheetAry[$i][5]);
			// Get data period from designation
			$designation = 'Noite';
			//$periodData = $period->getDataPeriodFromDesignation($spreadSheet[$i][2]); 
			$periodData = $period->getDataPeriodFromDesignation($designation); 
			$employeeName = $spreadSheetAry[$i][0];
			$this->id = NULL;
			$this->attendanceDate = $spreadSheetAry[$i][1];
			$this->registerDate = date('Y-m-d H:i:s');
			$this->idEmployee = $employeeData['id'];
			$this->idPeriod = $periodData['id'];
			//$this->attendanceTimeIn = $spreadSheetAry[$i][18];
			$this->attendanceTimeIn = '';
			//$this->attendanceTimeOut = $spreadSheetAry[$i][19];
			$this->attendanceTimeOut = '';
			// Create attendance
			$createAttendance = $this->registerAttendance();
		}
		if($createAttendance) return true;
		else return false;
	}

    // Get data attendance
    function getDataAttendance($id) 
    {
        $arrayData = [];
        $cons = "SELECT * FROM attendance WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['attendance_time_in'] = $reg->attendance_time_in;
                $arrayData['attendance_time_out'] = $reg->attendance_time_out;
                $arrayData['attendance_date'] = $reg->attendance_date;
                //Instanciate the period class
                $period = new Period($this->dbh);
                $arrayData['period'] = $period->getDataPeriod($reg->id_period);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT attendance.register_date, attendance.attendance_time_in, attendance.attendance_time_out, period.designation, period.initial, period.end, employee.full_name,
				employee.nationality, employee.gender, employee.employee_code,attendance.attendance_date FROM attendance 
				JOIN period ON attendance.id_period=period.id 
				JOIN employee ON attendance.id_employee=employee.id 
				WHERE attendance.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute(); 
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['attendance_time_in'] = 'Hora Entrada: ' . $reg->attendance_time_in;
                $arrayData['register_date'] = 'Data de registo: ' . $reg->register_date;
                $arrayData['attendance_time_out'] = 'Hora Saida: ' . $reg->attendance_time_out;
                $arrayData['attendance_date'] = 'Data do comprometimento: ' . $reg->attendance_date;
                $arrayData['designation'] = 'Período: ' . $reg->designation;
                $arrayData['initial'] = 'Início: ' . $reg->initial;
                $arrayData['end'] = 'Fim: ' . $reg->end;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>