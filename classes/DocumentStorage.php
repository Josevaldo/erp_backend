<?php
class DocumentStorage
{
	public $objectIdentifier;
	public $fileName;
	public $fileTemporaryName;
	public $rep;
	public $typeDocument;
	public $downloadFilePath;
	public $dbh;
	//public $fileDomainForDownload = 'http://be.intranet.mep.gov.ao/documents';
	public $fileDomainForDownload = 'http://192.168.10.62/documents';

	function __construct($documentPurpose,$objectIdentifier,$dbh)
	{
		$this->objectIdentifier = $objectIdentifier;
		$this->dbh = $dbh;
		switch ($documentPurpose) {
			case 'userPhoto':
				$this->rep = '../documents/userPhoto/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/userPhoto/'.$this->objectIdentifier.'/';
				$this->typeDocument = 'image';
				break;
			case 'employeePhoto':
				$this->rep = '../documents/employeePhoto/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/employeePhoto/'.$this->objectIdentifier.'/';
				$this->typeDocument = 'image';
				break;
			case 'enterpriseLogo':
				$this->rep = '../documents/enterpriseLogo/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/enterpriseLogo/'.$this->objectIdentifier.'/';
				$this->typeDocument = 'image';
				break;
			case 'birthdayPersonImage':
				$this->rep = '../documents/birthdayPersonImage/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/birthdayPersonImage/'.$this->objectIdentifier.'/';
				$this->typeDocument = 'image';
				break;
			case 'employeeArchive':
				$this->rep = '../documents/employeeArchive/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/employeeArchive/'.$this->objectIdentifier.'/';
				$this->typeDocument = '';
				break;
			case 'attendanceReport':
				$this->rep = '../documents/attendanceReport/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/attendanceReport/'.$this->objectIdentifier.'/';
				$this->typeDocument = '';
				break;
			case 'otherDocument':
				$this->rep = '../documents/otherDocument/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/otherDocument/'.$this->objectIdentifier.'/';
				$this->typeDocument = '';
				break;
			case 'eventLogo':
				$this->rep = '../documents/eventLogo/'.$this->objectIdentifier.'/';
				$this->downloadFilePath = $this->fileDomainForDownload.'/eventLogo/'.$this->objectIdentifier.'/';
				$this->typeDocument = 'image';
				break;
			default:
				$this->downloadFilePath = $this->fileDomainForDownload.'/defaultDocument/';
				$this->rep = '../documents/defaultDocument/';
		}
		//return $this->rep;
	}
	
	// store document
	function storeDocument()
	{
		$fileStored = array();
		if($this->typeDocument == 'image') $fileExtension = array('tiff','tif','jpeg','jpg','png','gif','svg');
		else $fileExtension = array('tiff','tif','jpeg','jpg','png','pdf','gif','doc','docx','dot','dotx','xlsx','xls','xlsm','xlsb','xltx','txt','pptx','pptm','ppt','pub','csv');
		$fileBaseName = basename($this->fileName);
		$path = $this->fileName;
		$extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));
		$filePointer = $this->rep;
		if(in_array($extension, $fileExtension)){
			if(file_exists($filePointer)){
				$moveUploadedFile = move_uploaded_file($this->fileTemporaryName,$filePointer.$fileBaseName);
				if($moveUploadedFile) $fileStored = $this->getDocument();
			}else{
				if(!empty($this->objectIdentifier)){
					$createDocumentImage = mkdir($filePointer);
					if($createDocumentImage){
						$moveUploadedFile = move_uploaded_file($this->fileTemporaryName,$filePointer.$fileBaseName);
						if($moveUploadedFile) $fileStored = $this->getDocument();
					}
				}
			}
		}
		return $fileStored;
	}
	
	// store document with default document
	function storeDocumentDefaultDocument()
	{
		//if($this->typeDocument == 'image') $fileExtension = array('tiff','tif','jpeg','jpg','png','gif','svg');
		//else $fileExtension = array('tiff','tif','jpeg','jpg','png','pdf','gif','doc','docx','dot','dotx','xlsx','xls','xlsm','xlsb','xltx','txt','pptx','pptm','ppt','pub','csv');
		if(!file_exists($this->rep)) mkdir($this->rep);
		$filePointer = $this->rep.'defaultDocument.txt';
		//$myfile = fopen($filePointer, "w") or die("Unable to open file!");
		$myfile = fopen($filePointer, "w");
		$txt = "Nehum documento submetido\n";
		fwrite($myfile, $txt);
		$txt1 = "Nehum documento submetido\n";
		fwrite($myfile, $txt1);
		fclose($myfile);
	}
	
	// Alter document 
	function alterDocument()
	{
		$documentAltered = '';
		$i = 0;
		if(file_exists($this->rep)){
			$files = scandir($this->rep);
			foreach($files as $file){
				if(($file != '.') AND ($file != '..')){
					$fName = $this->rep.$file;
					if($fName) unlink($fName);
					// store document
					$documentAltered = $this->storeDocument();
				} 
				$i++;
			}
		}else{
			// Get data before and after the execution of an action
			//$dataBeforeExecution = '';
			// store document
			$documentAltered = $this->storeDocument();
			$i++;
		}
		// Get data before and after the execution of an action
		//$dataAfterExecution = $fileName;
		// instance the class user
		$auditing = new Auditing($this->dbh);
		$response = $auditing->insertDataAuditingFile($this->rep,$this->typeDocument,'','');
		//return $i;
		return $documentAltered;
	}
	
	// Get document
	function getDocument()
	{	
		$i = 0;
		//$list = '';
		$list = array();
		if(file_exists($this->rep)){
			$files = scandir($this->rep);
			foreach($files as $file){
				//if(($file != '.') and ($file != '..')) $list = $this->downloadFilePath.$file;
				if(($file != '.') and ($file != '..')) $list[] = $this->downloadFilePath.$file;
				$i++;
			}
		}else $list = array();
		return $list;
	}
	
	// Get repository path
	function getRepositoryPath()
	{	
		return $this->rep;
	}
	
	// Get download File path
	function getDownloadFilePath()
	{	
		return $this->downloadFilePath;
	}
	
	// remove a specific file
	function removeSpecificFile($fileName)
	{	
		// Get repository path
		$rep = $this->getRepositoryPath();
		$fileToRemove = $rep.$fileName;
		if(file_exists($fileToRemove)) $resp = unlink($fileToRemove);
		else $resp = false;
		return $rep;
	}
	
	// Remove a directory with all kind of contents
	function removeDirectory($dir)
	{ 
		//DIRECTORY_SEPARATOR = '/';
		if (is_dir($dir)) { 
			$objects = scandir($dir);
			foreach ($objects as $object) { 
				if ($object != "." && $object != "..") { 
					if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
						removeDirectory($dir. DIRECTORY_SEPARATOR .$object);
					else
						unlink($dir. DIRECTORY_SEPARATOR .$object); 
				} 
			}
			rmdir($dir); 
		} 
	}
}
?>
