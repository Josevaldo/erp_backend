<?php

require_once 'Auditing.php';
require_once 'Training.php';
require_once 'Employee.php';

class Capacitation 
{

    public $id;
    public $idTraining;
    public $idEmployee;
    public $status;
    public $obs;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create capacitation
    function registerCapacitation() 
    {
        $arrayData = [];
        $cons = "INSERT INTO capacitation VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTraining);
        $prep->bindparam(2, $this->idEmployee);
        $prep->bindparam(3, $this->status);
        $prep->bindparam(4, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idTraining, $this->idEmployee);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Capacitação', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all capacitation
    function readCapacitation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM capacitation";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['obs'] = $reg->obs;
                $arrayData[$i]['status'] = $reg->status;
                // Get data of a spefic training
                $trainingData = new Training($this->dbh);
                $arrayData[$i]['training'] = $trainingData->getDataTraining($reg->id_training);
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employeeData->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined capacitation
    function readDeterminedCapacitation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM capacitation WHERE id_training  = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['obs'] = $reg->obs;
                $arrayData['status'] = $reg->status;
                // Get data of a spefic training
                $trainingData = new Training($this->dbh);
                $arrayData['training'] = $trainingData->getDataTraining($reg->id_training);
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData['employee'] = $employeeData->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update capacitation
    function updateCapacitation() 
    {
        $arrayData = [];
        $cons = "UPDATE capacitation SET id_training = ?, id_employee = ?, obs = ? WHERE id_training = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTraining);
        $prep->bindparam(2, $this->idEmployee);
        $prep->bindparam(3, $this->obs);
        $prep->bindparam(4, $this->id[0]);
        $prep->bindparam(5, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data before and after the execution of an action
            $this->id = array($this->idTraining, $this->idEmployee);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Capacitação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete capacitation
    function deleteCapacitation() 
    {
        $arrayData = [];
        $cons = "DELETE FROM capacitation WHERE id_training = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $this->id = [$this->idTraining, $this->idEmployee];
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Performance
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Capacitação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// Include employee in a determined training
	function includeEmployeeTraining($idEmployeeList)
	{
		$i = 0;
		foreach($idEmployeeList as $v){
			$this->idEmployee = $v;
			$response = $this->registerCapacitation();
			if($response) $i++;
		}
		return $i;
	}
	
	// Update capacitation status
    function updateCapacitationStatus() 
    {
        $arrayData = [];
        $cons = "UPDATE capacitation SET status = ? WHERE id_training = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->status);
        $prep->bindparam(2, $this->id[0]);
        $prep->bindparam(3, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data before and after the execution of an action
            $this->id = array($this->idTraining, $this->idEmployee);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Actualização de estado da Capacitação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get user profile
    function getUserProfile($idUser) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Assigned permitions to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $permition) 
    {
        $i = 0;
        if ((is_array($permition)) || (!empty($permition))) {
            $this->idUser = $idUserCreated;
            $this->obs = "Perfil criado no momento do registo Utilizador";
            foreach ($permition as $rr) {
                $i++;
                $this->idPermition = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data capacitation
    function getDataCapacitation($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM capacitation WHERE id_training  = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['obs'] = $reg->obs;
                $arrayData['status'] = $reg->status;
                // Get data of a spefic training
                $trainingData = new Training($this->dbh);
                $arrayData['training'] = $trainingData->getDataTraining($reg->id_training);
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData['employee'] = $employeeData->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT capacitation.status,capacitation.obs, training.designation, training.initial_date, training.end_date, training.local, training.goal, employee.full_name, employee.nationality, employee.gender, employee.employee_code FROM capacitation JOIN training ON capacitation.id_training=training.id JOIN employee ON capacitation.id_employee= employee.id WHERE capacitation.id_training = ? AND capacitation.id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['obs'] = 'Observação da capacitação: ' . $reg->obs;
                $arrayData['status'] = 'Estado: ' . $reg->status;
                $arrayData['designation'] = 'Formação: ' . $reg->designation;
                $arrayData['initial_date'] = 'Início: ' . $reg->initial_date;
                $arrayData['end_date'] = 'Fim: ' . $reg->end_date;
                $arrayData['local'] = 'Local: ' . $reg->local;
                $arrayData['goal'] = 'Objectivo: ' . $reg->goal;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>