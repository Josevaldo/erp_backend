<?php

require_once 'Auditing.php';
require_once 'ContractType.php';

class Contract 
{

    public $id;
    public $designation;
    public $initialDate;
    public $endDate;
    public $contractCode;
    public $obs;
    public $idContractType;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create contract
    function registerContract()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO contract VALUES(?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->initialDate);
        $prep->bindparam(4, $this->endDate);
        $prep->bindparam(5, $this->contractCode);
        $prep->bindparam(6, $this->obs);
        $prep->bindparam(7, $this->idContractType);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Contracto', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all contract
    function readContract() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM contract";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['initial_date'] = $reg->initial_date;
                $arrayData[$i]['end_date'] = $reg->end_date;
                $arrayData[$i]['contract_code'] = $reg->contract_code;
                $arrayData[$i]['obs'] = $reg->obs;
                //Instanciate the Contract Type
                $contractType = new ContractType($this->dbh);
                $arrayData[$i]['Contract_type'] = $contractType->getDataContractType($reg->id_contract_type);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined contract
    function readDeterminedContract()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM contract WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['contract_code'] = $reg->contract_code;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the Contract Type
                $contractType = new ContractType($this->dbh);
                $arrayData['Contract_type'] = $contractType->getDataContractType($reg->id_contract_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update contract
    function updateContract() 
    {
        $arrayData = [];
        $cons = "UPDATE contract SET designation = ?, initial_date = ?, end_date=?, contract_code = ?, obs = ?, id_contract_type = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->initialDate);
        $prep->bindparam(3, $this->endDate);
        $prep->bindparam(4, $this->contractCode);
        $prep->bindparam(5, $this->obs);
        $prep->bindparam(6, $this->idContractType);
        $prep->bindparam(7, $this->id);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Contracto', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete contract
    function deleteContract() 
    {
        $arrayData = [];
        $cons = "DELETE FROM contract WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Contrato', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data contract
    function getDataContract($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM contract WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['contract_code'] = $reg->contract_code;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the Contract Type
                $contractType = new ContractType($this->dbh);
                $arrayData['Contract_type'] = $contractType->getDataContractType($reg->id_contract_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT contract.designation AS contract_designation, contract.initial_date, contract.end_date, contract.contract_code, contract.obs AS contract_obs, contract_type.designation, contract_type.obs FROM contract JOIN contract_type ON contract.id_contract_type=contract_type.id= contract_type.id WHERE contract.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['contract_designation'] = 'Contracto: ' . $reg->contract_designation;
                $arrayData['initial_date'] = 'Início: ' . $reg->initial_date;
                $arrayData['end_date'] = 'Fim: ' . $reg->end_date;
                $arrayData['contract_code'] = 'Código: ' . $reg->contract_code;
                $arrayData['contract_obs'] = 'Observação: ' . $reg->contract_obs;
                $arrayData['designation'] = 'Tipo: ' . $reg->designation;
                $arrayData['obs'] = 'Observação do tipo: ' . $reg->obs;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>