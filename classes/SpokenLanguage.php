<?php

require_once 'Auditing.php';
require_once 'Employee.php';
require_once 'Language.php';

class SpokenLanguage
{

    public $id;
    public $idEmployee;
    public $idLanguage;
    public $level;
    public $obs;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create spoken_language
    function registerSpokenLanguage() 
    {
        $arrayData = [];
        $cons = "INSERT INTO spoken_language VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idEmployee);
        $prep->bindparam(2, $this->idLanguage);
        $prep->bindparam(3, $this->level);
        $prep->bindparam(4, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idEmployee, $this->idLanguage);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class SpokenLanguage
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Linguagem falada', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all spoken_language
    function readSpokenLanguage()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM spoken_language";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['level'] = $reg->level;
                $arrayData[$i]['obs'] = $reg->obs;
                // Get data of a spefic employee
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                // Get data of a spefic language
                $language = new Language($this->dbh);
                $arrayData[$i]['language'] = $language->getDataLanguage($reg->id_language);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined spoken_language
    function readDeterminedSpokenLanguage()
    {
        $arrayData = [];
        $cons = "SELECT * FROM spoken_language WHERE id_employee = ? AND id_language = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['level'] = $reg->level;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic employee
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
                // Get data of a spefic language
                $language = new Language($this->dbh);
                $arrayData['language'] = $language->getDataLanguage($reg->id_language);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update spoken_language
    function updateSpokenLanguage()
    {
        $arrayData = [];
        $cons = "UPDATE spoken_language SET id_employee = ?,id_language = ?, level=?, obs = ? WHERE id_employee = ? AND id_language = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idEmployee);
        $prep->bindparam(2, $this->idLanguage);
        $prep->bindparam(3, $this->level);
        $prep->bindparam(4, $this->obs);
        $prep->bindparam(5, $this->id[0]);
        $prep->bindparam(6, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            $this->id = array($this->idEmployee, $this->idLanguage);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Linguagem falada', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete spoken_language
    function deleteSpokenLanguage() 
    {
        $arrayData = [];
        $cons = "DELETE FROM spoken_language WHERE id_employee = ? AND id_language = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Linguagem falada', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get user profile
    function getUserProfile($idUser) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Assigned permitions to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $permition) {
        $i = 0;
        if ((is_array($permition)) || (!empty($permition))) {
            $this->idUser = $idUserCreated;
            $this->obs = "Perfil criado no momento do registo Utilizador";
            foreach ($permition as $rr) {
                $i++;
                $this->idPermition = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data spoken_language
    function getDataSpokenLanguage($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM spoken_language	WHERE id_employee = ? AND id_language = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['level'] = $reg->level;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic employee
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
                // Get data of a spefic language
                $language = new Language($this->dbh);
                $arrayData['language'] = $language->getDataLanguage($reg->id_language);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $dataReceivedFormated = '';
        $cons = "SELECT spoken_language.level, spoken_language.obs AS spoken_language_obs, language.designation, language.obs, employee.full_name, employee.nationality, employee.gender, employee.employee_code FROM spoken_language JOIN language ON spoken_language.id_language=language.id JOIN employee ON spoken_language.id_employee=employee.id WHERE spoken_language.id_employee = ? AND spoken_language.id_language = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['level'] = 'Nível: ' . $reg->level;
                $arrayData['spoken_language_obs'] = 'Observação da Linguagem falada: ' . $reg->spoken_language_obs;
                $arrayData['designation'] = 'Linguagem: ' . $reg->designation;
                $arrayData['obs'] = 'Observação da linguagem: ' . $reg->obs;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>