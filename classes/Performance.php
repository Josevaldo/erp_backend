<?php

require_once 'Auditing.php';
require_once 'Assessment.php';
require_once 'Employee.php';

class Performance 
{

    public $id;
    public $idAssessment;
    public $idEmployee;
    public $gradeObtained;
    public $obs;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create performance
    function registerPerformance()
    {
        $arrayData = [];
        $cons = "INSERT INTO performance VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idAssessment);
        $prep->bindparam(2, $this->idEmployee);
        $prep->bindparam(3, $this->gradeObtained);
        $prep->bindparam(4, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idAssessment, $this->idEmployee);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Desempenho', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all performance
    function readPerformance()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM performance";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['grade_obtained'] = $reg->grade_obtained;
                $arrayData[$i]['obs'] = $reg->obs;
                // Get data of a spefic Assessment
                $assessmentData = new Assessment($this->dbh);
                $arrayData[$i]['assessment'] = $assessmentData->getDataAssessment($reg->id_assessment);
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employeeData->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined performance
    function readDeterminedPerformance() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM performance WHERE id_assessment  = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['grade_obtained'] = $reg->grade_obtained;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic Assessment
                $assessmentData = new Assessment($this->dbh);
                $arrayData['assessment'] = $assessmentData->getDataAssessment($reg->id_assessment);
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData['employee'] = $employeeData->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update 
    function updatePerformance() 
    {
        $arrayData = [];
        $cons = "UPDATE performance SET id_assessment = ?, id_employee = ?,grade_obtained = ?, obs = ? WHERE id_assessment = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idAssessment);
        $prep->bindparam(2, $this->idEmployee);
        $prep->bindparam(3, $this->gradeObtained);
        $prep->bindparam(4, $this->obs);
        $prep->bindparam(5, $this->id[0]);
        $prep->bindparam(6, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data before and after the execution of an action
            $this->id = array($this->idAssessment, $this->idEmployee);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Desempenho', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete performance
    function deletePerformance() 
    {
        $arrayData = [];
        $cons = "DELETE FROM performance WHERE id_assessment = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $this->id = [$this->idAssessment, $this->idEmployee];
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Performance
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Desempenho', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get user profile
    function getUserProfile($idUser) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Assigned permitions to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $permition)
    {
        $i = 0;
        if ((is_array($permition)) || (!empty($permition))) {
            $this->idUser = $idUserCreated;
            $this->obs = "Perfil criado no momento do registo Utilizador";
            foreach ($permition as $rr) {
                $i++;
                $this->idPermition = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data performance
    function getDataPerformance($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM performance WHERE id_assessment  = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['grade_obtained'] = $reg->grade_obtained;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic Assessment
                $assessmentData = new Assessment($this->dbh);
                $arrayData['assessment'] = $assessmentData->getDataAssessment($reg->id_assessment);
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData['employee'] = $employeeData->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $arrayData=[];
        $dataReceivedFormated = '';
        $cons = "SELECT performance.grade_obtained, performance.obs AS performance_obs , assessment.designation, assessment.full_mark, assessment.obs, employee.full_name, employee.nationality, employee.gender, employee.employee_code FROM performance JOIN assessment ON performance.id_assessment=assessment.id JOIN employee ON performance.id_employee=employee.id WHERE performance.id_assessment = ? AND performance.id_employee= ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['grade_obtained'] = 'Nota Obtida: ' . $reg->grade_obtained;
                $arrayData['performance_obs'] = 'Observação do desempenho: ' . $reg->performance_obs;
                $arrayData['designation'] = 'Avaliação: ' . $reg->designation;
                $arrayData['full_mark'] = 'Máxima nota: ' . $reg->full_mark;
                $arrayData['obs'] = 'Observação da avaliação: ' . $reg->obs;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'C´podigo do funcionário: ' . $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>