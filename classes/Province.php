<?php
require_once 'Auditing.php';
class Province
{
	public $id;
	public $designation;
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Create Impact Filter
    function registerProvince()
	{
		$i = 0;
		$arrayData = [];
		$cons = "INSERT INTO province ValUES(?,?)";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id);
		$prep->bindparam(2, $this->designation);
		//$prep->execute();
		try{
			$prep->execute();
			$i++;
			//record inserted
			// Insert data in the auditing file
			$lastId = $this->dbh->lastInsertId();
			// Get data of Province before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
			// instance the class Auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('província','inserir','',$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read all Impact Filter
	function readProvince()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM province";
		$prep = $this->dbh->prepare($cons);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['designation'] = $reg->designation;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read determined Impact Filter
	function readDeterminedProvince()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM province WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['designation'] = $reg->designation;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Update Impact Filter
	function updateProvince()
	{
	    $arrayData = [];
		$cons = "UPDATE province SET designation = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->designation);
		$prep->bindparam(2, $this->id);
		//$prep->execute();
		// Get data of Province before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		try{
			$prep->execute();
			//record update
			// Get data of Province before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
			// instance the class auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('país','alterar',$dataBeforeExecution,$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			// return false;
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Delete Impact Filter
	function deleteProvince()
	{
        $arrayData = [];
		$cons = "DELETE FROM province WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		//$prep->execute();
		// Get data of Province before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		// instance the class Province
		$auditing = new Auditing($this->dbh);
		$response = $auditing->insertDataAuditingFile('país','eliminar',$dataBeforeExecution,'');
		try{
			$prep->execute();
			//record deleted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Get data Province
	function getDataProvince($id)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM province WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['designation'] = $reg->designation;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
		
	}
	
	// Get data before and after the execution of an action
	function getDataBeforeAfterAction($DataId)
	{
		$i = 0;
		$arrayData = [];
		$dataReceivedFormated = '';
		$cons = "SELECT * FROM Province WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $DataId, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData['id'] = 'Identificador: '.$reg->id;
				$arrayData['designation'] = 'Designação: '.$reg->designation;
				$i++;
			}
			//Format data of the system element
			if($arrayData){
				foreach($arrayData as $dr){
					$dataReceivedFormated .= $dr.', ';
				}
				$dataReceivedFormated = substr($dataReceivedFormated,0,-2);
			}else $dataReceivedFormated = '';
			return $dataReceivedFormated;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$dataReceivedFormated = $e->getMessage();
			return $dataReceivedFormated;
		}
	}
}
?>