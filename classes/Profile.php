<?php
require_once 'Auditing.php';
require_once 'User.php';
require_once 'Permition.php';
class Profile
{
	public $id;
	public $idUser;
	public $idPermition;
	public $obs;
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Create profile
    function registerProfile()
	{
		$arrayData = [];
		$cons = "INSERT INTO profile VALUES(?,?,?)";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->idUser);
		$prep->bindparam(2, $this->idPermition);
		$prep->bindparam(3, $this->obs);
		//$prep->execute();
		try{
			$prep->execute();
			//record inserted
			// Insert data in the auditing file
			//$lastId = $this->dbh->lastInsertId();
			$this->id = array($this->idUser,$this->idPermition);
			// Get data of profile before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
			// instance the class Profile
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('perfil','inserir','',$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			//return false;
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read all Profile
	function readProfile()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM profile";
		$prep = $this->dbh->prepare($cons);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData[$i]['id_user'] = $reg->id_user;
				// Get data of a spefic user
				$userData = new User($this->dbh);
				$arrayUserData = $userData->getDataUser($reg->id_user);
				$arrayData[$i]['user'] = $arrayUserData;
				// Get data of a spefic permition
				$permitionData = new Permition($this->dbh);
				$arrayPermitionData = $permitionData->getDataPermition($reg->id_permition);
				$arrayData[$i]['permition'] = $arrayPermitionData;
				$arrayData[$i]['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read determined Profile
	function readDeterminedProfile()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM profile	WHERE id_user = ? AND id_permition = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
		$prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData['id_user'] = $reg->id_user;
				// Get data of a spefic user
				$userData = new User($this->dbh);
				$arrayUserData = $userData->getDataUser($reg->id_user);
				$arrayData['user'] = $arrayUserData;
				// Get data of a spefic permition
				$permitionData = new Permition($this->dbh);
				$arraypermitionData = $permitionData->getDataPermition($reg->id_permition);
				$arrayData['permition'] = $arraypermitionData;
				$arrayData['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Update Profile
	function updateProfile()
	{
	    $arrayData = [];
		$cons = "UPDATE profile SET id_user = ?,id_permition = ?,obs = ? WHERE id_user = ? AND id_permition = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->idUser);
		$prep->bindparam(2, $this->idPermition);
		$prep->bindparam(3, $this->obs);
		$prep->bindparam(4, $this->id[0]);
		$prep->bindparam(5, $this->id[1]);
		//$prep->execute();
		// Get data of profile before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		try{
			$prep->execute();
			//record update
			// Get data of profile before and after the execution of an action
			$this->id = array($this->idUser,$this->idPermition);
			$dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
			// instance the class Profile
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('perfil','alterar',$dataBeforeExecution,$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Delete Profile
	function deleteProfile()
	{
        $arrayData = [];
		$cons = "DELETE FROM profile WHERE id_user = ? AND id_permition = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
		$prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
		//$prep->execute();
		// Get data of profile before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		// instance the class Profile
		$auditing = new Auditing($this->dbh);
		$response = $auditing->insertDataAuditingFile('perfil','eliminar',$dataBeforeExecution,'');
		try{
			$prep->execute();
			//record deleted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Get user profile
	function getUserProfile($idUser)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $idUser, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$arrayData[$i]['acronym'] = $reg->acronym;
				$arrayData[$i]['designation'] = $reg->designation;
				$arrayData[$i]['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Assigned permitions to user when creating this one, in order to create a new profile
	function createUserProfile($idUserCreated,$permition)
	{
		$i = 0;
		if((is_array($permition)) || (!empty($permition))){
			$this->idUser = $idUserCreated;
			$this->obs = "Perfil criado no momento do registo Utilizador";
			foreach($permition as $rr){
				$i++;
				$this->idPermition = $rr;
				$this->registerProfile();
			}
		}
	}
	
	// Get data profile
	function getDataProfile($id)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM profile	WHERE id_user = ? AND id_permition = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $id[0], PDO::PARAM_STR);
		$prep->bindparam(2, $id[1], PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData['id_user'] = $reg->id_user;
				// Get data of a spefic user
				$userData = new User($this->dbh);
				$arrayUserData = $userData->getDataUser($reg->id_user);
				$arrayData['user'] = $arrayUserData;
				//$arrayData['id_permition'] = $reg->id_permition;
				// Get data of a spefic permition
				$permitionData = new permition($this->dbh);
				$arrayPermitionData = $permitionData->getDataPermition($reg->id_permition);
				$arrayData['permition'] = $arrayPermitionData;
				$arrayData['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
		
	}
	
	// Get data before and after the execution of an action
	function getDataBeforeAfterAction($DataId)
	{
		$i = 0;
		$dataReceivedFormated = '';
		$cons = "SELECT * FROM profile 
				JOIN user ON user.id = profile.id_user
				JOIN permition ON permition.id = profile.id_permition
				WHERE profile.id_user = ? 
				AND profile.id_permition = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
		$prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData['user_name'] = 'Utilizador: '.$reg->name;
				$arrayData['user_email'] = 'Email: '.$reg->email;
				//$arrayData['acronym'] = 'Acrônimo: '.$reg->acronym;
				$arrayData['permition_designation'] = 'Designação: '.$reg->designation;
				$arrayData['obs'] = 'Observação: '.$reg->obs;
				$i++;
			}
			//Format data of the system element
			if($arrayData){
				foreach($arrayData as $dr){
					$dataReceivedFormated .= $dr.', ';
				}
				$dataReceivedFormated = substr($dataReceivedFormated,0,-2);
			}else $dataReceivedFormated = '';
			return $dataReceivedFormated;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			//return $e->getMessage();
			$dataReceivedFormated = $e->getMessage();
			return $dataReceivedFormated;
		}
	}
}
?>