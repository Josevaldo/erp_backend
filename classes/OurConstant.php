<?php

class OurConstant
{

	public static $ip = '192.168.10.102';
	// public static $ip = '192.168.10.62';
	
	public static function reportCreated()
	{
		$localPath = '../documents/reportCreated/';
		return $localPath;
	} 
	
	public static function csvFiles()
	{
		//$this->localPath = '../documents/csvFiles/';
		$localPath = '../documents/csvFiles/';
		//return $this->localPath;
		return $localPath;
	} 
	
	public static function excelFiles()
	{
		$localPath = '../documents/excelFiles/';
		return $localPath;
	} 
	
	public static function reportCreatedProduction()
	{
		$aliasPath = 'http://'.self::$ip.'/documents';
		$productionPath = $aliasPath.'/reportCreated/';
		return $productionPath;
	} 
	
	public static function csvFilesProduction()
	{
		$aliasPath = 'http://'.self::$ip.'/documents';
		$productionPath = $aliasPath.'/csvFiles/';
		return $productionPath;
	} 
	
	public static function excelFilesProduction(){
		$aliasPath = 'http://'.self::$ip.'/documents';
		$productionPath = $aliasPath.'/excelFiles/';
		return $productionPath;
	}

}

?>