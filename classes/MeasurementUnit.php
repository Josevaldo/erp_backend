<?php

require_once 'Auditing.php';

class MeasurementUnit
{

    public $id;
    public $symbol;
    public $designation;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create measurement_unit
    function registerMeasurementUnit()
    {
        $arrayData = [];
        $cons = "INSERT INTO measurement_unit VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->symbol);
        $prep->bindparam(3, $this->designation);
        try {
            $prep->execute();
            $lastId = $this->dbh->lastInsertId();
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Unidade de medida', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all measurement_unit
    function readMeasurementUnit() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM measurement_unit";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['symbol'] = $reg->symbol;
                $arrayData[$i]['designation'] = $reg->designation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined measurement_unit
    function readDeterminedMeasurementUnit()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM measurement_unit WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['symbol'] = $reg->symbol;
                $arrayData['designation'] = $reg->designation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update Indicator_unit_measurement
    function updateIndicatorUnitMeasurement() 
    {
        $arrayData = [];
        $cons = "UPDATE measurement_unit SET symbol = ?, designation = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->symbol);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->id);
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Unidade de medida', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete measurement_unit
    function deleteIndicatorUnitMeasurement() 
    {
        $arrayData = [];
        $cons = "DELETE FROM measurement_unit WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class politic
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Unidade de medida', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data of a measurement_unit
    function getDataMeasurementUnit($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM measurement_unit WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['symbol'] = $reg->symbol;
                $arrayData['designation'] = $reg->designation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an measurement_unit
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM measurement_unit WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['symbol'] = $reg->symbol;
                $arrayData['designation'] = $reg->designation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

}

?>