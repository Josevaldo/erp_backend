<?php

require_once 'Auditing.php';

class LicenseType
{

    public $id;
    public $designation;
    public $numberDay;
    public $price;
    public $obs;  
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

// Create license_type
    function registerLicenseType()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO license_type VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->numberDay);
        $prep->bindparam(4, $this->price);
        $prep->bindparam(5, $this->obs);
        
        try {
            $prep->execute();
            $i++;
//record inserted
// Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
// Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
// instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Tipo de licença', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Read all license_type
    function readLicenseType() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM license_type";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['number_day'] = $reg->number_day;
                $arrayData[$i]['price'] = $reg->price;
                $arrayData[$i]['obs'] = $reg->obs;               
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Read determined license_type
    function readDeterminedLicenseType() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM license_type WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['number_day'] = $reg->number_day;
                $arrayData['price'] = $reg->price;
                $arrayData['obs'] = $reg->obs; 
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Update license_type
    function updateLicenseType()
    {
        $arrayData = [];
        $cons = "UPDATE license_type SET designation = ?, number_day = ?, price = ?, obs = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);       
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->numberDay);
        $prep->bindparam(3, $this->price);
        $prep->bindparam(4, $this->obs); 
        $prep->bindparam(5, $this->id);
// Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
//record update
// Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
// instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Tipo de licença', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
// return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Delete license_type
    function deleteLicenseType() 
    {
        $arrayData = [];
        $cons = "DELETE FROM license_type WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
//$prep->execute();
// Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
// instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Tipo de licença', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
//record deleted
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Get data license_type
    function getDataLicenseType($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM license_type WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['number_day'] = $reg->number_day;
                $arrayData['price'] = $reg->price;
                $arrayData['obs'] = $reg->obs; 
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT* FROM license_type WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = "Identificaor do tipo de licença: ".$reg->id;
                $arrayData['designation'] = "Designação: ".$reg->designation;
                $arrayData['number_day'] = "Número do dia: ".$reg->number_day;
                $arrayData['price'] = "Preço: ".$reg->price;
                $arrayData['obs'] = "Observação: ".$reg->obs;
            }
//Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>