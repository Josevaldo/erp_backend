<?php

require_once 'Auditing.php';

class Education
{

    public $id;
    public $designation;
    public $obs;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create education
    function registerEducation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO education VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Educação', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all education
    function readEducation() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM education";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined education
    function readDeterminedEducation() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM education WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update education
    function updateEducation() 
    {
        $arrayData = [];
        $cons = "UPDATE education SET designation = ?, obs = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->obs);
        $prep->bindparam(3, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Educação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete education
    function deleteEducation()
    {
        $arrayData = [];
        $cons = "DELETE FROM education WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Educação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data education
    function getDataEducation($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM education WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM education WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['designation'] = 'Designação: ' . $reg->designation;
                $arrayData['obs'] = 'Observação: ' . $reg->obs;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>