<?php

require_once 'Auditing.php';
require_once 'Employee.php';
require_once 'Education.php';
require_once 'AcademicDegree.php';

class AcademicEducation 
{

    public $id;
    public $idEmployee;
    public $idEducation;
    public $idAcademicDegree;
    public $initialYear;
    public $endYear;
    public $establishment;
    public $obs;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create academic_education
    function registerAcademicEducation()
    {
        $arrayData = [];
        $cons = "INSERT INTO academic_education VALUES(?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idEmployee);
        $prep->bindparam(2, $this->idEducation);
        $prep->bindparam(3, $this->idAcademicDegree);
        $prep->bindparam(4, $this->initialYear);
        $prep->bindparam(5, $this->endYear);
        $prep->bindparam(6, $this->establishment);
        $prep->bindparam(7, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idEmployee, $this->idEducation, $this->idAcademicDegree);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Educação acadêmica', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all academic_education
    function readAcademicEducation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM academic_education";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['initial_year'] = $reg->initial_year;
                $arrayData[$i]['end_year'] = $reg->end_year;
                $arrayData[$i]['establishment'] = $reg->establishment;
                $arrayData[$i]['obs'] = $reg->obs;
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employeeData->getDataEmployee($reg->id_employee);
                // Get data of a spefic Academic Degree
                $academicDegreeData = new AcademicDegree($this->dbh);
                $arrayData[$i]['academic_degree'] = $academicDegreeData->getDataAcademicDegree($reg->id_academic_degree);
                // Get data of a spefic Education
                $educationData = new Education($this->dbh);
                $arrayData[$i]['academic_degree'] = $educationData->getDataEducation($reg->id_education);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined academic_education
    function readDeterminedAcademicEducation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM academic_education WHERE id_employee  = ? AND id_education = ? AND id_academic_degree = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['initial_year'] = $reg->initial_year;
                $arrayData['end_year'] = $reg->end_year;
                $arrayData['establishment'] = $reg->establishment;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData['employee'] = $employeeData->getDataEmployee($reg->id_employee);
                // Get data of a spefic Academic Degree
                $academicDegreeData = new AcademicDegree($this->dbh);
                $arrayData['academic_degree'] = $academicDegreeData->getDataAcademicDegree($reg->id_academic_degree);
                // Get data of a spefic Education
                $educationData = new Education($this->dbh);
                $arrayData['academic_degree'] = $educationData->getDataEducation($reg->id_education);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update academic_education
    function updateacAdemicEducation() 
    {
        $arrayData = [];
        $cons = "UPDATE academic_education SET 	id_employee = ?,id_education  = ?,id_academic_degree = ?, initial_year = ?, end_year = ?, establishment = ?, obs = ? WHERE id_employee  = ? AND id_education = ? AND id_academic_degree = ? ";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idEmployee);
        $prep->bindparam(2, $this->idEducation);
        $prep->bindparam(3, $this->idAcademicDegree);
        $prep->bindparam(4, $this->initialYear);
        $prep->bindparam(5, $this->endYear);
        $prep->bindparam(6, $this->establishment);
        $prep->bindparam(7, $this->obs);
        $prep->bindparam(8, $this->id[0]);
        $prep->bindparam(9, $this->id[1]);
        $prep->bindparam(10, $this->id[2]);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            $this->id = array($this->idEmployee, $this->idEducation, $this->idAcademicDegree);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Educação acadêmica', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete academic_education
    function deleteAcademicEducation()
    {
        $arrayData = [];
        $cons = "DELETE FROM academic_education WHERE id_employee  = ? AND id_education = ? AND id_academic_degree = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        //$prep->execute();
        // Get data before and after the execution of an action
        $this->id = array($this->idEmployee, $this->idEducation, $this->idAcademicDegree);
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Educação acadêmica', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get user profile
    function getUserProfile($idUser)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Assigned permitions to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $permition) 
    {
        $i = 0;
        if ((is_array($permition)) || (!empty($permition))) {
            $this->idUser = $idUserCreated;
            $this->obs = "Perfil criado no momento do registo Utilizador";
            foreach ($permition as $rr) {
                $i++;
                $this->idPermition = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data academic_education
    function getDataAcademicEducation($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM academic_education WHERE id_employee  = ? AND id_education = ? AND id_academic_degree = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $id[2], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['initial_year'] = $reg->initial_year;
                $arrayData['end_year'] = $reg->end_year;
                $arrayData['establishment'] = $reg->establishment;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic employee
                $employeeData = new Employee($this->dbh);
                $arrayData['employee'] = $employeeData->getDataEmployee($reg->id_employee);
                // Get data of a spefic Academic Degree
                $academicDegreeData = new AcademicDegree($this->dbh);
                $arrayData['academic_degree'] = $academicDegreeData->getDataAcademicDegree($reg->id_academic_degree);
                // Get data of a spefic Education
                $educationData = new Education($this->dbh);
                $arrayData['academic_degree'] = $educationData->getDataEducation($reg->id_education);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $arrayData=[];
        $dataReceivedFormated = '';
        $cons = "SELECT academic_education.initial_year, academic_education.end_year, academic_education.establishment, academic_education.obs AS academic_education_obs, education.designation, education.obs AS education_obs, employee.full_name, employee.nationality, employee.gender, employee.employee_code, academic_degree.designation AS academic_degree_designation, academic_degree.obs FROM academic_education JOIN employee ON academic_education.id_employee=employee.id JOIN education on academic_education.id_education=education.id JOIN academic_degree ON academic_education.id_academic_degree=academic_degree.id WHERE academic_education.id_employee = ? AND academic_education.id_education = ? AND academic_education.id_academic_degree = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        $prep->bindparam(3, $DataId[2], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['initial_year'] = 'Ano inicial: ' . $reg->end_year;
                $arrayData['end_year'] = 'Ano final: ' . $reg->end_year;
                $arrayData['establishment'] = 'Estabelecimento: ' . $reg->establishment;
                $arrayData['academic_education_obs'] = 'Observação da educação acadêmica: ' . $reg->academic_education_obs;
                $arrayData['designation'] = 'Educação: ' . $reg->designation;
                $arrayData['education_obs'] = 'Observação da educação: ' . $reg->education_obs;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
                $arrayData['academic_degree_designation'] = 'Grau acadêmico: ' . $reg->academic_degree_designation;
                $arrayData['obs'] = 'Observação do grau acadêmico: ' . $reg->obs;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>