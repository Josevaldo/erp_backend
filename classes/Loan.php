<?php

require_once 'Auditing.php';
require_once 'Employee.php';
require_once 'LoanType.php';

class Loan
{

    public $id;
    public $designation;
    public $registerDate;
    public $dateFirstPayment;
    public $dateLastPayment;
    public $value;
    public $idEmployee;
    public $idTypeLoan;
    public $state;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create loan
    function registerLoan() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO loan VALUES(?,?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->registerDate);
        $prep->bindparam(4, $this->dateFirstPayment);
        $prep->bindparam(5, $this->dateLastPayment);
        $prep->bindparam(6, $this->value);
        $prep->bindparam(7, $this->idEmployee);
        $prep->bindparam(8, $this->idTypeLoan);
        $prep->bindparam(9, $this->state);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Empréstimo', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all loan
    function readLoan() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM loan";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['date_first_payment'] = $reg->date_first_payment;
                $arrayData[$i]['date_last_payment'] = $reg->date_last_payment;
                $arrayData[$i]['value'] = $reg->value;
                $arrayData[$i]['state'] = $reg->state;
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['id_employee'] = $employee->getDataEmployee($reg->id_employee);
                //Instanciate the LoanType class
                $loanType = new LoanType($this->dbh);
                $arrayData[$i]['id_loan_type'] = $loanType->getDataLoanType($reg->id_type_loan);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined loan
    function readDeterminedLoan() 
    {
        $arrayData = [];
        $cons = "SELECT * FROM loan WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['date_first_payment'] = $reg->date_first_payment;
                $arrayData['date_last_payment'] = $reg->date_last_payment;
                $arrayData['value'] = $reg->value;
                $arrayData['state'] = $reg->state;
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['id_employee'] = $employee->getDataEmployee($reg->id_employee);
                //Instanciate the LoanType class
                $loanType = new LoanType($this->dbh);
                $arrayData['id_loan_type'] = $loanType->getDataLoanType($reg->id_type_loan);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update loan
    function updateLoan() 
    {
        $arrayData = [];
        $cons = "UPDATE loan SET designation = ?, date_first_payment = ?, date_last_payment = ?, value = ?, id_employee = ?, id_type_loan = ?, state = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->dateFirstPayment);
        $prep->bindparam(3, $this->dateLastPayment);
        $prep->bindparam(4, $this->value);
        $prep->bindparam(5, $this->idEmployee);
        $prep->bindparam(6, $this->idTypeLoan);
        $prep->bindparam(7, $this->state);
        $prep->bindparam(8, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Empréstimo', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete loan
    function deleteLoan()
    {
        $arrayData = [];
        $cons = "DELETE FROM loan WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Empréstimo', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data loan
    function getDataLoan($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM loan WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['date_first_payment'] = $reg->date_first_payment;
                $arrayData['date_last_payment'] = $reg->date_last_payment;
                $arrayData['value'] = $reg->value;
                $arrayData['state'] = $reg->state;
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['id_employee'] = $employee->getDataEmployee($reg->id_employee);
                //Instanciate the LoanType class
                $loanType = new LoanType($this->dbh);
                $arrayData['id_loan_type'] = $loanType->getDataLoanType($reg->id_type_loan);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT loan.designation AS loan_designation, loan.register_date, loan.date_first_payment, loan.date_last_payment, loan.value, loan.state, loan_type.designation AS loan_type_designation, loan_type.installment_number, loan_type.obs AS loan_type_obs, employee.full_name, employee.birth_place, employee.birth_date, employee.nationality, employee.gender, employee.employee_code FROM loan JOIN loan_type ON loan.id_type_loan=loan_type.id JOIN employee ON loan.id_employee=employee.id WHERE loan.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['loan_designation'] = 'Emprèstimo: ' . $reg->loan_designation;
                $arrayData['register_date'] = 'Data de registo: ' . $reg->register_date;
                $arrayData['date_first_payment'] = 'Data do primeiro pagamento: ' . $reg->date_first_payment;
                $arrayData['date_last_payment'] = 'Data do último pagamento: ' . $reg->date_last_payment;
                $arrayData['value'] = 'Valor: ' . $reg->value;
                $arrayData['state'] = 'Estado: ' . $reg->state;
                $arrayData['loan_type_designation'] = 'Observação: ' . $reg->loan_type_designation;
                $arrayData['installment_number'] = 'Número de prestação: ' . $reg->installment_number;
                $arrayData['loan_type_obs'] = 'Observação do tipo de esmpréstimo: ' . $reg->loan_type_obs;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['birth_date'] = 'Data de nascimento: ' . $reg->birth_date;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>