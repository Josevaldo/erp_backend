<?php

require_once 'Auditing.php';
//Valeu guy
class License
{

    public $id;
    public $registrationDate;
    public $expirationDate;
    public $valuePaid;
    public $idLicenseType; 
    public $idUser ;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

// Create license
    function registerLicense()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO license VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->registrationDate);
        $prep->bindparam(3, $this->expirationDate);
        $prep->bindparam(4, $this->valuePaid);
        $prep->bindparam(5, $this->idLicenseType);
        $prep->bindparam(6, $this->idUser);
        
        try {
            $prep->execute();
            $i++;
//record inserted
// Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
// Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
// instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Licença', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Read all license
    function readLicense() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM license";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['registration_date'] = $reg->registration_date;
                $arrayData[$i]['expiration_date'] = $reg->expiration_date;
                $arrayData[$i]['value_paid'] = $reg->value_paid;
                $arrayData[$i]['id_license_type'] = $reg->id_license_type;
                $arrayData[$i]['id_user'] = $reg->id_user;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Read determined license
    function readDeterminedLicense() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM license WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['registration_date'] = $reg->registration_date;
                $arrayData['expiration_date'] = $reg->expiration_date;
                $arrayData['value_paid'] = $reg->value_paid;
                $arrayData['id_license_type'] = $reg->id_license_type;
                $arrayData['id_user'] = $reg->id_user;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Update license
    function updateLicense()
    {
        $arrayData = [];
        $cons = "UPDATE license SET registration_date = ?, expiration_date = ?, value_paid = ?, id_license_type = ?, id_user =? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);       
        $prep->bindparam(1, $this->registrationDate);
        $prep->bindparam(2, $this->expirationDate);
        $prep->bindparam(3, $this->valuePaid);
        $prep->bindparam(4, $this->idLicenseType);
        $prep->bindparam(5, $this->idUser);
        $prep->bindparam(6, $this->id);
// Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
//record update
// Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
// instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Licença', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
// return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Delete license
    function deleteLicense() 
    {
        $arrayData = [];
        $cons = "DELETE FROM license WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
//$prep->execute();
// Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
// instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Licença', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
//record deleted
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Get data license
    function getDataLicense($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM license WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['registration_date'] = $reg->registration_date;
                $arrayData['expiration_date'] = $reg->expiration_date;
                $arrayData['value_paid'] = $reg->value_paid;
                $arrayData['id_license_type'] = $reg->id_license_type;
                $arrayData['id_user'] = $reg->id_user;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

// Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT license.registration_date, license.expiration_date, license.value_paid, license_type.designation, license_type.number_day, license_type.price, license_type.obs, user.complete_name, user.telephone, user.email FROM license JOIN license_type ON license.id_license_type=license_type.id JOIN user ON license.id_user=user.id WHERE license.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['registration_date'] = "Data de registo: ".$reg->registration_date;
                $arrayData['expiration_date'] = "Data de expiração: ".$reg->expiration_date;
                $arrayData['value_paid'] = "Valor pago: ".$reg->value_paid;
                $arrayData['designation'] = "Tipo de licença: ".$reg->designation;
                $arrayData['number_day'] = "Número do dia: ".$reg->number_day;
                $arrayData['price'] = "Preço: ".$reg->price;
                $arrayData['obs'] = "Observação: ".$reg->obs;
                $arrayData['complete_name'] = "Nome do usuário: : ".$reg->complete_name;
                $arrayData['telephone'] = "Telefone do usuário: ".$reg->telephone;
                $arrayData['email'] = "Email do usuário: ".$reg->email;
            }
//Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>