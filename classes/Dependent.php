<?php

require_once 'Auditing.php';
require_once 'Employee.php';

class Dependent 
{

    public $id;
    public $fullName;
    public $birthPlace;
    public $birthDate;
    public $gender;
    public $kinshipDegree;
    public $idEmployee ;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create dependent
    function registerDependent()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO dependent VALUES(?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->fullName);
        $prep->bindparam(3, $this->birthPlace);
        $prep->bindparam(4, $this->birthDate);
        $prep->bindparam(5, $this->gender);
        $prep->bindparam(6, $this->kinshipDegree);
        $prep->bindparam(7, $this->idEmployee);
        
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Dependente', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all dependent
    function readDependent()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM dependent";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['full_name'] = $reg->full_name;
                $arrayData[$i]['birth_place'] = $reg->birth_place;
                $arrayData[$i]['birth_date'] = $reg->birth_date;
                $arrayData[$i]['gender'] = $reg->gender;
                $arrayData[$i]['kinship_degree'] = $reg->kinship_degree;
                //Instanciate the employee
                $employuee = new Employee($this->dbh);
                $arrayData[$i]['employreee'] = $employuee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined dependent
    function readDeterminedDependent()
    {
        $arrayData = [];
        $cons = "SELECT * FROM dependent WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['full_name'] = $reg->full_name;
                $arrayData['birth_place'] = $reg->birth_place;
                $arrayData['birth_date'] = $reg->birth_date;
                $arrayData['gender'] = $reg->gender;
                $arrayData['kinship_degree'] = $reg->kinship_degree;
                //Instanciate the employee
                $employuee = new Employee($this->dbh);
                $arrayData['employreee'] = $employuee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update dependent
    function updateDependent() 
    {
        $arrayData = [];
        $cons = "UPDATE dependent SET full_name = ?, birth_place = ?, birth_date = ?, gender = ?, kinship_degree = ?, id_employee = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->fullName);
        $prep->bindparam(2, $this->birthPlace);
        $prep->bindparam(3, $this->birthDate);
        $prep->bindparam(4, $this->gender);
        $prep->bindparam(5, $this->kinshipDegree);
        $prep->bindparam(6, $this->idEmployee);
        $prep->bindparam(7, $this->id);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Dependente', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete dependent
    function deleteDependent() 
    {
        $arrayData = [];
        $cons = "DELETE FROM dependent WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Dependente', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data dependent
    function getDataDependent($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM dependent WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['full_name'] = $reg->full_name;
                $arrayData['birth_place'] = $reg->birth_place;
                $arrayData['birth_date'] = $reg->birth_date;
                $arrayData['gender'] = $reg->gender;
                $arrayData['kinship_degree'] = $reg->kinship_degree;
                //Instanciate the employee
                $employuee = new Employee($this->dbh);
                $arrayData['employreee'] = $employuee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT dependent.full_name AS dependent_full_name, dependent.birth_place AS dependent_birth_place, dependent.birth_date AS dependent_birth_date , dependent.gender AS dependent_gender , dependent.kinship_degree, employee.full_name, employee.birth_place, employee.birth_date, employee.nationality, employee.gender, employee.employee_code FROM dependent JOIN employee ON dependent.id_employee=employee.id WHERE dependent.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['dependent_full_name'] = 'Nome completo do depende: ' . $reg->dependent_full_name;
                $arrayData['dependent_birth_place'] = 'Local de nascimento do dependente: ' . $reg->dependent_birth_place;
                $arrayData['dependent_birth_date'] = 'Data de nascimento do dependente: ' . $reg->dependent_birth_date;
                $arrayData['dependent_gender'] = 'Sexo do dependente: ' . $reg->dependent_gender;
                $arrayData['kinship_degree'] = 'Grau de parentesco: ' . $reg->kinship_degree;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['birth_place'] = 'Local do nascimento do funcionário: ' . $reg->birth_place;
                $arrayData['birth_date'] = 'Data de nascimento do funcionário: ' . $reg->birth_date;
                $arrayData['nationality'] = 'Nacionalidade do funcionário: ' . $reg->nationality;
                
                $arrayData['gender'] = 'Sexo do funcionário: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
                
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>