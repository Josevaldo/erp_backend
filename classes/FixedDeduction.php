<?php

require_once 'Auditing.php';
require_once 'Office.php';
require_once 'Deduction.php';
require_once 'MeasurementUnitDeduction.php';

class FixedDeduction
{

    public $id;
    public $idOffice;
    public $idDeduction;
    public $value;
    public $idMeasurementUnitDeduction;
    public $obs;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create fixed_deduction
    function registerFixedDeduction() 
    {
        $arrayData = [];
        $cons = "INSERT INTO fixed_deduction VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idOffice);
        $prep->bindparam(2, $this->idDeduction);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->idMeasurementUnitDeduction);
        $prep->bindparam(5, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idOffice, $this->idDeduction);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('perfil', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all fixed_deduction
    function readFixedDeduction()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fixed_deduction";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['value'] = $reg->value;
                $arrayData[$i]['obs'] = $reg->obs;
                // Get data of a spefic Office
                $office = new Office($this->dbh);
                $arrayData[$i]['office'] = $office->getDataOffice($reg->id_office);
                // Get data of a spefic deduction
                $deduction = new Deduction($this->dbh);
                $arrayData[$i]['deduction'] = $deduction->getDataDeduction($reg->id_deduction);
                // Get data of a spefic MeasurementUnitDeduction 
                $measurementIUnitDeduction = new MeasurementUnitDeduction($this->dbh);
                $arrayData[$i]['measurement_unit_deduction'] = $measurementIUnitDeduction->getDataMeasurementUnitDeduction($reg->id_measurement_unit_deduction);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined fixed_deduction
    function readDeterminedFixedDeduction() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fixed_deduction WHERE id_office = ? AND id_deduction = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['value'] = $reg->value;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic Office
                $office = new Office($this->dbh);
                $arrayData['office'] = $office->getDataOffice($reg->id_office);
                // Get data of a spefic deduction
                $deduction = new Deduction($this->dbh);
                $arrayData['deduction'] = $deduction->getDataDeduction($reg->id_deduction);
                // Get data of a spefic MeasurementUnitDeduction 
                $measurementIUnitDeduction = new MeasurementUnitDeduction($this->dbh);
                $arrayData['measurement_unit_deduction'] = $measurementIUnitDeduction->getDataMeasurementUnitDeduction($reg->id_measurement_unit_deduction);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update fixed_deduction
    function updateFixedDeduction() 
    {
        $arrayData = [];
        $cons = "UPDATE fixed_deduction SET id_office = ?, id_deduction = ?, value = ?, id_measurement_unit_deduction = ?, obs = ? WHERE id_office = ? AND id_deduction = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idOffice);
        $prep->bindparam(2, $this->idDeduction);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->idMeasurementUnitDeduction);
        $prep->bindparam(5, $this->obs);
        $prep->bindparam(6, $this->id[0]);
        $prep->bindparam(7, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            $this->id = array($this->idOffice, $this->idDeduction);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Dedução fixada', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete fixed_deduction
    function deleteFixedDeduction()
    {
        $arrayData = [];
        $cons = "DELETE FROM fixed_deduction WHERE id_office = ? AND id_deduction = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Dedução fixada', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get user profile
    function getUserProfile($idUser)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Assigned permitions to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $permition)
    {
        $i = 0;
        if ((is_array($permition)) || (!empty($permition))) {
            $this->idUser = $idUserCreated;
            $this->obs = "Perfil criado no momento do registo Utilizador";
            foreach ($permition as $rr) {
                $i++;
                $this->idPermition = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data fixed_deduction
    function getDataFixedDeduction($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fixed_deduction WHERE id_office = ? AND id_deduction = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['value'] = $reg->value;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic Office
                $office = new Office($this->dbh);
                $arrayData['office'] = $office->getDataOffice($reg->id_office);
                // Get data of a spefic deduction
                $deduction = new Deduction($this->dbh);
                $arrayData['deduction'] = $deduction->getDataDeduction($reg->id_deduction);
                // Get data of a spefic MeasurementUnitDeduction 
                $measurementIUnitDeduction = new MeasurementUnitDeduction($this->dbh);
                $arrayData['measurement_unit_deduction'] = $measurementIUnitDeduction->getDataMeasurementUnitDeduction($reg->id_measurement_unit_deduction);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $dataReceivedFormated = '';
        $cons = "SELECT fixed_deduction.value, fixed_deduction.obs AS fixed_deduction_obs, office.designation AS office_designation , office.obs AS office_obs, deduction.designation AS deduction_designation, deduction.obs AS deduction_obs, measurement_unit_deduction.symbol, measurement_unit_deduction.designation AS measurement_unit_deduction_designation FROM fixed_deduction JOIN office ON fixed_deduction.id_office=office.id JOIN deduction ON fixed_deduction.id_deduction=deduction.id JOIN measurement_unit_deduction ON fixed_deduction.id_measurement_unit_deduction=measurement_unit_deduction.id WHERE id_office = ? AND id_deduction = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['value'] = 'Valor da dedução fixada: ' . $reg->value;
                $arrayData['fixed_deduction_obs'] = 'Observação da dedução fixada: ' . $reg->fixed_deduction_obs;
                $arrayData['office_designation'] = 'Cargo: ' . $reg->office_designation;
                $arrayData['office_obs'] = 'Observação do cargo: ' . $reg->office_obs;
                $arrayData['deduction_designation'] = 'Dedução: ' . $reg->deduction_designation;
                $arrayData['deduction_obs'] = 'Observação da dedução: ' . $reg->deduction_obs;
                $arrayData['measurement_unit_deduction_designation'] = 'Medida da dedução: ' . $reg->measurement_unit_deduction_designation;
                $arrayData['symbol'] = 'Símbolo da medida: ' . $reg->symbol;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>