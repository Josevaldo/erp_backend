<?php

require_once 'Auditing.php';
require_once 'Employee.php';

class Missing
{

    public $id;
    public $registrationDate;
    public $missingDate;
    public $obs;
    public $idEmployee;
    public $justified;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create Missing
    function registerMissing() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO missing VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->registrationDate);
        $prep->bindparam(3, $this->missingDate);
        $prep->bindparam(4, $this->idEmployee);
        $prep->bindparam(5, $this->justified);
        $prep->bindparam(6, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Falta', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all Missing
    function readMissing()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM missing";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['registration_date'] = $reg->registration_date;
                $arrayData[$i]['missing_date'] = $reg->missing_date;
                $arrayData[$i]['justified'] = $reg->justified;
                $arrayData[$i]['obs'] = $reg->obs;
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined Missing
    function readDeterminedMissing() 
    {
        $arrayData = [];
        $cons = "SELECT * FROM Missing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['registration_date'] = $reg->registration_date;
                $arrayData['missing_date'] = $reg->missing_date;
                $arrayData['justified'] = $reg->justified;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update Missing
    function updateMissing()
    {
        $arrayData = [];
        $cons = "UPDATE missing SET missing_date = ?, id_employee = ?, obs = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->missingDate);
        $prep->bindparam(2, $this->idEmployee);
        //$prep->bindparam(3, $this->justified);
        $prep->bindparam(3, $this->obs);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Falta', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete Missing
    function deleteMissing() 
    {
        $arrayData = [];
        $cons = "DELETE FROM missing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Falta', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// Justify missing
    function justifyMissing()
    {
        $arrayData = [];
        $cons = "UPDATE missing SET justified = ?, obs = ? WHERE id_employee = ? AND DATE(missing_date) >= ? AND DATE(missing_date) <= ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->justified);
        $prep->bindparam(2, $this->obs);
        $prep->bindparam(3, $this->idEmployee);
        $prep->bindparam(4, $this->missingDate[0]);
        $prep->bindparam(5, $this->missingDate[1]);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Falta', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data Missing
    function getDataMissing($id) 
    {
        $arrayData = [];
        $cons = "SELECT * FROM missing WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['registration_date'] = $reg->registration_date;
                $arrayData['missing_date'] = $reg->missing_date;
                $arrayData['justified'] = $reg->justified;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM missing 
				JOIN employee ON missing.id_employee = employee.id
				WHERE missing.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['registration_date'] = 'Data Registo: ' . $reg->registration_date;
                $arrayData['missing_date'] = 'Data de Falta: ' . $reg->missing_date;
                $arrayData['Missing_obs'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['justified'] = 'Justificada: ' . $reg->justified;
                $arrayData['obs'] = 'Observação: ' . $reg->obs;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>