<?php

require_once 'Auditing.php';
require_once 'PeriodType.php';

class Period 
{

    public $id;
    public $designation;
    public $initialPeriod;
    public $endPeriod;
    public $idPeriodType;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create period
    function registerperiod()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO period VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->initialPeriod);
        $prep->bindparam(4, $this->endPeriod);
        $prep->bindparam(5, $this->idPeriodType);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Período', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all period
    function readPeriod() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM period";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['initial_period'] = $reg->initial_period;
                $arrayData[$i]['end_period'] = $reg->end_period;
                //Instanciate the PeriodType class
                $periodType = new PeriodType($this->dbh);
                $arrayData[$i]['period_type'] = $periodType->getDataPeriodType($reg->id_period_type);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined period
    function readDeterminedPeriod()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_period'] = $reg->initial_period;
                $arrayData['end_period'] = $reg->end_period;
                //Instanciate the PeriodType class
                $periodType = new PeriodType($this->dbh);
                $arrayData[$i]['period_type'] = $periodType->getDataPeriodType($reg->id_period_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update period
    function updatePeriod()
    {
        $arrayData = [];
        $cons = "UPDATE period SET designation = ?, initial_period = ?, end_period=?, id_period_type =? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->initialPeriod);
        $prep->bindparam(3, $this->endPeriod);
        $prep->bindparam(4, $this->idPeriodType);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Período', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete period
    function deletePeriod()
    {
        $arrayData = [];
        $cons = "DELETE FROM period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Período', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// Get data period from designation
    function getDataPeriodFromDesignation($designation) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM period WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_period'] = $reg->initial_period;
                $arrayData['end_period'] = $reg->end_period;
                //Instanciate the PeriodType class
                $periodType = new PeriodType($this->dbh);
                $arrayData[$i]['period_type'] = $periodType->getDataPeriodType($reg->id_period_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data period
    function getDataPeriod($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_period'] = $reg->initialPeriod;
                $arrayData['end_period'] = $reg->end_period;
                //Instanciate the PeriodType class
                $periodType = new PeriodType($this->dbh);
                $arrayData[$i]['period_type'] = $periodType->getDataPeriodType($reg->id_period_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT period.designation AS period_designation, period.initial_period, period.end_period, period_type.designation, period_type.obs FROM period 
				JOIN period_type ON period.id_period_type=period_type.id 
				WHERE period.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['period_designation'] = 'Período: ' . $reg->period_designation;
                $arrayData['initial_period'] = 'Início: ' . $reg->initial_period;
                $arrayData['end_period'] = 'Fim: ' . $reg->end_period;
                $arrayData['períod_type'] = 'Tipo de periodo: ' . $reg->designation;
                $arrayData['obs'] = 'Observação do tipo de período ' . $reg->obs;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>