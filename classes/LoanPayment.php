<?php

require_once 'Auditing.php';
require_once 'Loan.php';

class LoanPayment 
{

    public $id;
    public $registerDate;
    public $value;
    public $idLoan;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create loan_payment
    function registerLoanPayment() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO loan_payment VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->registerDate);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->idLoan);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Pagamento do Empréstimo', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all loan_payment
    function readLoanPayment() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM loan_payment";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['register_date'] = $reg->register_date;
                $arrayData[$i]['value'] = $reg->value;
                //Instanciate the Loan class
                $loan = new Loan($this->dbh);
                $arrayData[$i]['id_loan'] = $loan->getDataLoan($reg->id_loan);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined loan_payment
    function readDeterminedLoanPayment()
    {
        $arrayData = [];
        $cons = "SELECT * FROM loan_payment WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['value'] = $reg->value;
                //Instanciate the Loan class
                $loan = new Loan($this->dbh);
                $arrayData['id_loan'] = $loan->getDataLoan($reg->id_loan);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update loan_payment
    function updateLoanPayment() 
    {
        $arrayData = [];
        $cons = "UPDATE loan_payment SET value = ?, id_loan = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->value);
        $prep->bindparam(2, $this->idLoan);
        $prep->bindparam(3, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Pagamento do Empréstimo', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete loan_payment
    function deleteLoanPayment() 
    {
        $arrayData = [];
        $cons = "DELETE FROM loan_payment WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Pagamento Empréstimo', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data loan_payment
    function getDataLoanPayment($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM loan_payment WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['register_date'] = $reg->register_date;
                $arrayData['value'] = $reg->value;
                //Instanciate the Loan class
                $loan = new Loan($this->dbh);
                $arrayData['id_loan'] = $loan->getDataLoan($reg->id_loan);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT loan_payment.register_date AS loan_payment_register_date, loan_payment.value AS loan_payment_value, loan.designation, loan.register_date, loan.date_first_payment, loan.date_last_payment, loan.value, loan.state FROM loan_payment JOIN loan ON loan_payment.id_loan=loan.id WHERE loan_payment.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['loan_payment_register_date'] = 'Data do pagamento do Emprèstimo: ' . $reg->loan_payment_register_date;
                $arrayData['loan_payment_value'] = 'Valor pago: ' . $reg->loan_payment_value;
                $arrayData['designation'] = 'Empréstimo: ' . $reg->designation;
                $arrayData['register_date'] = 'Data do empréstimo: ' . $reg->register_date;
                $arrayData['state'] = 'Estado: ' . $reg->state;
                $arrayData['date_first_payment'] = 'Data do primeiro pagamento: ' . $reg->date_first_payment;
                $arrayData['date_last_payment'] = 'Data do úlltimo pagamento: ' . $reg->date_last_payment;
                $arrayData['value'] = 'Valor de esmpréstimo: ' . $reg->value;
                $arrayData['state'] = 'Estado: ' . $reg->state;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>