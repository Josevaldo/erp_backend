<?php
require_once 'Auditing.php';
class Office
{
	public $id;
	public $designation;
	public $obs;
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Create office
    function registerOffice()
	{
		$i = 0;
		$arrayData = [];
		$cons = "INSERT INTO office VALUES(?,?,?)";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id);
		$prep->bindparam(2, $this->designation);
		$prep->bindparam(3, $this->obs);
		//$prep->execute();
		try{
			$prep->execute();
			$i++;
			//record inserted
			// Insert data in the auditing file
			$lastId = $this->dbh->lastInsertId();
			// Get data of office before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
			// instance the class Auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('cargo','inserir','',$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read all office
	function readOffice()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM office";
		$prep = $this->dbh->prepare($cons);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['designation'] = $reg->designation;
				$arrayData[$i]['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read determined office
	function readDeterminedOffice()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM office WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['designation'] = $reg->designation;
				$arrayData['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Update office
	function updateOffice()
	{
	    $arrayData = [];
		$cons = "UPDATE office SET designation = ?, obs = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->designation);
		$prep->bindparam(2, $this->obs);
		$prep->bindparam(3, $this->id);
		//$prep->execute();
		// Get data of office before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		try{
			$prep->execute();
			//record update
			// Get data of office before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
			// instance the class auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('cargo','alterar',$dataBeforeExecution,$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			// return false;
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Delete office
	function deleteOffice()
	{
        $arrayData = [];
		$cons = "DELETE FROM office WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		//$prep->execute();
		// Get data of office before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		// instance the class office
		$auditing = new Auditing($this->dbh);
		$response = $auditing->insertDataAuditingFile('cargo','eliminar',$dataBeforeExecution,'');
		try{
			$prep->execute();
			//record deleted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Get data office
	function getDataOffice($id)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM office WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['designation'] = $reg->designation;
				$arrayData['obs'] = $reg->obs;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
		
	}
	
	// Get data before and after the execution of an action
	function getDataBeforeAfterAction($DataId)
	{
		$i = 0;
		$arrayData = [];
		$dataReceivedFormated = '';
		$cons = "SELECT * FROM office WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $DataId, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData['id'] = 'Identificador: '.$reg->id;
				$arrayData['designation'] = 'Descrição: '.$reg->designation;
				$arrayData['obs'] = 'Acrónym: '.$reg->obs;
				$i++;
			}
			//Format data of the system element
			if($arrayData){
				foreach($arrayData as $dr){
					$dataReceivedFormated .= $dr.', ';
				}
				$dataReceivedFormated = substr($dataReceivedFormated,0,-2);
			}else $dataReceivedFormated = '';
			return $dataReceivedFormated;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$dataReceivedFormated = $e->getMessage();
			return $dataReceivedFormated;
		}
	}
}
?>