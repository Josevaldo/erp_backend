<?php
require_once 'Auditing.php';
class Municipal
{
	public $id;
	public $designation;
	public $idProvince;
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Create municipal
    function registerMunicipal()
	{
		$i = 0;
		$arrayData = [];
		$cons = "INSERT INTO municipal ValUES(?,?,?)";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id);
		$prep->bindparam(2, $this->designation);
		$prep->bindparam(3, $this->idProvince);
		//$prep->execute();
		try{
			$prep->execute();
			$i++;
			//record inserted
			// Insert data in the auditing file
			$lastId = $this->dbh->lastInsertId();
			// Get data of Municipal before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
			// instance the class Auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('municipio','inserir','',$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read all municipal
	function readMunicipal()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM municipal";
		$prep = $this->dbh->prepare($cons);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['designation'] = $reg->designation;
				$arrayData[$i]['id_province'] = $reg->id_province;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read determined municipal
	function readDeterminedMunicipal()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM municipal WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['designation'] = $reg->designation;
				$arrayData['id_province'] = $reg->id_province;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Update municipal
	function updateMunicipal()
	{
	    $arrayData = [];
		$cons = "UPDATE municipal SET designation = ?, id_province = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->designation);
		$prep->bindparam(2, $this->idProvince);
		$prep->bindparam(3, $this->id);
		//$prep->execute();
		// Get data of Municipal before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		try{
			$prep->execute();
			//record update
			// Get data of Municipal before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
			// instance the class auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('municipio','alterar',$dataBeforeExecution,$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			// return false;
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Delete municipal
	function deleteMunicipal()
	{
        $arrayData = [];
		$cons = "DELETE FROM municipal WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		//$prep->execute();
		// Get data of Municipal before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		// instance the class Municipal
		$auditing = new Auditing($this->dbh);
		$response = $auditing->insertDataAuditingFile('municipio','eliminar',$dataBeforeExecution,'');
		try{
			$prep->execute();
			//record deleted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Get minicipal of a determined province
	function getMunicipalOfProvince($idProvince)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM municipal WHERE id_province = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $idProvince, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['designation'] = $reg->designation;
				$arrayData[$i]['id_province'] = $reg->id_province;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
		
	}
	
	// Get data Municipal
	function getDataMunicipal($id)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM municipal WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['designation'] = $reg->designation;
				$arrayData['id_province'] = $reg->id_province;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
		
	}
	
	// Get data before and after the execution of an action
	function getDataBeforeAfterAction($DataId)
	{
		$i = 0;
		$arrayData = [];
		$dataReceivedFormated = '';
		$cons = "SELECT municipal.designation AS desi_mun,province.designation AS desi_prov FROM municipal 
				JOIN province ON id_province = province,id 
				WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $DataId, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData['id'] = 'Identificador: '.$reg->id;
				$arrayData['designation'] = 'Designação: '.$reg->desi_mun;
				$arrayData['id_province'] = 'Província: '.$reg->desi_prov;
				$i++;
			}
			//Format data of the system element
			if($arrayData){
				foreach($arrayData as $dr){
					$dataReceivedFormated .= $dr.', ';
				}
				$dataReceivedFormated = substr($dataReceivedFormated,0,-2);
			}else $dataReceivedFormated = '';
			return $dataReceivedFormated;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$dataReceivedFormated = $e->getMessage();
			return $dataReceivedFormated;
		}
	}
}
?>