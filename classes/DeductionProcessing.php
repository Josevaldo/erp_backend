<?php

require_once 'Auditing.php';
require_once 'Processing.php';
require_once 'Office.php';
require_once 'Deduction.php';
require_once 'Employee.php';

class DeductionProcessing 
{

    public $id;
    public $idProcessing;
    public $idOffice;
    public $idDeduction;
    public $idEmployee;
    public $numberDeduction;
    public $value;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create deduction_processing
    function registerDeductionProcessing()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO deduction_processing VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProcessing);
        $prep->bindparam(2, $this->idOffice);
        $prep->bindparam(3, $this->idDeduction);
        $prep->bindparam(4, $this->idEmployee);
        $prep->bindparam(5, $this->numberDeduction);
        $prep->bindparam(6, $this->value);

        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $this->id = [$this->idProcessing, $this->idOffice, $this->idDeduction, $this->idEmployee];
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Processamento da dedução', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all deduction_processing
    function readDeductionProcessing()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM deduction_processing";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['number_deduction'] = $reg->number_deduction;
                $arrayData[$i]['value'] = $reg->value;
                //Instanciate the processing class
                $processing = new Processing($this->dbh);
                $arrayData[$i]['processing'] = $processing->getDataProcessing($reg->id_processing);
                //Instanciate the Office class
                $office = new Office($this->dbh);
                $arrayData[$i]['Office'] = $office->getDataOffice($reg->id_office);
                //Instanciate the deduction class
                $deduction = new Deduction($this->dbh);
                $arrayData[$i]['deduction'] = $deduction->getDataDeduction($reg->id_deduction);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined deduction_processing
    function readDeterminedDeductionProcessing() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM deduction_processing WHERE id_processing = ? AND id_office = ? AND id_deduction = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        $prep->bindparam(4, $this->id[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['number_deduction'] = $reg->number_deduction;
                $arrayData['value'] = $reg->value;
                //Instanciate the processing class
                $processing = new Processing($this->dbh);
                $arrayData['processing'] = $processing->getDataProcessing($reg->id_processing);
                //Instanciate the Office class
                $office = new Office($this->dbh);
                $arrayData['Office'] = $office->getDataOffice($reg->id_office);
                //Instanciate the deduction class
                $deduction = new Deduction($this->dbh);
                $arrayData['deduction'] = $deduction->getDataDeduction($reg->id_deduction);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update deduction_processing
    function updateDeductionProcessing()
    {
        $arrayData = [];
        $cons = "UPDATE deduction_processing SET id_processing = ?, id_office = ?, id_deduction = ?, id_employee = ?, number_deduction = ?, value = ? WHERE id_processing = ? AND id_office = ? AND id_deduction = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProcessing);
        $prep->bindparam(2, $this->idOffice);
        $prep->bindparam(3, $this->idDeduction);
        $prep->bindparam(4, $this->idEmployee);
        $prep->bindparam(5, $this->numberDeduction);
        $prep->bindparam(6, $this->value);
        $prep->bindparam(7, $this->id[0]);
        $prep->bindparam(8, $this->id[1]);
        $prep->bindparam(9, $this->id[2]);
        $prep->bindparam(10, $this->id[3]);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $this->id = [$this->idProcessing, $this->idOffice, $this->idDeduction, $this->idEmployee];
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Processamento da dedução', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete deduction_processing 
    function deleteDeductionProcessing () 
    {
        $arrayData = [];
        $cons = "DELETE FROM deduction_processing WHERE id_processing = ? AND id_office = ? AND id_deduction = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        $prep->bindparam(4, $this->id[3], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        $this->id = [$this->idProcessing, $this->idOffice, $this->idDeduction, $this->idEmployee];
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Processamento da dedução', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data deduction_processing
    function getDataDeductionProcessing($id) 
    {
        $arrayData = [];
        $cons = "SELECT * FROM deduction_processing WHERE id_processing = ? AND id_office = ? AND id_deduction = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $id[2], PDO::PARAM_STR);
        $prep->bindparam(4, $id[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['number_deduction'] = $reg->number_deduction;
                $arrayData['value'] = $reg->value;
                //Instanciate the processing class
                $processing = new Processing($this->dbh);
                $arrayData['processing'] = $processing->getDataProcessing($reg->id_processing);
                //Instanciate the Office class
                $office = new Office($this->dbh);
                $arrayData['Office'] = $office->getDataOffice($reg->id_office);
                //Instanciate the deduction class
                $deduction = new Deduction($this->dbh);
                $arrayData['deduction'] = $deduction->getDataDeduction($reg->id_deduction);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT deduction_processing.number_deduction, deduction_processing.value, processing.designation AS processing_designation, processing.processing_date, office.designation AS office_designation, deduction.designation AS deduction_designation, employee.full_name, employee.nationality, employee.gender, employee.employee_code FROM deduction_processing JOIN processing ON deduction_processing.id_processing=processing.id JOIN office ON deduction_processing.id_office=office.id JOIN deduction ON deduction_processing.id_deduction=deduction.id JOIN employee ON deduction_processing.id_employee=employee.id WHERE deduction_processing.id_processing = ? AND deduction_processing.id_office = ? AND deduction_processing.id_deduction = ? AND deduction_processing.id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        $prep->bindparam(3, $DataId[2], PDO::PARAM_STR);
        $prep->bindparam(4, $DataId[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['number_deduction'] = $reg->number_deduction;
                $arrayData['value'] = $reg->value;
                $arrayData['processing_designation'] = $reg->processing_designation;
                $arrayData['processing_date'] = $reg->processing_date;
                $arrayData['office_designation'] = $reg->office_designation;
                $arrayData['deduction_designation'] = $reg->deduction_designation;
                $arrayData['full_name'] = $reg->full_name;
                $arrayData['nationality'] = $reg->nationality;
                $arrayData['gender'] = $reg->gender;
                $arrayData['employee_code'] = $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>