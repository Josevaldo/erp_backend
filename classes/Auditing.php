<?php
class Auditing
{
	public $id;
	public $systemElement;
	public $actionExecuted;
	public $dataBefore;
	public $dataAfter;
	public $userName;
	public $executionDate;
	//public $filePathForDownload = 'http://be.ppn.gov.ao:8080/documents';
	public $filePathForDownload = 'http://be.planagrao.gov.ao/documents';
	//public $filePathForDownload = 'http://be.ppn.mep.gov.ao/documents';
	//public $filePathForDownload = 'http://192.168.10.83/documents';
	public $filePathLocal = '../documents/csvFiles';
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Create Auditing
    function registerAuditing()
	{
		$cons = "INSERT INTO auditing VALUES(?,?,?,?,?,?,?)";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id);
		$prep->bindparam(2, $this->systemElement);
		$prep->bindparam(3, $this->actionExecuted);
		$prep->bindparam(4, $this->dataBefore);
		$prep->bindparam(5, $this->dataAfter);
		$prep->bindparam(6, $this->userName);
		$prep->bindparam(7, $this->executionDate);
		//$prep->execute();
		try{
			$prep->execute();
			//record inserted
			$arrayData = [];
			$arrayData['id'] = $this->id;
			return $arrayData;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			//return $e->getMessage();
			return false;
		}
	}
	
	// Read all Auditing
	function readAuditing()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM auditing";
		$prep = $this->dbh->prepare($cons);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['system_element'] = $reg->system_element;
				$arrayData[$i]['action_executed'] = $reg->action_executed;
				$arrayData[$i]['data_before'] = $reg->data_before;
				$arrayData[$i]['data_after'] = $reg->data_after;
				$arrayData[$i]['user_name'] = $reg->user_name;
				$arrayData[$i]['execution_date'] = $reg->executionDate;
				$i++;
			}
			//$arrayData[$i]['total_record'] = $i;
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			return false;
		}
	}
	
	// Read determined Auditing
	function readDeterminedAuditing()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM auditing WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData['id'] = $reg->id;
				$arrayData['system_element'] = $reg->system_element;
				$arrayData['action_executed'] = $reg->action_executed;
				$arrayData['data_before'] = $reg->data_before;
				$arrayData['data_after'] = $reg->data_after;
				$arrayData['user_name'] = $reg->user_name;
				$arrayData['execution_date'] = $reg->execution_date;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			return $e->getMessage();
			//return false;
		}
	}
	
	// Update Auditing
	function updateAuditing()
	{
	    $cons = "UPDATE auditing SET system_element = ?,action_executed = ?,data_before = ?,data_after = ?,user_name = ?,execution_date = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->systemElement);
		$prep->bindparam(2, $this->actionExecuted);
		$prep->bindparam(3, $this->dataBefore);
		$prep->bindparam(4, $this->dataAfter);
		$prep->bindparam(5, $this->userName);
		$prep->bindparam(6, $this->executionDate);
		$prep->bindparam(7, $this->id);
		//$prep->execute();
		try{
			$prep->execute();
			//record update
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			//return false;
			return $e->getMessage();
		}
	}
	
	// Delete Auditing
	function deleteAuditing()
	{
        $cons = "DELETE FROM Auditing WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		//$prep->execute();
		try{
			$prep->execute();
			//record deleted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			return false;
		}
	}
	
	// Insert data in the auditing file
	function insertDataAuditingFile($systemElement,$actionExecuted,$dataReceivedBeforeAction,$dataReceivedAfterAction)
	{
		$executionDate = date('Y-m-d H:i:s');
		// Get the user associated with the actual token 
		require_once 'UserToken.php';
		$userToken = new UserToken();
		$userName = $userToken->getUserAssociatedWithToken($this->dbh);
		$this->id = NULL;
		$this->systemElement = $systemElement;
		$this->actionExecuted = $actionExecuted;
		$this->dataBefore = $dataReceivedBeforeAction;
		$this->dataAfter = $dataReceivedAfterAction;
		$this->userName = $userName;
		$this->executionDate = $executionDate;
		$response = $this->registerAuditing();
	}
	
	// Get Auditing data from a determined inicial date to another date
	function getAuditingFromInicialDateEndDate($inicialDate,$endDate)
	{
		$i = 0;
		$arrayData = [];
		$inicialDate = strtotime($inicialDate);
		$endDate = strtotime($endDate);
		$inicialDate = date('Y-m-d',$inicialDate);
		$endDate = date('Y-m-d',$endDate);
		$cons = "SELECT * FROM auditing 
				WHERE DATE(execution_date) >= ?
				AND DATE(execution_date) <= ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $inicialDate, PDO::PARAM_STR);
		$prep->bindparam(2, $endDate, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['system_element'] = $reg->system_element;
				$arrayData[$i]['action_executed'] = $reg->action_executed;
				$arrayData[$i]['data_before'] = $reg->data_before;
				$arrayData[$i]['data_after'] = $reg->data_after;
				$arrayData[$i]['user_name'] = $reg->user_name;
				$arrayData[$i]['execution_date'] = $reg->execution_date;
				$i++;
			}
			//$arrayData[$i]['total_record'] = $i;
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			return false;
		}
	}
	
	// Get Auditing data from a determined inicial date to another date in csv format
	function getAuditingFromInicialDateEndDateCsv($inicialDate,$endDate)
	{
		$i = 0;
		$arrayData = [];
		$inicialDate = strtotime($inicialDate);
		$endDate = strtotime($endDate);
		$inicialDate = date('Y-m-d',$inicialDate);
		$endDate = date('Y-m-d',$endDate);
		$cons = "SELECT * FROM auditing 
				WHERE DATE(execution_date) >= ?
				AND DATE(execution_date) <= ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $inicialDate, PDO::PARAM_STR);
		$prep->bindparam(2, $endDate, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['system_element'] = $reg->system_element;
				$arrayData[$i]['action_executed'] = $reg->action_executed;
				$arrayData[$i]['data_before'] = $reg->data_before;
				$arrayData[$i]['data_after'] = $reg->data_after;
				$arrayData[$i]['user_name'] = $reg->user_name;
				$arrayData[$i]['execution_date'] = $reg->execution_date;
				$i++;
			}
			$t = 'auditing'.time().'.csv';
			$fp = fopen('../documents/csvFiles/'.$t, 'w');
			$dataHeader = array("Identificador","Elemento do Sistema","Acção","Dados antes execução","Dados após execução","Utilizador","Data execução");
			// Write header of csv data form
			fputcsv($fp,$dataHeader);
			// Loop through file pointer and a line
			foreach ($arrayData as $fields) {
				fputcsv($fp, $fields);
			}
			//$arrayDataCsv['ppn.mep.gov.ao/csvFiles/'.$t] = $t;
			$arrayDataCsv = $this->filePathForDownload.'/csvFiles/'.$t;
			return $arrayDataCsv;
			//return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			//return false;
			return $e->getMessage();
		}
	}
	
	// Export auditing data in CSV format
	function exportAuditingDataCsv()
	{
		$auditingArray = [];
		$auditingArray = $this->readAuditing();
		$t = 'auditingFile'.time().'.csv';
		$fp = fopen('../documents/csvFiles/'.$t, 'w');
		$dataHeader = array("Objecto","Ação","Dados antes operação","Dados após operação","Utilizador","Data");
		// Write header of csv data form
		fputcsv($fp,$dataHeader);
		// Loop through file pointer and a line
		foreach ($auditingArray as $fields) {
			fputcsv($fp, $fields);
		}
		$arrayDataCsv = $this->filePathForDownload.'/csvFiles/'.$t;
		return $arrayDataCsv;
	}
}
?>
