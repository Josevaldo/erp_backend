<?php

require_once 'Auditing.php';
require_once 'Municipal.php';
require_once 'Action.php';

class Local
{

    public $id;
    public $country;
    public $idMunicipal;
    public $longitude;
    public $latitude;
    public $idAction;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create local
    function registerLocal() 
    {
        $arrayData = [];
        $cons = "INSERT INTO local VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->country);
        $prep->bindparam(3, $this->idMunicipal);
        $prep->bindparam(4, $this->longitude);
        $prep->bindparam(5, $this->latitude);
        $prep->bindparam(6, $this->idAction);

        try {
            $prep->execute();
            $lastId = $this->dbh->lastInsertId();
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Local', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all local
    function readLocal() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM local";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->country;
                $arrayData[$i]['code'] = $reg->longitude;
                $arrayData[$i]['comment'] = $reg->latitude;
                //Stancing the Municipal class 
                $municipal = new Municipal($this->dbh);
                $arrayData[$i]['municipal'] = $municipal->getDataMunicipal($reg->id_municipal);
                //Stancing the Action class 
                $action = new Action($this->dbh);
                $arrayData[$i]['action'] = $action->getDataAction($reg->id_action);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined local
    function readDeterminedLocal() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM local WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->country;
                $arrayData['code'] = $reg->longitude;
                $arrayData['comment'] = $reg->latitude;
                //Stancing the Municipal class 
                $municipal = new Municipal($this->dbh);
                $arrayData['municipal'] = $municipal->getDataMunicipal($reg->id_municipal);
                //Stancing the Action class 
                $action = new Action($this->dbh);
                $arrayData['action'] = $action->getDataAction($reg->id_action);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update local
    function updateLocal() 
    {
        $arrayData = [];
        $cons = "UPDATE local SET country = ?, id_municipal = ?, longitude = ?, latitude = ?, id_action =? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->country);
        $prep->bindparam(2, $this->idMunicipal);
        $prep->bindparam(3, $this->longitude);
        $prep->bindparam(4, $this->latitude);
        $prep->bindparam(5, $this->idAction);
        $prep->bindparam(6, $this->id);
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Local', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete local
    function deleteLocal()
    {
        $arrayData = [];
        $cons = "DELETE FROM local WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        // Get data of impact filter before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Local', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data of a local
    function getDataLocal($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM local WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->country;
                $arrayData['code'] = $reg->longitude;
                $arrayData['comment'] = $reg->latitude;
                //Stancing the Municipal class 
                $municipal = new Municipal($this->dbh);
                $arrayData['municipal'] = $municipal->getDataMunicipal($reg->id_municipal);
                //Stancing the Action class 
                $action = new Action($this->dbh);
                $arrayData['action'] = $action->getDataAction($reg->id_action);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT country, local.longitude, local.latitude, municipal.designation AS municipal_designation, action.designation AS action_designation, action.code, action.cost FROM local JOIN municipal on municipal.id=local.id_municipal JOIN action ON action.id=local.id_action WHERE local.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['country'] = 'País: ' . $reg->country;
                $arrayData['longitude'] = 'Longitude:' . $reg->longitude;
                $arrayData['latitude'] = 'Latitude: ' . $reg->latitude;
                $arrayData['municipal_designation'] = 'Município: ' . $reg->municipal_designation;
                $arrayData['action_designation'] = 'Acção:' . $reg->action_designation;
                $arrayData['code'] = 'Código da acção: ' . $reg->code;
                $arrayData['cost'] = 'Custo da acção: ' . $reg->cost;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

}

?>