<?php

require_once 'Auditing.php';
require_once 'Employee.php';
require_once 'DayOffType.php';

class DayOff 
{

    public $id;
    public $initialDate;
    public $endDate;
    public $idEmployee;
    public $idDayOffType;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create day_off
    function registerDayOff() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO day_off VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->initialDate);
        $prep->bindparam(3, $this->endDate);
        $prep->bindparam(4, $this->idEmployee);
        $prep->bindparam(5, $this->idDayOffType);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Folga', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all day_off
    function readDayOff()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM day_off";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['initial_date'] = $reg->initial_date;
                $arrayData[$i]['end_date'] = $reg->end_date;
                //instanciate the Employree class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                //instanciate the dayOffType class
                $dayOffType = new DayOffType($this->dbh);
                $arrayData[$i]['day_off_type'] = $dayOffType->getDataDayOffType($reg->id_day_off_type);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined day_off
    function readDeterminedDayOff()
    {
        $arrayData = [];
        $cons = "SELECT * FROM day_off WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                //instanciate the Employree class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
                //instanciate the dayOffType class
                $dayOffType = new DayOffType($this->dbh);
                $arrayData['day_off_type'] = $dayOffType->getDataDayOffType($reg->id_day_off_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update day_off
    function updateDayOff() 
    {
        $arrayData = [];
        $cons = "UPDATE day_off SET initial_date = ?, end_date = ?, id_employee = ?, id_day_off_type = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->initialDate);
        $prep->bindparam(2, $this->endDate);
        $prep->bindparam(3, $this->idEmployee);
        $prep->bindparam(4, $this->idDayOffType);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Folga', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete day_off
    function deleteDayOff() 
    {
        $arrayData = [];
        $cons = "DELETE FROM day_off WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Folga', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data day_off
    function getDataDayOffType($id)
    {
        $arrayData = [];
        $cons = "SELECT * FROM day_off WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                //instanciate the Employree class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
                //instanciate the dayOffType class
                $dayOffType = new DayOffType($this->dbh);
                $arrayData['day_off_type'] = $dayOffType->getDataDayOffType($reg->id_day_off_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT day_off.initial_date, day_off.end_date, employee.full_name, employee.nationality,  employee.gender, employee.employee_code, day_off_type.designation, day_off_type.obs FROM day_off JOIN employee ON day_off.id_employee=employee.id JOIN day_off_type ON day_off.id_day_off_type= day_off_type.id WHERE day_off.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['initial_date'] = 'Início: ' . $reg->initial_date;
                $arrayData['end_date'] = 'Fim: ' . $reg->end_date;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário: ' . $reg->employee_code;
                $arrayData['designation'] = 'Tipo de folga: ' . $reg->designation;
                $arrayData['obs'] = 'Observação do tipo de folga: ' . $reg->obs;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>