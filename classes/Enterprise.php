<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class Enterprise 
{

    public $id;
    public $commercialDesignation;
    public $oficialDesignation;
    public $abbreviation;
    public $taxIdentificationNumber;
    public $address;
    public $telephone;
    public $email;
    public $registerNumber;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create enterprise
    function registerenterprise()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO enterprise VALUES(?,?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->commercialDesignation);
        $prep->bindparam(3, $this->oficialDesignation);
        $prep->bindparam(4, $this->abbreviation);
        $prep->bindparam(5, $this->taxIdentificationNumber);
        $prep->bindparam(6, $this->address);
        $prep->bindparam(7, $this->telephone);
        $prep->bindparam(8, $this->email);
        $prep->bindparam(9, $this->registerNumber);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Empresa', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all enterprise
    function readEnterprise() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM enterprise";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['commercial_designation'] = $reg->commercial_designation;
                $arrayData[$i]['oficial_designation'] = $reg->oficial_designation;
                $arrayData[$i]['abbreviation'] = $reg->abbreviation;
                $arrayData[$i]['tax_identification_number'] = $reg->tax_identification_number;
                $arrayData[$i]['address'] = $reg->address;
                $arrayData[$i]['telephone'] = $reg->telephone;
                $arrayData[$i]['email'] = $reg->email;
                $arrayData[$i]['register_number'] = $reg->register_number;
				$enterpriseLogo = new DocumentStorage('enterpriseLogo',$reg->id,$this->dbh);
				$enterpriseLogo = $enterpriseLogo->getDocument();
				$arrayData[$i]['enterpriseLogo'] = $enterpriseLogo;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined enterprise
    function readDeterminedEnterprise()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM enterprise WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['commercial_designation'] = $reg->commercial_designation;
                $arrayData['oficial_designation'] = $reg->oficial_designation;
                $arrayData['abbreviation'] = $reg->abbreviation;
                $arrayData['tax_identification_number'] = $reg->tax_identification_number;
                $arrayData['address'] = $reg->address;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['email'] = $reg->email;
                $arrayData['register_number'] = $reg->register_number;
				$enterpriseLogo = new DocumentStorage('enterpriseLogo',$reg->id,$this->dbh);
				$enterpriseLogo = $enterpriseLogo->getDocument();
				$arrayData['enterpriseLogo'] = $enterpriseLogo;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update enterprise
    function updateEnterprise() 
    {
        $arrayData = [];
        $cons = "UPDATE enterprise SET commercial_designation = ?,oficial_designation = ?, abbreviation = ?, tax_identification_number = ?, address = ?, telephone = ?, email = ?, register_number = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->commercialDesignation);
        $prep->bindparam(2, $this->oficialDesignation);
        $prep->bindparam(3, $this->abbreviation);
        $prep->bindparam(4, $this->taxIdentificationNumber);
        $prep->bindparam(5, $this->address);
        $prep->bindparam(6, $this->telephone);
        $prep->bindparam(7, $this->email);
        $prep->bindparam(8, $this->registerNumber);
        $prep->bindparam(9, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Empresa', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete enterprise
    function deleteEnterprise() 
    {
        $arrayData = [];
        $cons = "DELETE FROM enterprise WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Empresa', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// Delete enterprise with directory
	function deleteEnterpriseWithDirectory()
	{
        $del = $this->deleteEnterprise();
		if($del){
			//require_once 'DocumentStorage.php';
			//$deleteImage = new DocumentStorage('birthdayPersonImage',$this->id,$this->dbh);
			$deleteLogo = new DocumentStorage('enterpriseLogo',$this->id,$this->dbh);
			// Get repository path
			//$di = $deleteImage->getRepositoryPath();
			$dp = $deleteLogo->getRepositoryPath();
			// Remove a directory with all kind of contents
			//$deleteImage->removeDirectory($di);
			$deleteLogo->removeDirectory($dp);
		}
		return $del;
	}
	
	// Delete spefic enterprise document
	function deleteSpecificEnterpriseDocument($fileName)
	{	
		$removeDocument = new DocumentStorage('enterpriseLogo',$this->id,$this->dbh);
		// remove a specific file
		$rd = $removeDocument->removeSpecificFile($fileName);
		return $rd;
	}

    // Get data enterprise
    function getDataEnterprise($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM enterprise WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['commercial_designation'] = $reg->commercial_designation;
                $arrayData['oficial_designation'] = $reg->oficial_designation;
                $arrayData['abbreviation'] = $reg->abbreviation;
                $arrayData['tax_identification_number'] = $reg->tax_identification_number;
                $arrayData['address'] = $reg->address;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['email'] = $reg->email;
                $arrayData['register_number'] = $reg->register_number;
				$enterpriseLogo = new DocumentStorage('enterpriseLogo',$reg->id,$this->dbh);
				$enterpriseLogo = $enterpriseLogo->getDocument();
				$arrayData['enterpriseLogo'] = $enterpriseLogo;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        /* $cons = "SELECT *  FROM permition 
          JOIN municipal ON municipal.id = permition.id_municipal
          WHERE permition.id = ?"; */
        $cons = "SELECT * FROM enterprise WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['commercial_designation'] = "Designação comercial" . $reg->commercial_designation;
                $arrayData['oficial_designation'] = "Designação oficial" . $reg->oficial_designation;
                $arrayData['abbreviation'] = "abreviação" . $reg->abbreviation;
                $arrayData['tax_identification_number'] = "Taxa de identificação" . $reg->tax_identification_number;
                $arrayData['address'] = "Endereço" . $reg->address;
                $arrayData['telephone'] = "Telefone" . $reg->telephone;
                $arrayData['email'] = "Email" . $reg->email;
                $arrayData['register_number'] = "Número do registo" . $reg->register_number;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>