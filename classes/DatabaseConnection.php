<?php

class DatabaseConnection 
{

    private $host = "localhost";
    private $dbName = "rh";
    //private $dbPass = "intranet_mep@2023";
    private $dbPass = "";
    private $dbUserName = "root";
    //private $dbUserName = "intranet_mep";
    private $charset = 'utf8mb4';
    private $dsn;

    function tryConnect() 
    {
        try {
            $this->dsn = "mysql:host=$this->host;dbname=$this->dbName;charset=$this->charset";
            $dbh = new PDO($this->dsn, $this->dbUserName, $this->dbPass);
            $dbh->exec("set names utf8");
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $dbh;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

}

?>
