<?php

require_once 'Auditing.php';
require_once 'TrainingType.php';

class Training {

    public $id;
    public $initialDate;
    public $endDate;
    public $local;
    public $goal;
    public $idTrainingType;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create training
    function registerTraining() {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO training VALUES(?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->initialDate);
        $prep->bindparam(4, $this->endDate);
        $prep->bindparam(5, $this->local);
        $prep->bindparam(6, $this->goal);
        $prep->bindparam(7, $this->idTrainingType);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Formação', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all training
    function readTraining() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM training";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['initial_date'] = $reg->initial_date;
                $arrayData[$i]['end_date'] = $reg->end_date;
                $arrayData[$i]['local'] = $reg->local;
                $arrayData[$i]['goal'] = $reg->goal;
                $trainingType = new TrainingType($this->dbh);
                $arrayData[$i]['training_type'] = $trainingType->getDataTrainingType($reg->id_training_type);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined training
    function readDeterminedTraining() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM training WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['local'] = $reg->local;
                $arrayData['goal'] = $reg->goal;
                $trainingType = new TrainingType($this->dbh);
                $arrayData['training_type'] = $trainingType->getDataTrainingType($reg->id_training_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update training
    function updateTraining() {
        $arrayData = [];
        $cons = "UPDATE training SET designation = ?, initial_date = ?, end_date = ?, local = ?, goal = ?, id_training_type = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->initialDate);
        $prep->bindparam(3, $this->endDate);
        $prep->bindparam(4, $this->local);
        $prep->bindparam(5, $this->goal);
        $prep->bindparam(6, $this->idTrainingType);
        $prep->bindparam(7, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Formação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete training
    function deleteTraining() {
        $arrayData = [];
        $cons = "DELETE FROM training WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Formação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data training
    function getDataTraining($id) {
        $arrayData = [];
        $cons = "SELECT * FROM training WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['local'] = $reg->local;
                $arrayData['goal'] = $reg->goal;
                $trainingType = new TrainingType($this->dbh);
                $arrayData['training_type'] = $trainingType->getDataTrainingType($reg->id_training_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT training.designation AS training_designation, training.initial_date, training.end_date, training.local, training.goal, training_type.designation, training_type.obs FROM training JOIN training_type ON training.id_training_type= training_type.id WHERE training.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['training_designation'] = 'Designação: ' . $reg->training_designation;
                $arrayData['initial_date'] = 'Observação: ' . $reg->initial_date;
                $arrayData['end_date'] = 'Designação: ' . $reg->end_date;
                $arrayData['local'] = 'Observação: ' . $reg->local;
                $arrayData['goal'] = 'Observação: ' . $reg->goal;
                $arrayData['designation'] = 'Observação: ' . $reg->designation;
                $arrayData['obs'] = 'Observação: ' . $reg->obs;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>