<?php

require_once 'Auditing.php';
require_once 'AssessmentType.php';

class Assessment
{

    public $id;
    public $designation;
    public $fullMark;
    public $obs;
    public $idAssessmentType;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create assessment
    function registerAssessment() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO assessment VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->fullMark);
        $prep->bindparam(4, $this->obs);
        $prep->bindparam(5, $this->idAssessmentType);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Avaliação', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all assessment
    function readAssessment()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM assessment";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['full_mark'] = $reg->full_mark;
                $arrayData[$i]['obs'] = $reg->obs;
                //Instanciate the AssessmentType class
                $assessmentType = new AssessmentType($this->dbh);
                $arrayData[$i]['assessment_type'] = $assessmentType->getDataAssessmentType($reg->id_assessment_type);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined assessment
    function readDeterminedAssessment() 
    {
        $arrayData = [];
        $cons = "SELECT * FROM assessment WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['full_mark'] = $reg->full_mark;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the AssessmentType class
                $assessmentType = new AssessmentType($this->dbh);
                $arrayData['assessment_type'] = $assessmentType->getDataAssessmentType($reg->id_assessment_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update assessment
    function updateAssessment()
    {
        $arrayData = [];
        $cons = "UPDATE assessment SET designation = ?, full_mark = ?, obs = ?, id_assessment_type = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->fullMark);
        $prep->bindparam(3, $this->obs);
        $prep->bindparam(4, $this->idAssessmentType);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Avaliação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete assessment
    function deleteAssessment() 
    {
        $arrayData = [];
        $cons = "DELETE FROM assessment WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Avaliação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data assessment
    function getDataAssessment($id) 
    {
        $arrayData = [];
        $cons = "SELECT * FROM assessment WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['full_mark'] = $reg->full_mark;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the AssessmentType class
                $assessmentType = new AssessmentType($this->dbh);
                $arrayData['assessment_type'] = $assessmentType->getDataAssessmentType($reg->id_assessment_type);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT assessment.designation AS assessment_designation, assessment.full_mark, assessment.obs AS assessment_obs, assessment_type.designation, assessment_type.obs FROM assessment JOIN assessment_type ON assessment.id_assessment_type=assessment_type.id WHERE assessment.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['assessment_designation'] = 'Avaliação: ' . $reg->assessment_designation;
                $arrayData['full_mark'] = 'Nota máxima: ' . $reg->full_mark;
                $arrayData['assessment_obs'] = 'Observação: ' . $reg->assessment_obs;
                $arrayData['designation	'] = 'Tipo de avaliação: ' . $reg->designation;
                $arrayData['obs'] = 'Observação do tipo de avaliação: ' . $reg->obs;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>