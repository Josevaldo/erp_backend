<?php

require_once 'Auditing.php';
require_once 'Enterprise.php';

class Store 
{

    public $id;
    public $designation;
    public $telephone;
    public $address;
    public $email;
    public $idEnterprise;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create store
    function registerStore()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO store VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->telephone);
        $prep->bindparam(4, $this->address);
        $prep->bindparam(5, $this->email);
        $prep->bindparam(6, $this->idEnterprise);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Loja', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all store
    function readStore()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM store";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['telephone'] = $reg->telephone;
                $arrayData[$i]['address'] = $reg->address;
                $arrayData[$i]['email'] = $reg->email;
//                Instancing the Enterprise Class
                $enterprise = new Enterprise($this->dbh);
                $arrayData[$i]['enterprise'] = $enterprise->getDataEnterprise($reg->id_enterprise);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined store
    function readDeterminedStore()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM store WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['address'] = $reg->address;
                $arrayData['email'] = $reg->email;
//                                Instancing the Enterprise Class
                $enterprise = new Enterprise($this->dbh);
                $arrayData['enterprise'] = $enterprise->getDataEnterprise($reg->id_enterprise);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update store
    function updateStore() 
    {
        $arrayData = [];
        $cons = "UPDATE store SET designation = ?,telephone = ?, address = ?, email = ?, id_enterprise = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->telephone);
        $prep->bindparam(3, $this->address);
        $prep->bindparam(4, $this->email);
        $prep->bindparam(5, $this->idEnterprise);
        $prep->bindparam(6, $this->id);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Loja', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete store
    function deleteStore()
    {
        $arrayData = [];
        $cons = "DELETE FROM store WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Loja', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data store
    function getDataStore($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM store WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['address'] = $reg->address;
                $arrayData['email'] = $reg->email;
//                                                Instancing the Enterprise Class
                $enterprise = new Enterprise($this->dbh);
                $arrayData['enterprise'] = $enterprise->getDataEnterprise($reg->id_enterprise);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        /*
          JOIN municipal ON municipal.id = permition.id_municipal
          WHERE permition.id = ?"; */
        $cons = "SELECT store.designation AS store_designation, store.telephone AS store_telephone, store.address AS store_address, store.email AS store_email, enterprise.commercial_designation AS enterprise_commercial_designation , enterprise.oficial_designation AS enterprise_oficial_designation, enterprise.abbreviation AS enterprise_abbreviation , enterprise.tax_identification_number AS enterprise_tax_identification_number, enterprise.address AS enterprise_address , enterprise.telephone AS enterprise_telephone, enterprise.email AS enterprise_email FROM store JOIN enterprise on store.id_enterprise=enterprise.id WHERE store.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
//                $arrayData['id'] = "Identificador da loja: " . $reg->id;
                $arrayData['store_designation'] = "Designação da loja: " . $reg->store_designation;
                $arrayData['store_telephone'] = "Telefone da loja: " . $reg->store_telephone;
                $arrayData['store_address'] = "Endereço da loja: " . $reg->store_address;
                $arrayData['store_email'] = "Email Loja: " . $reg->store_email;
                $arrayData['enterprise_commercial_designation'] = "Designação da empresa: " . $reg->enterprise_commercial_designation;
                $arrayData['enterprise_oficial_designation'] = "Designação oficial da empresa: " . $reg->enterprise_oficial_designation;
                $arrayData['enterprise_abbreviation'] = "abreviação da empresa: " . $reg->enterprise_abbreviation;
                $arrayData['enterprise_tax_identification_number'] = "Taxa de identificação: " . $reg->enterprise_tax_identification_number;
                $arrayData['enterprise_address'] = "Endereço da empresa: " . $reg->enterprise_address;
                $arrayData['enterprise_telephone'] = "Telefone da empresa: " . $reg->enterprise_telephone;
                $arrayData['enterprise_email'] = "Email da empresa: " . $reg->enterprise_email;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>