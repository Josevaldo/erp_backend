<?php

require_once 'Auditing.php';
require_once 'UserToken.php';
require_once 'Profile.php';
require_once 'Store.php';

class User 
{

    public $id;
    public $completeName;
    public $name;
    public $telephone;
    public $email;
    public $password;
    public $license;
    public $idStore;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create user
    function registerUser() 
    {
        $arrayData = [];
        $cons = "INSERT INTO user VALUES(?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->completeName);
        $prep->bindparam(3, $this->name);
        $prep->bindparam(4, $this->telephone);
        $prep->bindparam(5, $this->email);
        $prep->bindparam(6, $this->password);
        $prep->bindparam(7, $this->license);
        $prep->bindparam(8, $this->idStore);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all user
    function readUser() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['complete_name'] = $reg->complete_name;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['telephone'] = $reg->telephone;
                $arrayData[$i]['email'] = $reg->email;
                $arrayData[$i]['password'] = $reg->password;
                $arrayData[$i]['license'] = $reg->license;
//                Stancing store class
                $userStore = new Store($this->dbh);
                $arrayData[$i]['store'] = $userStore->getDataStore($reg->id_store);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined user
    function readDeterminedUser()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['complete_name'] = $reg->complete_name;
                $arrayData['name'] = $reg->name;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['email'] = $reg->email;
                $arrayData['password'] = $reg->password;
                $arrayData['license'] = $reg->license;
                //                Stancing store class
                $userStore = new Store($this->dbh);
                $arrayData['store'] = $userStore->getDataStore($reg->id_store);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update user
    function updateUser() 
    {
        $arrayData = [];
        $cons = "UPDATE user SET complete_name = ?,name = ?,telephone = ?,email = ?, license = ?, id_store = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->completeName);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->telephone);
        $prep->bindparam(4, $this->email);
//		$prep->bindparam(5, $this->password);
        $prep->bindparam(5, $this->license);
        $prep->bindparam(6, $this->idStore);
        $prep->bindparam(7, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete user
    function deleteUser() 
    {
        $arrayData = [];
        $cons = "DELETE FROM user WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('utilizador', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Check if the email already exists
    function checkEmailExists() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user WHERE email = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->email, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }
            //$arrayData[] = $i;
            //return $arrayData;
            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Alter user's password
    function alterUserPassword()
    {
        $arrayData = [];
        $cons = "UPDATE user SET password = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->password);
        $prep->bindparam(2, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('utilizador', 'alterar senha', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Authenticate user
    function authenticateUser() 
    {
        $i = 0;
        $arrayData = [];
        $payload = [];
        $issueAt = time();
        $cons = "SELECT * FROM user WHERE email = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->email, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                if (password_verify($this->password, $reg->password)) {

                    $i++;
                    $payload = [
                        //'exp' => (new DateTime("now"))->getTimestamp(),
                        'iat' => $issueAt,
                        'uid' => $reg->id,
                        'email' => $reg->email,
                    ];
                    $this->id = $reg->id;
                    $this->idStore = $reg->id_store;
                }
            }
            //$userData = $this->readDeterminedUser();
            if ($this->id) {
                // Generate a token for a user
                //require_once 'UserToken.php';
                $userToken = new UserToken();
                $userToken->payload = $payload;
                $token = $userToken->generateToken();
                $arrayData['token'] = $token;
                // Insert data of user connected
                $userConnected = $userToken->userConnected($token, $this->id, $this->dbh);
                //return $token;
                // Read determined user
                $arrayData['user'] = $this->readDeterminedUser();
                // Get user profile
                $userProfile = new Profile($this->dbh);
                $arrayData['profile'] = $userProfile->getUserProfile($this->id);
            } else
                $arrayData = [];
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data of a spefic user
    function getDataUser($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['complete_name'] = $reg->complete_name;
                $arrayData['name'] = $reg->name;
                $arrayData['telephone'] = $reg->telephone;
                $arrayData['email'] = $reg->email;
                $arrayData['password'] = $reg->password;
                $arrayData['license'] = $reg->license;
                $arrayData['store'] = $reg->id_store;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT user.id AS user_id, user.complete_name AS user_complete_name , user.name AS user_name , user.telephone AS user_telephone , user.email AS user_email, user.license AS user_license , store.designation AS store_designation, store.telephone AS store_telephone, store.address AS store_address, store.email AS store_email FROM user JOIN store ON user.id_store=store.id WHERE user.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['user_id'] = "Identificador do utilizador: " . $reg->user_id;
                $arrayData['complete_name'] = "Nome completo: " . $reg->user_complete_name;
                $arrayData['name'] = "Nome: " . $reg->user_name;
                $arrayData['telephone'] = "Telefone do utilizador: " . $reg->user_telephone;
                $arrayData['email'] = "Email: " . $reg->user_email;
                $arrayData['password'] = "Licença: " . $reg->user_license;
                $arrayData['license'] = "Designação da loja: " . $reg->store_designation;
                $arrayData['store'] = "Telefone da loja: " . $reg->store_telephone;
                $arrayData['password'] = "Endereço da loja: " . $reg->store_address;
                $arrayData['license'] = "Email da loja: " . $reg->store_email;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>