<?php

require_once 'Auditing.php';
require_once 'Employee.php';

class OtherIormation 
{

    public $id;
    public $designation;
    public $initialYear;
    public $endYear;
    public $obs;
    public $idEmployee;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create other_formation
    function registerOtherFormation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO other_formation VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->initialYear);
        $prep->bindparam(4, $this->endYear);
        $prep->bindparam(5, $this->obs);
        $prep->bindparam(6, $this->idEmployee);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Outra formação', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all other_formation
    function readOtherFormation() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM other_formation";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['initial_year'] = $reg->initial_year;
                $arrayData[$i]['end_year'] = $reg->end_year;
                $arrayData[$i]['obs'] = $reg->obs;
                //Instanciate the Employee class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined otherFormation
    function readDeterminedOtherFormation()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM other_formation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_year'] = $reg->initial_year;
                $arrayData['end_year'] = $reg->end_year;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the Employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update other_formation
    function updateOtherFormation()
    {
        $arrayData = [];
        $cons = "UPDATE other_formation SET designation = ?, initial_year = ?, end_year = ?, obs = ?, id_employee =? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->initialYear);
        $prep->bindparam(3, $this->endYear);
        $prep->bindparam(4, $this->obs);
        $prep->bindparam(5, $this->idEmployee);
        $prep->bindparam(6, $this->id);

        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Outra formação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete other_formation
    function deleteOtherFormation()
    {
        $arrayData = [];
        $cons = "DELETE FROM other_formation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Outra formação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data other_formation
    function getDataOtherFormation($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM other_formation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['initial_year'] = $reg->initial_year;
                $arrayData['end_year'] = $reg->end_year;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the Employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT other_formation.designation, other_formation.initial_year, other_formation.end_year, other_formation.obs, employee.full_name, employee.nationality, employee.gender, employee.employee_code FROM other_formation JOIN employee ON other_formation.id_employee = employee.id WHERE other_formation.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['designation'] = 'Outra formação: ' . $reg->designation;
                $arrayData['initial_year'] = 'Início: ' . $reg->initial_year;
                $arrayData['end_year'] = 'Fim: ' . $reg->end_year;
                $arrayData['obs'] = 'Observação da outra formação: ' . $reg->obs;
                $arrayData['full_name'] = 'Funcionário: ' . $reg->full_name;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código do funcionário ' . $reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>