<?php

require_once 'Auditing.php';
require_once 'Employee.php';

class Career 
{

    public $id;
    public $description;
    public $initialDate;
    public $endDate;
    public $obs;
    public $idEmployee;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create career
    function registerCareer()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO career VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->initialDate);
        $prep->bindparam(4, $this->endDate);
        $prep->bindparam(5, $this->obs);
        $prep->bindparam(6, $this->idEmployee);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('carreira', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all career
    function readcareer() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM career";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['description'] = $reg->description;
                $arrayData[$i]['initial_date'] = $reg->initial_date;
                $arrayData[$i]['end_date'] = $reg->end_date;
                $arrayData[$i]['obs'] = $reg->obs;
                //Instanciate the employee
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined career
    function readDeterminedCareer()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM career WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['description'] = $reg->description;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the employee Type
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update career
    function updateCareer() 
    {
        $arrayData = [];
        $cons = "UPDATE career SET description = ?, initial_date = ?, end_date=?, obs = ?, id_employee = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->description);
        $prep->bindparam(2, $this->initialDate);
        $prep->bindparam(3, $this->endDate);
        $prep->bindparam(4, $this->obs);
        $prep->bindparam(5, $this->idEmployee);
        $prep->bindparam(6, $this->id);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('carreira', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete career
    function deleteCareer() 
    {
        $arrayData = [];
        $cons = "DELETE FROM career WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('carreira', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data career
    function getDataCareer($id)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM career WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['description'] = $reg->description;
                $arrayData['initial_date'] = $reg->initial_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['obs'] = $reg->obs;
                //Instanciate the employee
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM career 
				JOIN employee ON career.id_employee=employee.id 
				WHERE career.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['career_description'] = 'Description: ' . $reg->description;
                $arrayData['initial_date'] = 'Início: ' . $reg->initial_date;
                $arrayData['end_date'] = 'Fim: ' . $reg->end_date;
                $arrayData['obs'] = 'Observação: ' . $reg->obs;
                $arrayData['employee'] = 'Funcionário: ' . $reg->full_name;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>