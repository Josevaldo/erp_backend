<?php

require_once 'Auditing.php';
require_once 'Contract.php';
require_once 'Department.php';
require_once 'DocumentStorage.php';

class Employee 
{

    public $id;
    public $fullName;
    public $birthPlace;
    public $birthDate;
    public $nationality;
    public $gender;
    public $employeeCode;
    public $otherId;
    public $fatherName;
    public $motherName;
    public $email;
    public $address;
    public $idContract;
    public $idDepartment;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create employee
    function registerEmployee()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO employee VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->fullName);
        $prep->bindparam(3, $this->birthPlace);
        $prep->bindparam(4, $this->birthDate);
        $prep->bindparam(5, $this->nationality);
        $prep->bindparam(6, $this->gender);
        $prep->bindparam(7, $this->employeeCode);
        $prep->bindparam(8, $this->otherId);
        $prep->bindparam(9, $this->fatherName);
        $prep->bindparam(10, $this->motherName);
        $prep->bindparam(11, $this->email);
        $prep->bindparam(12, $this->address);
        $prep->bindparam(13, $this->idContract);
        $prep->bindparam(14, $this->idDepartment);
        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Funcionário', 'inserir', '', $dataAfterExecution);
            //return true;
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all employee
    function readEmployee()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM employee";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['full_name'] = $reg->full_name;
                $arrayData[$i]['birth_place'] = $reg->birth_place;
                $arrayData[$i]['birth_date'] = $reg->birth_date;
                $arrayData[$i]['nationality'] = $reg->nationality;
                $arrayData[$i]['gender'] = $reg->gender;
                $arrayData[$i]['employee_code'] = $reg->employee_code;
                $arrayData[$i]['other_id'] = $reg->other_id;
                $arrayData[$i]['father_name'] = $reg->father_name;
                $arrayData[$i]['mother_name'] = $reg->mother_name;
                $arrayData[$i]['email'] = $reg->email;
                $arrayData[$i]['address'] = $reg->address;
                //Instanciate the Contract
                $contract = new Contract($this->dbh);
                $arrayData[$i]['contract'] = $contract->getDataContract($reg->id_contract);
				//Instanciate the Department
                $department = new Department($this->dbh);
                $arrayData[$i]['department'] = $department->getDataDepartment($reg->id_department);
				$employeePhoto = new DocumentStorage('employeePhoto',$reg->id,$this->dbh);
				$employeePhoto = $employeePhoto->getDocument();
				$employeeArchive = new DocumentStorage('employeeArchive',$reg->id,$this->dbh);
				$employeeArchive = $employeeArchive->getDocument();
				$arrayData[$i]['employeePhoto'] = $employeePhoto;
				$arrayData[$i]['employeeArchive'] = $employeeArchive;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined employee
    function readDeterminedEmployee()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM employee WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['full_name'] = $reg->full_name;
                $arrayData['birth_place'] = $reg->birth_place;
                $arrayData['birth_date'] = $reg->birth_date;
                $arrayData['nationality'] = $reg->nationality;
                $arrayData['gender'] = $reg->gender;
                $arrayData['employee_code'] = $reg->employee_code;
                $arrayData['other_id'] = $reg->other_id;
                $arrayData['father_name'] = $reg->father_name;
                $arrayData['mother_name'] = $reg->mother_name;
				$arrayData['email'] = $reg->email;
                $arrayData['address'] = $reg->address;
                //Instanciate the Contract
                $contract = new Contract($this->dbh);
                $arrayData['contract'] = $contract->getDataContract($reg->id_contract);
				//Instanciate the Department
                $department = new Department($this->dbh);
                $arrayData['department'] = $department->getDataDepartment($reg->id_department);
				$employeePhoto = new DocumentStorage('employeePhoto',$reg->id,$this->dbh);
				$employeePhoto = $employeePhoto->getDocument();
				$employeeArchive = new DocumentStorage('employeeArchive',$reg->id,$this->dbh);
				$employeeArchive = $employeeArchive->getDocument();
				$arrayData['employeePhoto'] = $employeePhoto;
				$arrayData['employeeArchive'] = $employeeArchive;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update employee
    function updateEmployee() 
    {
        $arrayData = [];
        $cons = "UPDATE employee SET full_name = ?, birth_place = ?, birth_date = ?, nationality = ?, gender = ?, employee_code = ?, other_id = ?, father_name = ?, mother_name = ?,email = ?,address = ?, id_contract = ?,id_department = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->fullName);
        $prep->bindparam(2, $this->birthPlace);
        $prep->bindparam(3, $this->birthDate);
        $prep->bindparam(4, $this->nationality);
        $prep->bindparam(5, $this->gender);
        $prep->bindparam(6, $this->employeeCode);
        $prep->bindparam(7, $this->otherId);
        $prep->bindparam(8, $this->fatherName);
        $prep->bindparam(9, $this->motherName);
        $prep->bindparam(10, $this->email);
        $prep->bindparam(11, $this->address);
        $prep->bindparam(12, $this->idContract);
        $prep->bindparam(13, $this->idDepartment);
        $prep->bindparam(14, $this->id);
        //$prep->execute();
        // Get data before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Funcionário', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete employee
    function deleteEmployee() 
    {
        $arrayData = [];
        $cons = "DELETE FROM employee WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class permition
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Funcionário', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }
	
	// Delete employee with directory
	function deleteEmployeeWithDirectory()
	{
        $del = $this->deleteEmployee();
		if($del){
			//require_once 'DocumentStorage.php';
			//$deleteImage = new DocumentStorage('birthdayPersonImage',$this->id,$this->dbh);
			$deletePhoto = new DocumentStorage('employeePhoto',$this->id,$this->dbh);
			// Get repository path
			//$di = $deleteImage->getRepositoryPath();
			$dp = $deletePhoto->getRepositoryPath();
			// Remove a directory with all kind of contents
			//$deleteImage->removeDirectory($di);
			$deletePhoto->removeDirectory($dp);
		}
		return $del;
	}
	
	// Delete spefic employee document
	function deleteSpecificEmployeeDocument($fileName)
	{	
		$removeDocument = new DocumentStorage('employeeArchive',$this->id,$this->dbh);
		// remove a specific file
		$rd = $removeDocument->removeSpecificFile($fileName);
		return $rd;
	}
	
	// Check if the email already exists
	function checkEmailExists()
	{
		$i = 0;
		$cons = "SELECT * FROM employee WHERE email = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->email, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$i++;
			}
			return $i;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			return false;
		}
	}
	
	// Get data employee from other ID
    function getDataEmployeeFromOtherId($otherId) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM employee WHERE other_id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $otherId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['full_name'] = $reg->full_name;
                $arrayData['birth_place'] = $reg->birth_place;
                $arrayData['birth_date'] = $reg->birth_date;
                $arrayData['nationality'] = $reg->nationality;
                $arrayData['gender'] = $reg->gender;
                $arrayData['employee_code'] = $reg->employee_code;
                $arrayData['other_id'] = $reg->other_id;
                $arrayData['father_name'] = $reg->father_name;
                $arrayData['mother_name'] = $reg->mother_name;
				$arrayData['email'] = $reg->email;
                $arrayData['address'] = $reg->address;
                //Instanciate the Contract
                $contract = new Contract($this->dbh);
                $arrayData['Contract'] = $contract->getDataContract($reg->id_contract);
				//Instanciate the Department
                $department = new Department($this->dbh);
                $arrayData['department'] = $department->getDataDepartment($reg->id_department);
				$employeePhoto = new DocumentStorage('employeePhoto',$reg->id,$this->dbh);
				$employeePhoto = $employeePhoto->getDocument();
				$employeeArchive = new DocumentStorage('employeeArchive',$reg->id,$this->dbh);
				$employeeArchive = $employeeArchive->getDocument();
				$arrayData['employeePhoto'] = $employeePhoto;
				$arrayData['employeeArchive'] = $employeeArchive;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data employee
    function getDataEmployee($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM employee WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['full_name'] = $reg->full_name;
                $arrayData['birth_place'] = $reg->birth_place;
                $arrayData['birth_date'] = $reg->birth_date;
                $arrayData['nationality'] = $reg->nationality;
                $arrayData['gender'] = $reg->gender;
                $arrayData['employee_code'] = $reg->employee_code;
                $arrayData['other_id'] = $reg->other_id;
                $arrayData['father_name'] = $reg->father_name;
                $arrayData['mother_name'] = $reg->mother_name;
				$arrayData['email'] = $reg->email;
                $arrayData['address'] = $reg->address;
                //Instanciate the Contract
                $contract = new Contract($this->dbh);
                $arrayData['Contract'] = $contract->getDataContract($reg->id_contract);
				//Instanciate the Department
                $department = new Department($this->dbh);
                $arrayData['department'] = $department->getDataDepartment($reg->id_department);
				$employeePhoto = new DocumentStorage('employeePhoto',$reg->id,$this->dbh);
				$employeePhoto = $employeePhoto->getDocument();
				$employeeArchive = new DocumentStorage('employeeArchive',$reg->id,$this->dbh);
				$employeeArchive = $employeeArchive->getDocument();
				$arrayData['employeePhoto'] = $employeePhoto;
				$arrayData['employeeArchive'] = $employeeArchive;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT employee.full_name, employee.birth_date, employee.nationality, employee.gender, employee.employee_code, contract.designation, contract.initial_date, 
				contract.end_date, contract.contract_code,department.sigla AS sigla,employee.email,employee.address FROM employee 
				JOIN contract ON employee.id_contract= contract.id WHERE employee.id = ?
				JOIN department ON department.id_department= department.id 
				WHERE employee.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['full_name'] = 'Nome completo: ' . $reg->full_name;
                $arrayData['birth_date'] = 'Data de nascimento: ' . $reg->birth_date;
                $arrayData['nationality'] = 'Nacionalidade: ' . $reg->nationality;
                $arrayData['gender'] = 'Sexo: ' . $reg->gender;
                $arrayData['employee_code'] = 'Código: ' . $reg->employee_code;
                $arrayData['designation'] = 'Contratop: ' . $reg->designation;
                $arrayData['initial_date'] = 'Início do contrato: ' . $reg->initial_date;
                $arrayData['end_date'] = 'Fim do contrato: ' . $reg->end_date;
                $arrayData['contract_code'] = 'Código do contrato: ' . $reg->contract_code;
                $arrayData['department'] = 'Departamento: ' . $reg->sigla;
                $arrayData['address'] = 'Endereço: ' . $reg->address;
                $arrayData['email'] = 'Email: ' . $reg->email;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>