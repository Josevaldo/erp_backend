<?php

require_once 'Auditing.php';
require_once 'Office.php';
require_once 'Salary.php';
require_once 'MeasurementUnitSalary.php';

class FixedSalary
{

    public $id;
    public $idOffice;
    public $idSalary;
    public $value;
    public $idMeasurementUnitSalary;
    public $obs;
    public $dbh;

    function __construct($dbh)
    {
        $this->dbh = $dbh;
    }

    // Create fixed_salary
    function registerFixedSlary() 
    {
        $arrayData = [];
        $cons = "INSERT INTO fixed_salary VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idOffice);
        $prep->bindparam(2, $this->idSalary);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->idMeasurementUnitSalary);
        $prep->bindparam(5, $this->obs);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idOffice, $this->idSalary);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Salário fixado', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all fixed_salary
    function readFixedSalary()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fixed_salary";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['value'] = $reg->value;
                $arrayData[$i]['obs'] = $reg->obs;
                // Get data of a spefic Office
                $office = new Office($this->dbh);
                $arrayData[$i]['office'] = $office->getDataOffice($reg->id_office);
                // Get data of a spefic salary
                $salary = new Salary($this->dbh);
                $arrayData[$i]['salary'] = $salary->getDataSalary($reg->id_salary);
                // Get data of a spefic MeasurementUnitSalary 
                $measurementIUnitSalary = new MeasurementUnitSalary($this->dbh);
                $arrayData[$i]['measurement_unit_salary'] = $measurementIUnitSalary->getDataMeasurementUnitSalary($reg->id_measutement_unit_salary);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined fixed_Salary
    function readDeterminedFixedSalary() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fixed_salary WHERE id_office = ? AND id_salary = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['value'] = $reg->value;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic Office
                $office = new Office($this->dbh);
                $arrayData['office'] = $office->getDataOffice($reg->id_office);
                // Get data of a spefic salary
                $salary = new Salary($this->dbh);
                $arrayData['salary'] = $salary->getDataSalary($reg->id_salary);
                // Get data of a spefic MeasurementUnitSalary 
                $measurementIUnitSalary = new MeasurementUnitSalary($this->dbh);
                $arrayData['measurement_unit_salary'] = $measurementIUnitSalary->getDataMeasurementUnitSalary($reg->id_measutement_unit_salary);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update fixed_salary
    function updateFixedSalary() 
    {
        $arrayData = [];
        $cons = "UPDATE fixed_salary SET id_office = ?, id_salary = ?, value = ?, id_measutement_unit_salary = ?, obs = ? WHERE id_office = ? AND id_salary = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idOffice);
        $prep->bindparam(2, $this->idSalary);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->idMeasurementUnitSalary);
        $prep->bindparam(5, $this->obs);
        $prep->bindparam(6, $this->id[0]);
        $prep->bindparam(7, $this->id[1]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            $this->id = array($this->idOffice, $this->idSalary);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Salário fixado', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete fixed_salary
    function deleteFixedSalary()
    {
        $arrayData = [];
        $cons = "DELETE FROM fixed_salary WHERE id_office = ? AND id_salary = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Salário fixado', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get user profile
    function getUserProfile($idUser)
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile 
				JOIN permition ON permition.id = profile.id_permition
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['obs'] = $reg->obs;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Assigned permitions to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $permition)
    {
        $i = 0;
        if ((is_array($permition)) || (!empty($permition))) {
            $this->idUser = $idUserCreated;
            $this->obs = "Perfil criado no momento do registo Utilizador";
            foreach ($permition as $rr) {
                $i++;
                $this->idPermition = $rr;
                $this->registerProfile();
            }
        }
    }

    // Get data fixed_salary
    function getDataFixedSalary($id) 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fixed_salary WHERE id_office = ? AND id_salary = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['value'] = $reg->value;
                $arrayData['obs'] = $reg->obs;
                // Get data of a spefic Office
                $office = new Office($this->dbh);
                $arrayData['office'] = $office->getDataOffice($reg->id_office);
                // Get data of a spefic salary
                $salary = new Salary($this->dbh);
                $arrayData['salary'] = $salary->getDataSalary($reg->id_salary);
                // Get data of a spefic MeasurementUnitDeduction 
                $measurementIUnitSalary = new MeasurementUnitSalary($this->dbh);
                $arrayData['measurement_unit_salary'] = $measurementIUnitSalary->getDataMeasurementUnitSalary($reg->id_measurement_unit_deduction);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId)
    {
        $dataReceivedFormated = '';
        $cons = "SELECT fixed_salary.value, fixed_salary.obs AS fixed_salary_obs, salary.designation AS salary_designation, salary.obs AS salary_obs , office.designation AS office_designation , office.obs AS office_obs, measurement_unit_salary.designation ASmeasurement_unit_salary_designation, measurement_unit_salary.symbol FROM fixed_salary JOIN office ON fixed_salary.id_office=office.id JOIN salary ON fixed_salary.id_salary=salary.id JOIN measurement_unit_salary ON fixed_salary.id_measutement_unit_salary=measurement_unit_salary.id WHERE id_office = ? AND id_salary = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['value'] = 'Valor do salário fixado: ' . $reg->value;
                $arrayData['fixed_salary_obs'] = 'Observação do salário fixado: ' . $reg->fixed_salary_obs;
                $arrayData[''] = 'Salário: ' . $reg->salary_designation	;
                $arrayData['salary_obs'] = 'Observação do salário: ' . $reg->salary_obs;
                $arrayData['office_designation'] = 'Cargo: ' . $reg->office_designation;
                $arrayData['office_obs	'] = 'Observação do cargo: ' . $reg->office_obs;
                //$arrayData['measurement_unit_salary_designation'] = 'Medida do salário: ' . $reg->measurement_unit_salary_designation;
                $arrayData['symbol'] = 'Símbolo da medida: ' . $reg->symbol;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>