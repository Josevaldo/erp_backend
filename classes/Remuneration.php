<?php

require_once 'Auditing.php';
require_once 'Processing.php';
require_once 'Office.php';
require_once 'Salary.php';
require_once 'Employee.php';

class Remuneration
{

    public $id;
    public $idProcessing;
    public $idOffice;
    public $idSalary;
    public $idEmployee;
    public $numberSalary;
    public $value;
    public $dbh;

    function __construct($dbh) 
    {
        $this->dbh = $dbh;
    }

    // Create remuneration
    function registerRemuneration()
    {
        $i = 0;
        $arrayData = [];
        $cons = "INSERT INTO remuneration VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProcessing);
        $prep->bindparam(2, $this->idOffice);
        $prep->bindparam(3, $this->idSalary);
        $prep->bindparam(4, $this->idEmployee);
        $prep->bindparam(5, $this->numberSalary);
        $prep->bindparam(6, $this->value);

        //$prep->execute();
        try {
            $prep->execute();
            $i++;
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of permition before and after the execution of an action
            $this->id = [$this->idProcessing, $this->idOffice, $this->idSalary, $this->idEmployee];
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Remuneração', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read all remuneration
    function readRemuneration()
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM remuneration";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['number_salary'] = $reg->number_salary;
                $arrayData[$i]['value'] = $reg->value;
                //Instanciate the processing class
                $processing = new Processing($this->dbh);
                $arrayData[$i]['processing'] = $processing->getDataProcessing($reg->id_processing);
                //Instanciate the Office class
                $office = new Office($this->dbh);
                $arrayData[$i]['Office'] = $office->getDataOffice($reg->id_office);
                //Instanciate the Salary class
                $salary = new Salary($this->dbh);
                $arrayData[$i]['salary'] = $salary->getDataSalary($reg->id_salary);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData[$i]['employee'] = $employee->getDataEmployee($reg->id_employee);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Read determined remuneration
    function readDeterminedRemuneration() 
    {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM remuneration WHERE id_processing = ? AND id_office = ? AND id_salary = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        $prep->bindparam(4, $this->id[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['number_salary'] = $reg->number_salary;
                $arrayData['value'] = $reg->value;
                //Instanciate the processing class
                $processing = new Processing($this->dbh);
                $arrayData['processing'] = $processing->getDataProcessing($reg->id_processing);
                //Instanciate the Office class
                $office = new Office($this->dbh);
                $arrayData['Office'] = $office->getDataOffice($reg->id_office);
                 //Instanciate the Salary class
                $salary = new Salary($this->dbh);
                $arrayData[$i]['salary'] = $salary->getDataSalary($reg->id_salary);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Update remuneration
    function updateRemuneration()
    {
        $arrayData = [];
        $cons = "UPDATE remuneration SET id_processing = ?, id_office = ?, id_salary = ?, id_employee = ?, number_salary = ?, value = ? WHERE id_processing = ? AND id_office = ? AND id_salary = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProcessing);
        $prep->bindparam(2, $this->idOffice);
        $prep->bindparam(3, $this->idSalary);
        $prep->bindparam(4, $this->idEmployee);
        $prep->bindparam(5, $this->numberSalary);
        $prep->bindparam(6, $this->value);
        $prep->bindparam(7, $this->id[0]);
        $prep->bindparam(8, $this->id[1]);
        $prep->bindparam(9, $this->id[2]);
        $prep->bindparam(10, $this->id[3]);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of permition before and after the execution of an action
            $this->id = [$this->idProcessing, $this->idOffice, $this->idSalary, $this->idEmployee];
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Remuneração', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Delete remuneration
    function deleteRemuneration() 
    {
        $arrayData = [];
        $cons = "DELETE FROM remuneration WHERE id_processing = ? AND id_office = ? AND id_salary = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        $prep->bindparam(4, $this->id[3], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of permition before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        $this->id = [$this->idProcessing, $this->idOffice, $this->idSalary, $this->idEmployee];
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Processamento da dedução', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data remuneration
    function getDataRemuneration($id) 
    {
        $arrayData = [];
        $cons = "SELECT * FROM remuneration WHERE id_processing = ? AND id_office = ? AND id_salary = ? AND id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $id[2], PDO::PARAM_STR);
        $prep->bindparam(4, $id[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['number_salary'] = $reg->number_salary;
                $arrayData['value'] = $reg->value;
                //Instanciate the processing class
                $processing = new Processing($this->dbh);
                $arrayData['processing'] = $processing->getDataProcessing($reg->id_processing);
                //Instanciate the Office class
                $office = new Office($this->dbh);
                $arrayData['Office'] = $office->getDataOffice($reg->id_office);
                 //Instanciate the Salary class
                $salary = new Salary($this->dbh);
                $arrayData['salary'] = $salary->getDataSalary($reg->id_salary);
                //Instanciate the employee class
                $employee = new Employee($this->dbh);
                $arrayData['employee'] = $employee->getDataEmployee($reg->id_employee);
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $arrayData['db_error'] = $e->getMessage();
            return $arrayData;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) 
    {
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT remuneration.number_salary, remuneration.value, processing.designation AS processing_designation, processing.processing_date, office.designation AS office_designation,salary.designation AS salary_designation, employee.full_name, employee.nationality, employee.gender, employee.employee_code FROM remuneration JOIN processing ON remuneration.id_processing=processing.id JOIN office ON remuneration.id_office=office.id JOIN salary ON remuneration.id_salary=salary.id JOIN employee ON remuneration.id_employee=employee.id WHERE remuneration.id_processing = ? AND remuneration.id_office = ? AND remuneration.id_salary = ? AND remuneration.id_employee = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        $prep->bindparam(3, $DataId[2], PDO::PARAM_STR);
        $prep->bindparam(4, $DataId[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['number_salary'] = "Número do salário: ".$reg->number_salary;
                $arrayData['value'] = "Valor: ".$reg->value;
                $arrayData['processing_designation'] = "Processamento: ". $reg->processing_designation;
                $arrayData['processing_date'] = "Data do processamento: ".$reg->processing_date;
                $arrayData['office_designation'] = "Cargo: ".$reg->office_designation;
                $arrayData['salary_designation'] = "Salário: ".$reg->salary_designation;
                $arrayData['full_name'] = "Funcionário: ".$reg->full_name;
                $arrayData['nationality'] = "Nacionalidade: ".$reg->nationality;
                $arrayData['gender'] = "Sexo: ".$reg->gender;
                $arrayData['employee_code'] = "Código do funcionário: ".$reg->employee_code;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            $dataReceivedFormated = $e->getMessage();
            return $dataReceivedFormated;
        }
    }

}

?>