<?php
class UserToken
{
	public $userHttpHeader;
	public $payload;
	public $idUser;
	private $userKey = 'SIP@2023';
	
	/*function __construct($header,$payload)
	{
		$this->userHeader = $header;
		$this->payload = $payload;
	}*/
	
	// Generate a token for a user
    function generateToken()
	{
		if($this->payload){
			//Application Key
			//$key = 'Test';

			//Header Token
			$header = [
				'typ' => 'JWT',
				'alg' => 'HS256'
			];

			//Payload - Content
			/*$payload = [
				'exp' => (new DateTime("now"))->getTimestamp(),
				'uid' => 1,
				'email' => 'email@email.com',
			];*/

			//JSON
			$header = json_encode($header);
			$payload = json_encode($this->payload);

			//Base 64
			$header = base64_encode($header);
			$payload = base64_encode($payload);

			//Sign
			$sign = hash_hmac('sha256', $header . "." . $payload, $this->userKey, true);
			$sign = base64_encode($sign);

			//Token
			$token = $header . '.' . $payload . '.' . $sign;
		}else $token = false;
		return $token;
	}
	
	// Validate the sign of a token 
    function validateSignToken()
	{
		$bearer = explode(' ',$this->userHttpHeader['Authorization']);
		//$bearer[0] = 'bearer';
		//$bearer[1] = 'token jwt';
		$token = explode('.',$bearer[1]);
		/*$header = $token[0];
		$payload = $token[1];
		$sign = $token[2];*/
		if(empty($token[0])) $header = '';
		else $header = $token[0];
		if(empty($token[1])) $payload = '';
		else $payload = $token[1];
		if(empty($token[2])) $sign = '';
		else $sign = $token[2];
		// Check sign 
		$validate = hash_hmac('sha256', $header . "." . $payload, $this->userKey, true);
		$validate = base64_encode($validate);
		if($sign === $validate) return true;
		else return false;
	}
	
	// Validate the expiration of a token 
    function validateExpirationToken()
	{
		$bearer = explode(' ',$this->userHttpHeader['Authorization']);
		//$bearer[0] = 'bearer';
		//$bearer[1] = 'token jwt';
		$token = explode('.',$bearer[1]);
		$header = base64_decode($token[0]);
		$payload = base64_decode($token[1]);
		$payload = json_decode($payload);
		foreach($payload as $k => $v){
			//$v .= $x." = ".$val;
			if($k == "iat"){
				$userIat = $v;
				$limit = 3600;
				$expiration = $userIat + $limit;
				$actualTime = time();
				if($expiration > $actualTime) $validate = true;
				else $validate = false;
			}
		}
		return $validate;
	}
	
	// Get the authorization to access resource
	function getAuthorization()
	{
		// Get header sent by the user 
		$this->userHttpHeader = apache_request_headers();
		// Validate the sign of a token 
		if(empty($this->userHttpHeader['Authorization'])) $authorization = false;
		else{
			$validateSign = $this->validateSignToken();
			if($validateSign){
				// Validate the expiration of a token 
				$validateExpiration = $this->validateExpirationToken();
				if($validateExpiration) $authorization = true;
				else $authorization = false;
			}else $authorization = false;
		}
		return $authorization;
	}
	
	// Insert data of user connected
	function userConnected($token,$idUser,$dbh)
	{
		$date_created = date("Y-m-d");
		$id = NULL;
		$cons = "INSERT INTO user_connected VALUES(?,?,?,?)";
		$prep = $dbh->prepare($cons);
		$prep->bindparam(1, $id);
		$prep->bindparam(2, $token);
		$prep->bindparam(3, $date_created);
		$prep->bindparam(4, $idUser);
		//$prep->execute();
		try{
			$prep->execute();
			//record inserted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			return false;
		}
	}
	
	// Get the user associated with the actual token 
    function getUserAssociatedWithToken($dbh)
	{
		// Get header sent by the user 
		$this->userHttpHeader = apache_request_headers();
		$bearer = array();
		if(empty($this->userHttpHeader['Authorization'])){
			$userName = 'Internauta';
			return $userName;
		}else{
			$bearer = explode(' ',$this->userHttpHeader['Authorization']);
			$i = 0;
			$userName = '';
			$token = $bearer[1];
			$cons = "SELECT * FROM user_connected 
					JOIN user ON user_connected.id_user = user.id 
					WHERE token_created = ?";
			$prep = $dbh->prepare($cons);
			$prep->bindparam(1, $token);
			//$prep->execute();
			try{
				$prep->execute();
				while($reg = $prep->fetch(PDO::FETCH_OBJ)){
					$i++;
					$userName = $reg->name;
				}
				return $userName;
			}catch(Exception $e){
				//Some error occured. (i.e. violation of constraints)
				return false;
			}
		}
	}
}
?>