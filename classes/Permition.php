<?php
require_once 'Auditing.php';
class Permition
{
	public $id;
	public $comment;
	public $designation;
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Create permition
    function registerPermition()
	{
		$i = 0;
		$arrayData = [];
		$cons = "INSERT INTO permition VALUES(?,?,?)";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id);
		$prep->bindparam(2, $this->designation);
		$prep->bindparam(3, $this->comment);
		//$prep->execute();
		try{
			$prep->execute();
			$i++;
			//record inserted
			// Insert data in the auditing file
			$lastId = $this->dbh->lastInsertId();
			// Get data of permition before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
			// instance the class Auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('permissão','inserir','',$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read all permition
	function readPermition()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM permition";
		$prep = $this->dbh->prepare($cons);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				$arrayData[$i]['id'] = $reg->id;
				$arrayData[$i]['comment'] = $reg->comment;
				$arrayData[$i]['designation'] = $reg->designation;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Read determined permition
	function readDeterminedPermition()
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM permition WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['comment'] = $reg->comment;
				$arrayData['designation'] = $reg->designation;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Update permition
	function updatePermition()
	{
	    $arrayData = [];
		$cons = "UPDATE permition SET designation = ?,comment = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->designation);
		$prep->bindparam(2, $this->comment);
		$prep->bindparam(3, $this->id);
		//$prep->execute();
		// Get data of permition before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		try{
			$prep->execute();
			//record update
			// Get data of permition before and after the execution of an action
			$dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
			// instance the class auditing
			$auditing = new Auditing($this->dbh);
			$response = $auditing->insertDataAuditingFile('permissão','alterar',$dataBeforeExecution,$dataAfterExecution);
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			// return false;
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Delete permition
	function deletePermition()
	{
        $arrayData = [];
		$cons = "DELETE FROM permition WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $this->id, PDO::PARAM_STR);
		//$prep->execute();
		// Get data of permition before and after the execution of an action
		$dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
		// instance the class permition
		$auditing = new Auditing($this->dbh);
		$response = $auditing->insertDataAuditingFile('permissão','eliminar',$dataBeforeExecution,'');
		try{
			$prep->execute();
			//record deleted
			return true;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
	}
	
	// Get data permition
	function getDataPermition($id)
	{
		$i = 0;
		$arrayData = [];
		$cons = "SELECT * FROM permition WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $id, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				$arrayData['id'] = $reg->id;
				$arrayData['comment'] = $reg->comment;
				$arrayData['designation'] = $reg->designation;
				$i++;
			}
			return $arrayData;
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$arrayData['db_error'] = $e->getMessage();
			return $arrayData;
		}
		
	}
	
	// Get data before and after the execution of an action
	function getDataBeforeAfterAction($DataId)
	{
		$i = 0;
		$arrayData = [];
		$dataReceivedFormated = '';
		/*$cons = "SELECT *  FROM permition 
				JOIN municipal ON municipal.id = permition.id_municipal
				WHERE permition.id = ?";*/
		$cons = "SELECT * FROM permition WHERE id = ?";
		$prep = $this->dbh->prepare($cons);
		$prep->bindparam(1, $DataId, PDO::PARAM_STR);
		try{
			$prep->execute();
			while($reg = $prep->fetch(PDO::FETCH_OBJ)){
				//$i++;
				//$arrayData['id'] = 'Identificador: '.$reg->id;
				$arrayData['comment'] = 'Acrónimo: '.$reg->comment;
				$arrayData['designation'] = 'designation: '.$reg->designation;
				$i++;
			}
			//Format data of the system element
			if($arrayData){
				foreach($arrayData as $dr){
					$dataReceivedFormated .= $dr.', ';
				}
				$dataReceivedFormated = substr($dataReceivedFormated,0,-2);
			}else $dataReceivedFormated = '';
			return $dataReceivedFormated;
			
		}catch(Exception $e){
			//Some error occured. (i.e. violation of constraints)
			$dataReceivedFormated = $e->getMessage();
			return $dataReceivedFormated;
		}
	}
}
?>