<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/DeductionProcessing.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class deductionprocessing
$dedutionProcessing = new DeductionProcessing($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $dedutionProcessing->idProcessing = $data->id_processing;
        $dedutionProcessing->idOffice = $data->id_office;
        $dedutionProcessing->idDeduction = $data->id_deduction;
        $dedutionProcessing->idEmployee = $data->id_employee;
        $dedutionProcessing->numberDeduction = $data->number_deduction;
        $dedutionProcessing->value = $data->value;
        $response = $dedutionProcessing->registerDeductionProcessing();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Processamento da dedução registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Processamento da dedução não registado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $dedutionProcessing->readDeductionProcessing(); // Read all processing
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Processamento da dedução encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhum processamento da dedução encontrado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
        // Update deductionprocessing
        $dedutionProcessing->id = [$data->id_processing, $data->id_office, $data->id_deduction, $data->id_employee];
        $dedutionProcessing->idProcessing = $data->id_processing;
        $dedutionProcessing->idOffice = $data->id_office;
        $dedutionProcessing->idDeduction = $data->id_deduction;
        $dedutionProcessing->idEmployee = $data->id_employee;
        $dedutionProcessing->numberSalary = $data->number_salary;
        $dedutionProcessing->value = $data->value;
        $response = $dedutionProcessing->updateDeductionProcessing();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Processamento da dedução actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Processamento da dedução não actualizado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $dedutionProcessing->id = $id;
            // Retrieve the response about the delete of deductionprocessing
            $response = $dedutionProcessing->deleteDeductionProcessing();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Processamento da dedução eliminado com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Processamento da dedução não eliminado', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>