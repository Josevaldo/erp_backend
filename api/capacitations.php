<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Capacitation.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class capacitation
$capacitation = new Capacitation($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $capacitation->idTraining = $data->id_training;
        $capacitation->idEmployee = $data->id_employee;
        $capacitation->obs = $data->obs;
        $capacitation->status = 'Pendente';
        $response = $capacitation->registerCapacitation();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Capacitação registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Capacitação não registada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('GET' === $method) {
        $response = $capacitation->readCapacitation(); // Read all capacitation
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Capacitação encontrada', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhuma capacitação encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('PUT' === $method) {
        // Update capacitation
        $capacitation->id = $data->id;
        $capacitation->idTraining = $data->id_training;
        $capacitation->idEmployee = $data->id_employee;
        $capacitation->obs = $data->obs;
        $response = $capacitation->updateCapacitation();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Capacitação actualizada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Capacitação não actualizada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('DELETE' === $method) {
        // Delete capacitation
        //$profile->id = $data->id;

        foreach ($data->id as $id) {
            $capacitation->id = $id;
            // Retrieve the response about the delete of capacitation
            $response = $capacitation->deleteCapacitation();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Capacitação eliminada com successo', $response);
                else
                    $responseReturned = $returned->returnResult(false, 'Capacitaç~sao não eliminada', array());
            } else {
                $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
				//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>