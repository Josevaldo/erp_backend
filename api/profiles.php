<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Profile.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class profile
$profile = new Profile($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $profile->idUser = $data->id_user;
        $profile->idPermition = $data->id_permition;
        if (empty($data->obs))
            $profile->obs = 'Perfil criado automaticamente';
        else
            $profile->obs = $data->obs;
        // Retrieve the response about the register of profile
        $response = $profile->registerProfile();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Perfil registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Perfil não registado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('GET' === $method) {
        $response = $profile->readProfile(); // Read all profile
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Perfil encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nemhum perfil encontrado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('PUT' === $method) {
        // Update profile
        $profile->id = $data->id;
        $profile->idUser = $data->id_user;
        $profile->idPermition = $data->id_permition;
        //$profile->comment = $data->comment;
        if (empty($data->obs))
            $profile->obs = 'Perfil criado automaticamente';
        else
            $profile->obs = $data->obs;
        // Retrieve the response about the update of profile
        $response = $profile->updateProfile();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Perfil actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Perfil não actualizado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('DELETE' === $method) {
        // Delete profile
        //$profile->id = $data->id;

        foreach ($data->id as $id) {
            $profile->id = $id;
            // Retrieve the response about the delete of profile
            $response = $profile->deleteProfile();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Perfil eliminado com successo', $response);
                else
                    $responseReturned = $returned->returnResult(false, 'Perfil não eliminado', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>