<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Enterprise.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Enterprise
$enterprise = new Enterprise($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $enterprise->id = NULL;
        $enterprise->commercialDesignation = $data->commercial_designation;
        $enterprise->oficialDesignation = $data->oficial_designation;
        $enterprise->abbreviation = $data->abbreviation;
        $enterprise->taxIdentificationNumber = $data->tax_identification_number;
        $enterprise->address = $data->address;
        $enterprise->telephone = $data->telephone;
        $enterprise->email = $data->email;
        $enterprise->registerNumber = $data->register_number;
        $response = $enterprise->registerenterprise();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empresa registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Empresa não registada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    if ($token) {
        $response = $enterprise->readEnterprise(); // Read all enterprise
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empresa(s) encontrada(s)', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nemhuma empresa encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update enterprise
        $enterprise->id = $data->id;
        $enterprise->commercialDesignation = $data->commercial_designation;
        $enterprise->oficialDesignation = $data->oficial_designation;
        $enterprise->abbreviation = $data->abbreviation;
        $enterprise->taxIdentificationNumber = $data->tax_identification_number;
        $enterprise->address = $data->address;
        $enterprise->telephone = $data->telephone;
        $enterprise->email = $data->email;
        $enterprise->registerNumber = $data->register_number;
        // Retrieve the response about the update of Enterprise
        $response = $enterprise->updateEnterprise();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empresa actualizada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Empresa não actualizada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    //if($token){
    // Delete enterprise  
    foreach ($data->id as $id) {
        $enterprise->id = $id;
        // Retrieve the response about the delete of enterprise
        //$response = $enterprise->deleteEnterprise();
        $response = $enterprise->deleteEnterpriseWithDirectory();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empresa(s)eliminada(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Empresa não eliminada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    }
    //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>