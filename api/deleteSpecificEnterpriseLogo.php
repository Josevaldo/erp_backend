<?php

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Enterprise.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class enterprise
$enterprise = new Enterprise($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if('DELETE' === $method){
		//if($token){
			// Delete user
			//$user->id = $data->id;
			$i=0;
			foreach($data->id as $id){
				//$user->id = $id;
				$enterprise->id = $data->id[$i];
				// Delete spefic enterprise document
				$response = $enterprise->deleteSpecificEnterpriseDocument($data->file_name[$i]);
				$i++;
				// Retrieve the response about the delete of user
				//$response = $user->deleteuser();
				// Return the result
				if($response) $responseReturned = $returned->returnResult(true,'Logotipo eliminado com successo',array());
				else $responseReturned = $returned->returnResult(false,'Logotipo não eliminado',array());
			}
		//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
	}
}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>