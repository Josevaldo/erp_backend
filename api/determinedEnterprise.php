<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Enterprise.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class enterprise
$enterprise = new Enterprise($db);
// instance the class returned
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('GET' === $method) {
    // Get the authorization to access resource
    $token = $userToken->getAuthorization();
    if ($token) {
        $enterprise->id = $_GET['id'];
        // Read determined user
        $response = $enterprise->readDeterminedEnterprise();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empresa encontrada', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nemhuma empresa encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>