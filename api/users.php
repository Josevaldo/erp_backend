<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/User.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class user
$user = new User($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $user->id = NULL;
        $user->name = $data->name;
        $user->completeName = $data->complete_name;
        $user->email = $data->email;
        //$user->password = $data->password;
        $user->password = password_hash($data->password, PASSWORD_DEFAULT);
        $user->telephone = $data->telephone;
        $user->license = $data->license;
        $user->idStore = $data->id_store;
        // Check if the email already exists
        $emailExist = $user->checkEmailExists();
        if ($emailExist)
            $responseReturned = $returned->returnResult(false, 'Este email já existe no sistema', array());
        else {
            // Retrieve the response about the register of user
            $response = $user->registerUser();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response) {
                    // Assigned roles to user when creating this one, in order to create a new profile
//                    $newProfile = new Profile($db);
//                    $profileCreated = $newProfile->createUserProfile($response, $user->idStore);
                    $responseReturned = $returned->returnResult(true, 'Usuário registado com successo', $response);
                } else
                    $responseReturned = $returned->returnResult(false, 'Usuário não registado', array());
            } else {
                $responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
//                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } elseif ('GET' === $method) {
        $response = $user->readUser(); // Read all user
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Usuários encontrados', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nemhum usuário encontrado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('PUT' === $method) {
        // Update user
        $user->id = $data->id;
        $user->name = $data->name;
        $user->completeName = $data->complete_name;
        $user->email = $data->email;
        //$user->password = $data->password;
        $user->password = password_hash($data->password, PASSWORD_DEFAULT);
        $user->telephone = $data->telephone;
        $user->license = $data->license;
        $user->idStore = $data->id_store;
        // Retrieve the response about the update of adhrent
        $response = $user->updateUser();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Usuário actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Usuário não actualizado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
            //$responseReturned = $returned->returnResult(false,'Processamento falhou, verifique os dados enviados',array());
        }
    } elseif ('DELETE' === $method) {
        // Delete user
        //$user->id = $data->id;
        foreach ($data->id as $id) {
            $user->id = $id;
            // Retrieve the response about the delete of adhrent
            $response = $user->deleteUser();
            //$response = $user->deleteUserWithDirectory();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Usuário eliminado com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Usuário não eliminado', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>