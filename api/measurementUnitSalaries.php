<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/MeasurementUnitSalary.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class measurement_unit_salary
$measurementUnitSalary = new MeasurementUnitSalary($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $measurementUnitSalary->id = NULL;
        $measurementUnitSalary->designation = $data->designation;
        $measurementUnitSalary->symbol = $data->symbol;
        $response = $measurementUnitSalary->registerMeasurementUnitSalary();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Unidade de medida do salário registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Unidade de medida do salário não registada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $measurementUnitSalary->readMeasurementUnitSalary(); // Read all Unidade de medida do salário
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Unidade de medida do salário encontrada', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhuma Unidade de medida do salário encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
        // Update measurementUnitSalary
        $measurementUnitSalary->id = $data->id;
        $measurementUnitSalary->designation = $data->designation;
        $measurementUnitSalary->symbol = $data->symbol;
        $response = $measurementUnitSalary->updateUnitMeasurementSalary();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Unidade de medida do salário actualizada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Unidade de medida do salário não actualizada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $measurementUnitSalary->id = $id;
            // Retrieve the response about the delete UnitMeasurementSalary
            $response = $measurementUnitSalary->deleteUnitMeasurementSalary();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Unidade de medida do salário eliminada com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Unidade de medida do salário não eliminada', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>