<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/LicenseType.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class LicenseType
$licenseType = new LicenseType($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $licenseType->id = null;
        $licenseType->designation = $data->designation;
        $licenseType->numberDay = $data->number_day;
        $licenseType->price = $data->price;
        $licenseType->obs = $data->obs;
        $response = $licenseType->registerLicenseType();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de licença registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de licença não registado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    if ($token) {
        $response = $licenseType->readLicenseType(); // Read License Type
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo(s) de licenças(s) encontrado(s)', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhum Tipo de licenças encontrado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update License Type
        $licenseType->id = $data->id;
        $licenseType->designation = $data->designation;
        $licenseType->numberDay = $data->number_day;
        $licenseType->price = $data->price;
        $licenseType->obs = $data->obs;
        $response = $licenseType->updateLicenseType();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de licença actualizada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de licença não actualizada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    //if($token){
    // Delete Licende Type
    foreach ($data->id as $id) {
        $licenseType->id = $id;
        // Retrieve the response about the delete of License Type
        $response = $licenseType->deleteLicenseType();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de licença(s) eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de licença não eliminado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    }
    //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>