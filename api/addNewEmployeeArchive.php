<?php

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: PUT,POST"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Employee.php";
require_once "../classes/Returned.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class employee
$employee = new Employee($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/*$json = file_get_contents('php://input');
$data = json_decode($json);*/
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method){
	// Get the authorization to access resource
	$token = $userToken->getAuthorization();
	if($token){
		//$submitedFile = $_FILES['otherDocument']['name'];
		if($_FILES['employeeArchive']['name']){
			$employee->id = $_POST['id_employee'];
			// Check if this user is registered in the system
			$checkEmployee = $employee->readDeterminedEmployee();
			if($checkEmployee){
				// Alter image 
				//$rep = '../images/users/'.$user->id.'/';
				$employeeDocument = new DocumentStorage('employeeArchive',$employee->id,$db);
				$numberOtherDocument = count($_FILES['employeeArchive']['name']);
				for($i=0;$i<$numberOtherDocument;$i++){
					$employeeDocument->fileName = $_FILES['employeeArchive']['name'][$i];
					$employeeDocument->fileTemporaryName = $_FILES['employeeArchive']['tmp_name'][$i];
					$employeeDocument->objectIdentifier = $employee->id;
					$response = $employeeDocument->storeDocument();
					// Return the result
					if($response) $responseReturned = $returned->returnResult(true,'Nova documento submetido com successo',array());
					else $responseReturned = $returned->returnResult(false,'Documento não foi submetido',array());
				}
			}else $responseReturned = $returned->returnResult(false,'O funcionário não existe no sistema',array());
		}else $responseReturned = $returned->returnResult(false,'Nehum documento submetido',array());
	}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
}else{
	$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
}
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>