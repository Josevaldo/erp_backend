<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Career.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class career
$career = new Career($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $career->id = NULL;
        $career->description = $data->description;
        $career->initialDate = $data->initial_date;
        $career->endDate = $data->end_date;
        $career->obs = $data->obs;
        $career->idEmployee = $data->id_employee;
        $response = $career->registerCareer();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'carreira registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'carreira não registada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $career->readcareer(); // Read all career
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'carreira encontrada', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhuma carreira encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
        // Update career
        $career->id = $data->id;
        $career->description = $data->description;
        $career->initialDate = $data->initial_date;
        $career->endDate = $data->end_date;
        $career->obs = $data->obs;
        $career->idEmployee = $data->id_employee;
        $response = $career->updatecareer();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'carreira actualizada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'carreira não actualizada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $career->id = $id;
            // Retrieve the response about the delete of career
            $response = $career->deletecareer();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'carreira eliminada com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'carreira não eliminada', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>