<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Employee.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class employee
$employee = new Employee($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $employee->id = NULL;
        $employee->fullName = $data->full_name;
        $employee->birthPlace = $data->birth_place;
        $employee->birthDate = $data->birth_date;
        $employee->nationality = $data->nationality;
        $employee->gender = $data->gender;
        $employee->employeeCode = $data->employee_code;
        $employee->otherId = $data->other_id;
        $employee->fatherName = $data->father_name;
        $employee->motherName = $data->mother_name;
        $employee->idContract = $data->id_contract;
        $employee->idDepartment = $data->id_department;
        $response = $employee->registerEmployee();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Funcionário registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Funcionário não registado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $employee->readEmployee(); // Read all employee
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Funcionário encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhum funcionário encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
        // Update Contract
        $employee->id = $data->id;
        $employee->fullName = $data->full_name;
        $employee->birthPlace = $data->birth_place;
        $employee->birthDate = $data->birth_date;
        $employee->nationality = $data->nationality;
        $employee->gender = $data->gender;
        $employee->employeeCode = $data->employee_code;
        $employee->otherId = $data->other_id;
        $employee->fatherName = $data->father_name;
        $employee->motherName = $data->mother_name;
        $employee->idContract = $data->id_contract;
        $employee->idDepartment = $data->id_department;
        $response = $employee->updateEmployee();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Funcionário actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Funcionário não actualizado', array());
        } else {
            $responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $employee->id = $id;
            // Retrieve the response about the delete of employee
            //$response = $employee->deleteEmployee();
            $response = $employee->deleteEmployeeWithDirectory();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Funcionário eliminado com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Funcionário não eliminado', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>