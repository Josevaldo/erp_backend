<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Deduction.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class dedution
$dedution = new Deduction($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $dedution->id = NULL;
        $dedution->designation = $data->designation;
        $dedution->obs = $data->obs;
        $response = $dedution->registerDeduction();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Dedução registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Dedução não registada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
			//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $dedution->readDeduction(); // Read all dedution
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Dedução encontrada', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhuma dedução encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
		//if(($data->designation != 'IRT') || ($data->designation != 'Segurança Social')){
			// Update dedution
			$dedution->id = $data->id;
			$dedution->designation = $data->designation;
			$dedution->obs = $data->obs;
			$response = $dedution->updateDeduction();
			// Return the result
			if (empty($response['db_error'])) {
				if ($response)
					$responseReturned = $returned->returnResult(true, 'Dedução actualizada com successo', array());
				else
					$responseReturned = $returned->returnResult(false, 'Dedução não actualizada', array());
			} else {
				//$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
				$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
			}
		//}
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $dedution->id = $id;
            // Retrieve the response about the delete of dedution
            $response = $dedution->deleteDeduction();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Dedução eliminada com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Dedução não eliminadaS', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>