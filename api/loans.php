<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Loan.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class loan
$loan = new Loan($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $loan->id = NULL;
        $loan->designation = $data->designation;
        $loan->registerDate = date("y-m-d");
        $loan->dateFirstPayment = $data->date_first_payment;
        $loan->dateLastPayment = $data->date_last_payment;
        $loan->value = $data->value;
        $loan->idEmployee = $data->id_employee;
        $loan->idTypeLoan = $data->id_type_loan;
        $loan->state = $data->state;
        $response = $loan->registerLoan();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empréstimo registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Empréstimo não registado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $loan->readLoan(); // Read all loan
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empréstimo encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhum empréstimo encontrado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
        // Update loan
        $loan->id = $data->id;
        $loan->designation = $data->designation;
        //$loan->registerDate = date("y-m-d");
        $loan->dateFirstPayment = $data->date_first_payment;
        $loan->dateLastPayment = $data->date_last_payment;
        $loan->value = $data->value;
        $loan->idEmployee = $data->id_employee;
        $loan->idTypeLoan = $data->id_type_loan;
        $loan->state = $data->state;
        $response = $loan->updateLoan();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Empréstimo actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Empréstimo não actualizado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $loan->id = $id;
            // Retrieve the response about the delete of loan
            $response = $loan->deleteLoan();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Empréstimo eliminado com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Empréstimo não eliminado', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>