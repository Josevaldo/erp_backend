<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Employee.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class employee
$employee = new Employee($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
//$json = file_get_contents('php://input');
//$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
		if((empty($_FILES['employeePhoto']['name'])) || (empty($_FILES['employeeArchive']['name']))) $responseReturned = $returned->returnResult(false,'Fotografia ou dossiers do funcionário não submetido',array());
		else{
			$data = json_decode($_POST['employeeData']);
			$employee->id = NULL;
			$employee->fullName = $data->full_name;
			$employee->birthPlace = $data->birth_place;
			$employee->birthDate = $data->birth_date;
			$employee->nationality = $data->nationality;
			$employee->gender = $data->gender;
			$employee->employeeCode = $data->employee_code;
			$employee->otherId = $data->other_id;
			$employee->fatherName = $data->father_name;
			$employee->motherName = $data->mother_name;
			$employee->email = $data->email;
			$employee->address = $data->address;
			$employee->idContract = $data->id_contract;
			$employee->idDepartment = $data->id_department;
			// Check if the email already exists
			$emailExist = $employee->checkEmailExists();
			if($emailExist) $responseReturned = $returned->returnResult(false,'Este email já existe no sistema',array());
			else{
				$response = $employee->registerEmployee();
				// Return the result
				if (empty($response['db_error'])) {
					if ($response){
						$employee->id = $response;
						// instance the class DocumentStorage to employee photo
						$employeePhotoSubmited = new DocumentStorage('employeePhoto',$employee->id,$db);
						$employeePhotoSubmited->fileName = $_FILES['employeePhoto']['name'];
						$employeePhotoSubmited->fileTemporaryName = $_FILES['employeePhoto']['tmp_name'];
						// store document
						$employeePhotoStored = $employeePhotoSubmited->storeDocument();
						
						// instance the class DocumentStorage to employee archive
						$employeeArchiveSubmited = new DocumentStorage('employeeArchive',$employee->id,$db);
						$numberOtherArchive = count($_FILES['employeeArchive']['name']);
						for($i=0;$i<$numberOtherArchive;$i++){
							$employeeArchiveSubmited->fileName = $_FILES['employeeArchive']['name'][$i];
							$employeeArchiveSubmited->fileTemporaryName = $_FILES['employeeArchive']['tmp_name'][$i];
							// store document
							$employeeArchiveStored = $employeeArchiveSubmited->storeDocument();
						}
						$responseReturned = $returned->returnResult(true, 'Funcionário registado com successo', $response);
					}	
					else
						$responseReturned = $returned->returnResult(false, 'Funcionário não registado', array());
				} else {
					$responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
					//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
				}
			}
			
		}
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>