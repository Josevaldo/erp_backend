<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/AcademicEducation.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class academicEducation
$academicEducation = new AcademicEducation($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $academicEducation->idEmployee = $data->id_employee;
        $academicEducation->idEducation = $data->id_education;
        $academicEducation->idAcademicDegree = $data->id_academic_degree;
        $academicEducation->initialYear = $data->initial_year;
        $academicEducation->endYear = $data->end_year;
        $academicEducation->establishment = $data->establishment;
        $academicEducation->obs = $data->obs;
        // Retrieve the response about the register of academic Education
        $response = $academicEducation->registerAcademicEducation();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Educação acadêmica registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Educação Acadêmica não registada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('GET' === $method) {
        $response = $academicEducation->readAcademicEducation(); // Read all academic education
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Educação Acadêmica encontrada', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhuma educação acadêmica encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('PUT' === $method) {
        // Update academic education
        $academicEducation->id = $data->id;
        $academicEducation->idEmployee = $data->id_employee;
        $academicEducation->idEducation = $data->id_education;
        $academicEducation->idAcademicDegree = $data->id_academic_degree;
        $academicEducation->initialYear = $data->initial_year;
        $academicEducation->endYear = $data->end_year;
        $academicEducation->establishment = $data->establishment;
        $academicEducation->obs = $data->obs;
        // Retrieve the response about the update of profile
        $response = $academicEducation->updateacAdemicEducation();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Educação acadêmica actualizada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Educação acadêmica não actualizada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('DELETE' === $method) {
        // Delete academic education
        foreach ($data->id as $id) {
            $academicEducation->id = $id;
            // Retrieve the response about the delete of academic education
            $response = $academicEducation->deleteAcademicEducation();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Educação acadêmica eliminada com successo', $response);
                else
                    $responseReturned = $returned->returnResult(false, 'Educação acadêmica não eliminada', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>