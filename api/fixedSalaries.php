<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/FixedSalary.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class FixedSalary
$fixedSalary = new FixedSalary($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $fixedSalary->idOffice = $data->id_office;
        $fixedSalary->idSalary = $data->id_salary;
        $fixedSalary->value = $data->value;
        $fixedSalary->idMeasurementUnitSalary = $data->id_measurement_unit_salary;
        $fixedSalary->obs = $data->obs;
        $response = $fixedSalary->registerFixedSlary();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Salário fixado registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Salário fixado não registada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('GET' === $method) {
        $response = $fixedSalary->readFixedSalary(); // Read all fixed salary
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Salário fixado encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhum salário fixado encontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('PUT' === $method) {
        // Update fixed Salary
        $fixedSalary->id = $data->id;
        $fixedSalary->idOffice = $data->id_office;
        $fixedSalary->idSalary = $data->id_salary;
        $fixedSalary->value = $data->value;
        $fixedSalary->idMeasurementUnitSalary = $data->id_measurement_unit_salary;
        $fixedSalary->obs = $data->obs;
        $response = $fixedSalary->updateFixedSalary();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Salário fixado actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Salário fixado não actualizado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('DELETE' === $method) {
        // Delete profile
        //$profile->id = $data->id;

        foreach ($data->id as $id) {
            $fixedSalary->id = $id;
            $response = $fixedSalary->deleteFixedSalary();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Salário eliminado com successo', $response);
                else
                    $responseReturned = $returned->returnResult(false, 'Salário fixado não eliminada', array());
            } else {
//                $responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>