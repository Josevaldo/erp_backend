<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/PeriodType.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Period_type
$periodType = new PeriodType($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        $periodType->id = NULL;
        $periodType->designation = $data->designation;
        $periodType->obs = $data->obs;
        $response = $periodType->registerperiodtype();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de período registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de período não registado', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('GET' === $method) {
        //if($token){
        $response = $periodType->readPeriodType(); // Read all PeriodType
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de período encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nenhum tipo de períodoencontrada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('PUT' === $method) {
        //if($token){
        // Update periodType
        $periodType->id = $data->id;
        $periodType->designation = $data->designation;
        $periodType->obs = $data->obs;
        $response = $periodType->updatePeriodType();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de período actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de período não actualizado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } elseif ('DELETE' === $method) {
        //if($token){
        foreach ($data->id as $id) {
            $periodType->id = $id;
            // Retrieve the response about the delete of periodType
            $response = $periodType->deletePeriodType();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Tipo de período eliminado com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Tipo de período não eliminado', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>