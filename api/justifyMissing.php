<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,PUT");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Missing.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Missing
$missing = new Missing($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('PUT' === $method) {
        foreach ($data->id_employee as $idEmp) {
			$missing->idEmployee = $idEmp;
			$missing->missingDate[0] = $data->initial_missing_date;
			$missing->missingDate[1] = $data->end_missing_date;
			$missing->justified = 'S';
			$missing->obs = $data->obs;
			// Justify missing
			$response = $missing->justifyMissing();
			// Return the result
			if (empty($response['db_error'])) {
				if ($response)
					$responseReturned = $returned->returnResult(true, 'Falta actualizada com successo', array());
				else
					$responseReturned = $returned->returnResult(false, 'Falta não actualizada', array());
			} else {
				$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
				//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
			}
		}
    }else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>