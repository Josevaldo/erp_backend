<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Attendance.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class attendance
$attendance = new Attendance($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
//$json = file_get_contents('php://input');
//$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
		if((empty($_FILES['attendanceReport']['name']))) $responseReturned = $returned->returnResult(false,'Relatório de assiduidade não submetido',array());
		else{
			$attendance->id = time();
			// instance the class DocumentStorage
			$attendanceReportSubmited = new DocumentStorage('attendanceReport',$attendance->id,$db);
			$attendanceReportSubmited->fileName = $_FILES['attendanceReport']['name'];
			$attendanceReportSubmited->fileTemporaryName = $_FILES['attendanceReport']['tmp_name'];
			// store document
			$documentStored = $attendanceReportSubmited->storeDocument();
			if($documentStored){
				// Get repository path
				$documentStoredPath = $attendanceReportSubmited->getRepositoryPath().$attendanceReportSubmited->fileName;
				// Upload attendance file
				$response = $attendance->uploadExcelAttendanceFile($documentStoredPath);
			}else $response = false;
			// Return the result
			if (empty($response['db_error'])) {
				if ($response){
					$responseReturned = $returned->returnResult(true, 'Relatório de assiduidade submetido com successo', $response);
				}	
				else
					$responseReturned = $returned->returnResult(false, 'Relatório não submetido', array());
			} else {
				$responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
				//$responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
			}	
		}
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>