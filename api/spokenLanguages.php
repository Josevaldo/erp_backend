<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/SpokenLanguage.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class SpokenLanguage
$spokenLanguage = new SpokenLanguage($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        $spokenLanguage->idEmployee = $data->id_employee;
        $spokenLanguage->idLanguage = $data->id_language;
        $spokenLanguage->level = $data->level;
        $spokenLanguage->obs = $data->obs;

        $response = $spokenLanguage->registerSpokenLanguage();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Linguagem falada registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Linguagem falada não registada', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('GET' === $method) {
        $response = $spokenLanguage->readSpokenLanguage(); // Read all spoken language
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Perfil encontrado', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Nemhum perfil encontrado', array());
        } else {
            //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('PUT' === $method) {
        // Update spoken language
        $spokenLanguage->id = $data->id;
        $spokenLanguage->idEmployee = $data->id_employee;
        $spokenLanguage->idLanguage = $data->id_language;
        $spokenLanguage->level = $data->level;
        $spokenLanguage->obs = $data->obs;
        // Retrieve the response about the update of spoken Language
        $response = $spokenLanguage->updateSpokenLanguage();
        // Return the result
        if (empty($response['db_error'])) {
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Linguagem falada actualizada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Linguagem Falada actualizada', array());
        } else {
            $responseReturned = $returned->returnResult(false, $response['db_error'], $response['db_error']);
//            $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
        }
    } elseif ('DELETE' === $method) {
        // Delete spokenLanguage
        foreach ($data->id as $id) {
            $spokenLanguage->id = $id;
            // Retrieve the response about the delete of spoken language
            $response = $spokenLanguage->deleteSpokenLanguage();
            // Return the result
            if (empty($response['db_error'])) {
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Linguagem fala eliminada com successo', $response);
                else
                    $responseReturned = $returned->returnResult(false, 'Linguagem falada não eliminada', array());
            } else {
                //$responseReturned = $returned->returnResult(false,$response['db_error'],$response['db_error']);
                $responseReturned = $returned->returnResult(false, 'Processamento falhou, verifique os dados enviados', array());
            }
        }
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>