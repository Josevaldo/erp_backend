-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 19-Jun-2024 às 17:56
-- Versão do servidor: 8.0.31
-- versão do PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `rh`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `academic_degree`
--

DROP TABLE IF EXISTS `academic_degree`;
CREATE TABLE IF NOT EXISTS `academic_degree` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `obs` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `academic_degree`
--

INSERT INTO `academic_degree` (`id`, `designation`, `obs`) VALUES
(2, 'Hello world', 'Valeu'),
(3, 'Olá', 'Valeu'),
(4, 'Olá', 'Valeu');

-- --------------------------------------------------------

--
-- Estrutura da tabela `academic_education`
--

DROP TABLE IF EXISTS `academic_education`;
CREATE TABLE IF NOT EXISTS `academic_education` (
  `id_employee` int NOT NULL,
  `id_education` int NOT NULL,
  `id_academic_degree` int NOT NULL,
  `initial_year` int NOT NULL,
  `end_year` int NOT NULL,
  `establishment` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id_employee`,`id_education`,`id_academic_degree`),
  KEY `id_education` (`id_education`),
  KEY `id_academic_degree` (`id_academic_degree`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `academic_education`
--

INSERT INTO `academic_education` (`id_employee`, `id_education`, `id_academic_degree`, `initial_year`, `end_year`, `establishment`, `obs`) VALUES
(1, 2, 2, 1996, 34, 'Coneição, meu amor', 'Camacupa, Bié');

-- --------------------------------------------------------

--
-- Estrutura da tabela `assessment`
--

DROP TABLE IF EXISTS `assessment`;
CREATE TABLE IF NOT EXISTS `assessment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `full_mark` decimal(10,0) NOT NULL,
  `obs` text NOT NULL,
  `id_assessment_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_assessment_type` (`id_assessment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `assessment`
--

INSERT INTO `assessment` (`id`, `designation`, `full_mark`, `obs`, `id_assessment_type`) VALUES
(3, 'Conceição Luís Maurício', '2025', '1', 1),
(4, '2025-05-22', '1', '1', 1),
(5, 'Conceição Luís Maurício', '2025', '1', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `assessment_type`
--

DROP TABLE IF EXISTS `assessment_type`;
CREATE TABLE IF NOT EXISTS `assessment_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `assessment_type`
--

INSERT INTO `assessment_type` (`id`, `designation`, `obs`) VALUES
(1, 'Conceição Maurício', '2025-05-22'),
(2, 'Conceição Luís Maurício', '2025-05-22'),
(3, 'Conceição Luís Maurício', '2025-05-22'),
(4, 'Conceição Luís Maurício', '2025-05-22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `attendance`
--

DROP TABLE IF EXISTS `attendance`;
CREATE TABLE IF NOT EXISTS `attendance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `attendance_time_in` time NOT NULL,
  `attendance_time_out` time NOT NULL,
  `register_date` datetime NOT NULL,
  `attendance_date` date NOT NULL,
  `id_period` int NOT NULL,
  `id_employee` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`),
  KEY `id_period` (`id_period`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `attendance`
--

INSERT INTO `attendance` (`id`, `attendance_time_in`, `attendance_time_out`, `register_date`, `attendance_date`, `id_period`, `id_employee`) VALUES
(1, '00:00:01', '00:00:00', '0000-00-00 00:00:00', '0000-00-00', 1, 1),
(2, '00:00:01', '00:00:00', '0000-00-00 00:00:00', '0000-00-00', 1, 1),
(3, '00:00:01', '00:00:00', '0000-00-00 00:00:00', '0000-00-00', 1, 1),
(7, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(8, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(9, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(10, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(11, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(12, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(13, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(14, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 1),
(15, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 2),
(16, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 2),
(17, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 2),
(18, '00:00:00', '00:00:00', '2024-06-19 17:36:21', '0000-00-00', 8, 2),
(19, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 2),
(20, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 2),
(21, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 2),
(22, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 2),
(23, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(24, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(25, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(26, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(27, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(28, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(29, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(30, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 3),
(31, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(32, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(33, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(34, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(35, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(36, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(37, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(38, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 6),
(39, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(40, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(41, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(42, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(43, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(44, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(45, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(46, '00:00:00', '00:00:00', '2024-06-19 17:36:22', '0000-00-00', 8, 7),
(47, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 1),
(48, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 1),
(49, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 1),
(50, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 1),
(51, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 2),
(52, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 2),
(53, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 2),
(54, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 2),
(55, '00:00:00', '00:00:00', '2024-06-19 17:39:26', '0000-00-00', 8, 3),
(56, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 3),
(57, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 3),
(58, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 3),
(59, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 6),
(60, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 6),
(61, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 6),
(62, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 6),
(63, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 7),
(64, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 7),
(65, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 7),
(66, '00:00:00', '00:00:00', '2024-06-19 17:39:27', '0000-00-00', 8, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attendance2`
--

DROP TABLE IF EXISTS `attendance2`;
CREATE TABLE IF NOT EXISTS `attendance2` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration_date` datetime NOT NULL,
  `time_worked` int NOT NULL,
  `person_id` int NOT NULL,
  `person_name` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `attendance3`
--

DROP TABLE IF EXISTS `attendance3`;
CREATE TABLE IF NOT EXISTS `attendance3` (
  `id` int NOT NULL AUTO_INCREMENT,
  `attendance_movement` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `register_date` datetime NOT NULL,
  `attendance_date` date NOT NULL,
  `attendance_time` time NOT NULL,
  `id_period` int NOT NULL,
  `id_employee` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`),
  KEY `id_period` (`id_period`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `attendance3`
--

INSERT INTO `attendance3` (`id`, `attendance_movement`, `register_date`, `attendance_date`, `attendance_time`, `id_period`, `id_employee`) VALUES
(1, '1', '0000-00-00 00:00:00', '0000-00-00', '00:00:00', 1, 1),
(2, '1', '0000-00-00 00:00:00', '0000-00-00', '00:00:00', 1, 1),
(3, '1', '0000-00-00 00:00:00', '0000-00-00', '00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `auditing`
--

DROP TABLE IF EXISTS `auditing`;
CREATE TABLE IF NOT EXISTS `auditing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `system_element` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `action_executed` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `data_before` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `data_after` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `user_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `execution_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3324 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `auditing`
--

INSERT INTO `auditing` (`id`, `system_element`, `action_executed`, `data_before`, `data_after`, `user_name`, `execution_date`) VALUES
(2827, 'Grau acadêmico', 'inserir', '', 'Observação: Valeu, Designação: Olá', '', '2024-05-30 10:32:13'),
(2828, 'Grau acadêmico', 'alterar', 'Observação: Valeu, Designação: Olá', 'Observação: Valeu, Designação: Hello world', '', '2024-05-30 10:34:53'),
(2829, 'Grau acadêmico', 'inserir', '', 'Observação: Valeu, Designação: Olá', '', '2024-05-30 10:35:06'),
(2830, 'Grau acadêmico', 'eliminar', 'Observação: Valeu, Designação: Hello world', '', '', '2024-05-30 10:37:30'),
(2831, 'Tipo de contrato', 'inserir', '', 'Observação: Valeu, Designação: Olá', 'Moisés', '2024-05-30 12:38:58'),
(2832, 'Tipo de contrato', 'alterar', 'Observação: Valeu, Designação: Olá', 'Observação: Valeu, Designação: wor', 'Moisés', '2024-05-30 12:39:55'),
(2833, 'Tipo de contrato', 'inserir', '', 'Observação: Valeu, Designação: Olá', 'Moisés', '2024-05-30 12:40:05'),
(2834, 'Tipo de contrato', 'eliminar', 'Observação: Valeu, Designação: wor', '', 'Moisés', '2024-05-30 12:43:36'),
(2835, 'contrato', 'inserir', '', 'Contrato: Olá, Data inicial: 0000-00-00, Data final: 0000-00-00, Código do contracto: Olá, Observação do contracto: Valeu, Tipo de contracto: Olá, Observação do tipo de contracto: Valeu', 'Moisés', '2024-05-30 13:45:50'),
(2836, 'contrato', 'inserir', '', 'Contrato: Olá, Data inicial: 2024-07-12, Data final: 2024-08-05, Código do contracto: Olá, Observação do contracto: Valeu, Tipo de contracto: Olá, Observação do tipo de contracto: Valeu', 'Moisés', '2024-05-30 13:47:18'),
(2837, 'Tipo de contrato', 'alterar', 'Contrato: Olá, Data inicial: 2024-07-12, Data final: 2024-08-05, Código do contracto: Olá, Observação do contracto: Valeu, Tipo de contracto: Olá, Observação do tipo de contracto: Valeu', 'Contrato: Hello, Data inicial: 2024-07-16, Data final: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu, Tipo de contracto: Olá, Observação do tipo de contracto: Valeu', 'Moisés', '2024-05-30 13:48:55'),
(2838, 'contrato', 'eliminar', 'Contrato: Olá, Data inicial: 0000-00-00, Data final: 0000-00-00, Código do contracto: Olá, Observação do contracto: Valeu, Tipo de contracto: Olá, Observação do tipo de contracto: Valeu', '', 'Moisés', '2024-05-30 14:03:25'),
(2839, 'Funcionário', 'inserir', '', 'Nome completo: Moisés Félix Henda Laurindo, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', 'Moisés', '2024-05-31 10:33:28'),
(2840, 'Funcionário', 'inserir', '', 'Nome completo: Moisés Félix Henda Laurindo, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', 'Moisés', '2024-05-31 10:35:08'),
(2841, 'Funcionário', 'inserir', '', 'Nome completo: Moisés Félix Henda Laurindo, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', 'Moisés', '2024-05-31 10:35:18'),
(2842, 'Funcionário', 'inserir', '', 'Nome completo: Moisés Félix Henda Laurindo, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', 'Moisés', '2024-05-31 10:35:18'),
(2843, 'Funcionário', 'alterar', 'Nome completo: Moisés Félix Henda Laurindo, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', 'Nome completo: Domingos, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', 'Moisés', '2024-05-31 10:35:51'),
(2844, 'Funcionário', 'eliminar', 'Nome completo: Moisés Félix Henda Laurindo, Local de nascimento: Camacupa, Bié, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Outro id: 85f, Nome do pai: Moisés Laurindo, Nome da mãe: Isabel Henda, Contracto: Hello, Data de início do contracto: 2024-07-16, Data do fim do contracto: 2024-08-09, Código do contracto: Olá, Observação do contracto: Valeu', '', 'Moisés', '2024-05-31 10:43:21'),
(2845, 'Educação', 'inserir', '', 'Observação: Camacupa, Bié, Designação: Amor à Pátria', 'Moisés', '2024-05-31 11:13:52'),
(2846, 'Educação', 'inserir', '', 'Observação: Camacupa, Bié, Designação: Amor à Pátria', 'Moisés', '2024-05-31 11:14:57'),
(2847, 'Educação', 'inserir', '', 'Observação: Camacupa, Bié, Designação: Amor à Pátria', 'Moisés', '2024-05-31 11:14:58'),
(2848, 'Educação', 'inserir', '', 'Observação: Camacupa, Bié, Designação: Amor à Pátria', 'Moisés', '2024-05-31 11:14:59'),
(2849, 'Educação', 'alterar', 'Observação: Camacupa, Bié, Designação: Amor à Pátria', 'Observação: Andulo, Bié, Designação: Amor à Pátria', 'Moisés', '2024-05-31 11:16:02'),
(2850, 'Educação', 'eliminar', 'Observação: Camacupa, Bié, Designação: Amor à Pátria', '', 'Moisés', '2024-05-31 11:21:42'),
(2851, 'Educação acadêmica', 'inserir', '', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Moisés', '2024-05-31 18:42:25'),
(2852, 'Educação acadêmica', 'alterar', '', '', 'Moisés', '2024-05-31 19:11:32'),
(2853, 'Educação acadêmica', 'alterar', '', '', 'Moisés', '2024-05-31 19:13:28'),
(2854, 'Educação acadêmica', 'alterar', '', '', 'Moisés', '2024-05-31 19:14:20'),
(2855, 'Educação acadêmica', 'alterar', '', '', 'Moisés', '2024-05-31 19:23:00'),
(2856, 'Educação acadêmica', 'alterar', '', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Moisés', '2024-05-31 19:27:30'),
(2857, 'Educação acadêmica', 'alterar', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu chocolate, Observação: Valeu', 'Moisés', '2024-05-31 19:34:00'),
(2858, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:33:30'),
(2859, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:35:22'),
(2860, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:36:49'),
(2861, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:37:32'),
(2862, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:41:25'),
(2863, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:44:41'),
(2864, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:47:15'),
(2865, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:49:03'),
(2866, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:49:35'),
(2867, 'Educação acadêmica', 'inserir', '', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Moisés', '2024-05-31 20:50:56'),
(2868, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:52:31'),
(2869, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:54:35'),
(2870, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:55:08'),
(2871, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-05-31 20:55:49'),
(2872, 'Educação acadêmica', 'inserir', '', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Moisés', '2024-06-01 09:00:24'),
(2873, 'Educação acadêmica', 'inserir', '', 'Identificador do funcionário: 1, dentificador da educação: 1, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Moisés', '2024-06-01 09:01:23'),
(2874, 'Educação acadêmica', 'inserir', '', 'Identificador do funcionário: 1, dentificador da educação: 2, dentificador do grau acadêmico: 2, Ano inicial: 1996, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação: Valeu', 'Moisés', '2024-06-01 09:02:32'),
(2875, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-01 09:05:20'),
(2876, 'Tipo de período', 'inserir', '', 'Observação: 2, Designação: 2', 'Moisés', '2024-06-01 21:46:12'),
(2877, 'Tipo de período', 'alterar', 'Observação: 2, Designação: 2', 'Observação: Amaral, Designação: Arroz', 'Moisés', '2024-06-01 21:49:25'),
(2878, 'Tipo de período', 'inserir', '', 'Observação: 2, Designação: 2', 'Moisés', '2024-06-01 21:49:40'),
(2879, 'Tipo de período', 'inserir', '', 'Observação: 2, Designação: 2', 'Moisés', '2024-06-01 21:49:49'),
(2880, 'Tipo de período', 'eliminar', 'Observação: Amaral, Designação: Arroz', '', 'Moisés', '2024-06-01 21:53:45'),
(2881, 'Período', 'inserir', '', 'Período: 0, Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', 'Moisés', '2024-06-01 23:15:03'),
(2882, 'Período', 'inserir', '', 'Período: São, Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', 'Moisés', '2024-06-01 23:18:51'),
(2883, 'Período', 'inserir', '', 'Período: São, Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', 'Moisés', '2024-06-01 23:18:52'),
(2884, 'Período', 'inserir', '', 'Período: São, Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', 'Moisés', '2024-06-01 23:20:03'),
(2885, 'Tipo de período', 'alterar', 'Período: , Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', 'Período: São, Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', 'Moisés', '2024-06-01 23:20:18'),
(2886, 'Período', 'eliminar', 'Período: São, Início: 0, Fim: 0, Tipo de período: 2, Observação do tipo de período: 2', '', 'Moisés', '2024-06-01 23:23:32'),
(2887, 'Comparecimento', 'inserir', '', 'Movimento do Comparecimento: São, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:07:46'),
(2888, 'Comparecimento', 'inserir', '', 'Movimento do Comparecimento: São, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:13:07'),
(2889, 'Comparecimento', 'inserir', '', 'Movimento do Comparecimento: São, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:13:08'),
(2890, 'Comparecimento', 'inserir', '', 'Movimento do Comparecimento: São, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:13:14'),
(2891, 'Comparecimento', 'alterar', 'Movimento do Comparecimento: São, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Movimento do Comparecimento: Mingadas, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:14:38'),
(2892, 'Comparecimento', 'alterar', 'Movimento do Comparecimento: Mingadas, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Movimento do Comparecimento: , Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:15:26'),
(2893, 'Comparecimento', 'alterar', 'Movimento do Comparecimento: , Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Movimento do Comparecimento: , Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-02 08:15:28'),
(2894, 'Comparecimento', 'eliminar', 'Movimento do Comparecimento: São, Tempo do comparecimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-02 08:21:21'),
(2895, 'Tipo de folga', 'inserir', '', 'Observação: Hoje, Designação: São', 'Moisés', '2024-06-02 21:42:38'),
(2896, 'Tipo de folga', 'alterar', 'Observação: Hoje, Designação: São', 'Observação: Hoje, Designação: São Mora', 'Moisés', '2024-06-02 21:45:21'),
(2897, 'Tipo de folga', 'inserir', '', 'Observação: Hoje, Designação: São', 'Moisés', '2024-06-02 21:47:07'),
(2898, 'Tipo de folga', 'inserir', '', 'Observação: Hoje, Designação: São', 'Moisés', '2024-06-02 21:47:07'),
(2899, 'Tipo de folga', 'inserir', '', 'Observação: Hoje, Designação: São', 'Moisés', '2024-06-02 21:47:08'),
(2900, 'Tipo de folga', 'eliminar', 'Observação: Hoje, Designação: São', '', 'Moisés', '2024-06-02 21:51:42'),
(2901, 'Folga', 'inserir', '', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:33:07'),
(2902, 'Folga', 'inserir', '', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:37:52'),
(2903, 'Folga', 'inserir', '', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:37:53'),
(2904, 'Folga', 'inserir', '', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:37:56'),
(2905, 'Folga', 'inserir', '', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:37:57'),
(2906, 'Folga', 'alterar', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:56:45'),
(2907, 'Folga', 'alterar', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:57:49'),
(2908, 'Folga', 'alterar', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:58:42'),
(2909, 'Folga', 'alterar', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-02 23:59:14'),
(2910, 'Folga', 'alterar', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Início da folga: 2024-08-12, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-03 00:07:40'),
(2911, 'folga', 'eliminar', 'Início da folga: 2024-08-23, Fim da folga: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: São Mora, Observação do tipo de folga: Hoje', '', 'Moisés', '2024-06-03 00:27:45'),
(2912, 'Dedução', 'inserir', '', 'Observação: 2025-05-22, Designação: 2024-08-23', 'Moisés', '2024-06-03 06:51:48'),
(2913, 'Dedução', 'inserir', '', 'Observação: 2025-05-22, Designação: Morada', 'Moisés', '2024-06-03 06:52:43'),
(2914, 'Dedução', 'inserir', '', 'Observação: 2025-05-22, Designação: Morada', 'Moisés', '2024-06-03 06:52:52'),
(2915, 'Dedução', 'inserir', '', 'Observação: 2025-05-22, Designação: Morada', 'Moisés', '2024-06-03 06:52:53'),
(2916, 'Dedução', 'alterar', 'Observação: 2025-05-22, Designação: 2024-08-23', 'Observação: 2025-05-22, Designação: Amada', 'Moisés', '2024-06-03 06:54:13'),
(2917, 'Dedução', 'eliminar', 'Observação: 2025-05-22, Designação: Morada', '', 'Moisés', '2024-06-03 06:57:02'),
(2918, 'cargo', 'inserir', '', 'Descrição: Morada, Observação: 2025-05-22', 'Moisés', '2024-06-03 07:58:29'),
(2919, 'cargo', 'inserir', '', 'Descrição: Morada, Observação: 2025-05-22', 'Moisés', '2024-06-03 07:58:43'),
(2920, 'cargo', 'inserir', '', 'Descrição: Morada, Observação: 2025-05-22', 'Moisés', '2024-06-03 07:58:44'),
(2921, 'cargo', 'inserir', '', 'Descrição: Morada, Observação: 2025-05-22', 'Moisés', '2024-06-03 07:58:45'),
(2922, 'cargo', 'inserir', '', 'Descrição: Morada, Observação: 2025-05-22', 'Moisés', '2024-06-03 07:58:45'),
(2923, 'cargo', 'alterar', 'Descrição: Morada, Observação: 2025-05-22', 'Descrição: Conceição Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-03 08:03:30'),
(2924, 'cargo', 'alterar', 'Descrição: Conceição Maurício, Observação: 2025-05-22', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-03 08:10:12'),
(2925, 'cargo', 'eliminar', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22', '', 'Moisés', '2024-06-03 08:10:45'),
(2926, 'Processamento', 'inserir', '', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', 'Moisés', '2024-06-03 09:01:59'),
(2927, 'Processamento', 'alterar', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', 'Descrição: Conceição Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', 'Moisés', '2024-06-03 09:04:09'),
(2928, 'Processamento', 'inserir', '', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', 'Moisés', '2024-06-03 09:04:41'),
(2929, 'Processamento', 'inserir', '', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', 'Moisés', '2024-06-03 09:04:42'),
(2930, 'Processamento', 'inserir', '', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', 'Moisés', '2024-06-03 09:04:43'),
(2931, 'Processamento', 'eliminar', 'Descrição: Conceição Luís Maurício, Observação: 2025-05-22, Data de processamento: 2024-06-03', '', 'Moisés', '2024-06-03 09:09:23'),
(2932, 'Grau acadêmico', 'inserir', '', '', '', '2024-06-03 22:53:25'),
(2933, 'Grau acadêmico', 'inserir', '', '', '', '2024-06-03 22:53:46'),
(2934, 'Grau acadêmico', 'inserir', '', '', '', '2024-06-03 22:53:47'),
(2935, 'Grau Cadêmico', 'alterar', '', '', '', '2024-06-03 22:55:16'),
(2936, 'Grau acadêmico', 'eliminar', '', '', 'Moisés', '2024-06-03 22:56:51'),
(2937, 'Tipo de contracto', 'inserir', '', 'Designação: Olá, Observação: Valeu', 'Moisés', '2024-06-04 01:07:35'),
(2938, 'Tipo de contracto', 'inserir', '', 'Designação: Olá, Observação: Valeu', 'Moisés', '2024-06-04 01:08:03'),
(2939, 'Tipo de contrato', 'alterar', '', '', 'Moisés', '2024-06-04 01:10:02'),
(2940, 'Tipo de contrato', 'alterar', '', '', 'Moisés', '2024-06-04 01:10:34'),
(2941, 'Tipo de contrato', 'alterar', 'Designação: Olá, Observação: Valeu', 'Designação: Débora Bartolomeu, Observação: Valeu', 'Moisés', '2024-06-04 01:10:47'),
(2942, 'Tipo de contrato', 'eliminar', 'Designação: Olá, Observação: Valeu', '', 'Moisés', '2024-06-04 01:11:39'),
(2943, 'Contracto', 'inserir', '', '', 'Moisés', '2024-06-04 02:07:37'),
(2944, 'Contracto', 'inserir', '', '', 'Moisés', '2024-06-04 02:08:00'),
(2945, 'Contracto', 'inserir', '', '', 'Moisés', '2024-06-04 02:08:06'),
(2946, 'Contracto', 'alterar', '', '', 'Moisés', '2024-06-04 02:09:57'),
(2947, 'Contrato', 'eliminar', '', '', 'Moisés', '2024-06-04 02:11:00'),
(2948, 'Funcionário', 'inserir', '', 'Nome completo: Moisés Félix Henda Laurindo, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Contratop: Boa Minga, Início do contrato: 2024-07-16, Fim do contrato: 2024-08-09, Código do contrato: Olá', 'Moisés', '2024-06-04 08:22:32'),
(2949, 'Funcionário', 'inserir', '', 'Nome completo: Moisés Félix Henda Laurindo, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Contratop: Boa Minga, Início do contrato: 2024-07-16, Fim do contrato: 2024-08-09, Código do contrato: Olá', 'Moisés', '2024-06-04 08:23:07'),
(2950, 'Funcionário', 'eliminar', '', '', 'Moisés', '2024-06-04 08:39:36'),
(2951, 'Funcionário', 'eliminar', 'Nome completo: Moisés Félix Henda Laurindo, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Contratop: Boa Minga, Início do contrato: 2024-07-16, Fim do contrato: 2024-08-09, Código do contrato: Olá', '', 'Moisés', '2024-06-04 08:40:01'),
(2952, 'Funcionário', 'alterar', 'Nome completo: Domingos, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Contratop: Boa Minga, Início do contrato: 2024-07-16, Fim do contrato: 2024-08-09, Código do contrato: Olá', 'Nome completo: Pedal, Data de nascimento: 2024-08-05, Nacionalidade: Angolana, Sexo: Masculino, Código: we5, Contratop: Boa Minga, Início do contrato: 2024-07-16, Fim do contrato: 2024-08-09, Código do contrato: Olá', 'Moisés', '2024-06-04 09:54:36'),
(2953, 'Educação', 'inserir', '', 'Designação: Amor à Pátria, Observação: Camacupa, Bié', 'Moisés', '2024-06-05 00:19:20'),
(2954, 'Educação', 'alterar', 'Designação: Amor à Pátria, Observação: Andulo, Bié', 'Designação: Amor ao dinheiro, Observação: Andulo, Bié', 'Moisés', '2024-06-05 00:20:47'),
(2955, 'Educação', 'eliminar', 'Designação: Amor à Pátria, Observação: Camacupa, Bié', '', 'Moisés', '2024-06-05 00:25:52'),
(2956, 'Educação acadêmica', 'inserir', '', 'Ano inicial: 34, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação da educação acadêmica: Camacupa, Bié, Educação: Amor ao dinheiro, Observação da educação: Andulo, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Grau acadêmico: Hello world, Observação do grau acadêmico: Valeu', 'Moisés', '2024-06-05 05:39:04'),
(2957, 'Educação acadêmica', 'alterar', 'Ano inicial: 34, Ano final: 34, Estabelecimento: Coneição, meu amor, Observação da educação acadêmica: Camacupa, Bié, Educação: Amor ao dinheiro, Observação da educação: Andulo, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Grau acadêmico: Hello world, Observação do grau acadêmico: Valeu', 'Ano inicial: 34, Ano final: 34, Estabelecimento: Coneição, meu chocolate, Observação da educação acadêmica: Camacupa, Bié, Educação: Amor ao dinheiro, Observação da educação: Andulo, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Grau acadêmico: Hello world, Observação do grau acadêmico: Valeu', 'Moisés', '2024-06-05 05:54:18'),
(2958, 'Educação acadêmica', 'alterar', 'Ano inicial: 34, Ano final: 34, Estabelecimento: Coneição, meu chocolate, Observação da educação acadêmica: Camacupa, Bié, Educação: Amor ao dinheiro, Observação da educação: Andulo, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Grau acadêmico: Hello world, Observação do grau acadêmico: Valeu', 'Ano inicial: 34, Ano final: 34, Estabelecimento: Coneição, minha vida, Observação da educação acadêmica: Camacupa, Bié, Educação: Amor ao dinheiro, Observação da educação: Andulo, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Grau acadêmico: Hello world, Observação do grau acadêmico: Valeu', 'Moisés', '2024-06-05 06:08:15'),
(2959, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-05 06:36:51'),
(2960, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-05 06:39:59'),
(2961, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-05 07:50:07'),
(2962, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-05 07:54:24'),
(2963, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-05 07:55:44'),
(2964, 'Educação acadêmica', 'eliminar', '', '', 'Moisés', '2024-06-05 08:20:12'),
(2965, 'Tipo de avaliação', 'inserir', '', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-05 08:22:14'),
(2966, 'Tipo Avaliação', 'alterar', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', 'Designação: Conceição Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-05 08:23:44'),
(2967, 'Tipo de avaliação', 'inserir', '', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-05 08:30:05'),
(2968, 'Tipo de avaliação', 'inserir', '', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-05 08:30:06'),
(2969, 'Tipo de avaliação', 'inserir', '', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-05 08:30:06'),
(2970, 'Tipo de avaliação', 'inserir', '', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', 'Moisés', '2024-06-05 08:30:07'),
(2971, 'Tipo de avaliação', 'eliminar', 'Designação: Conceição Luís Maurício, Observação: 2025-05-22', '', 'Moisés', '2024-06-05 08:32:03'),
(2972, 'Avaliação', 'inserir', '', 'Avaliação: 2025-05-22, Nota máxima: 1, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Moisés', '2024-06-05 09:29:06'),
(2973, 'Avaliação', 'inserir', '', 'Avaliação: 2025-05-22, Nota máxima: 1, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Moisés', '2024-06-05 09:30:02'),
(2974, 'Avaliação', 'inserir', '', 'Avaliação: Conceição Luís Maurício, Nota máxima: 2025, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Moisés', '2024-06-05 09:31:04'),
(2975, 'Avaliação', 'inserir', '', 'Avaliação: Conceição Luís Maurício, Nota máxima: 2025, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Moisés', '2024-06-05 09:31:04'),
(2976, 'Avaliação', 'inserir', '', 'Avaliação: Conceição Luís Maurício, Nota máxima: 2025, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Moisés', '2024-06-05 09:31:05'),
(2977, 'Avaliação', 'alterar', 'Avaliação: 2025-05-22, Nota máxima: 1, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Avaliação: Conceição Luís Maurício, Nota máxima: 2025, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', 'Moisés', '2024-06-05 09:35:00'),
(2978, 'Avaliação', 'eliminar', 'Avaliação: Conceição Luís Maurício, Nota máxima: 2025, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', '', 'Moisés', '2024-06-05 09:38:11'),
(2979, 'Avaliação', 'eliminar', 'Avaliação: Conceição Luís Maurício, Nota máxima: 2025, Observação: 1, Tipo de avaliação: Conceição Maurício, Observação do tipo de avaliação: 2025-05-22', '', 'Moisés', '2024-06-05 09:38:11'),
(2980, 'Desempenho', 'inserir', '', '', 'Moisés', '2024-06-05 21:19:48'),
(2981, 'Desempenho', 'inserir', '', 'Nota Obtida: 23, Observação do desempenho: Olá Pessoal, Avaliação: Conceição Luís Maurício, Máxima nota: 2025, Observação da avaliação: 1, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, C´podigo do funcionário: we5', 'Moisés', '2024-06-05 21:21:33'),
(2982, 'Desempenho', 'inserir', '', 'Nota Obtida: 23, Observação do desempenho: Olá Pessoal, Avaliação: Conceição Luís Maurício, Máxima nota: 2025, Observação da avaliação: 1, Funcionário: Pedal, Nacionalidade: Angolana, Sexo: Masculino, C´podigo do funcionário: we5', 'Moisés', '2024-06-05 21:21:54'),
(2983, 'Desempenho', 'inserir', '', 'Nota Obtida: 23, Observação do desempenho: Olá Pessoal, Avaliação: Conceição Luís Maurício, Máxima nota: 2025, Observação da avaliação: 1, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, C´podigo do funcionário: we5', 'Moisés', '2024-06-05 21:22:04'),
(2984, 'Desempenho', 'alterar', 'Nota Obtida: 23, Observação do desempenho: Olá Pessoal, Avaliação: Conceição Luís Maurício, Máxima nota: 2025, Observação da avaliação: 1, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, C´podigo do funcionário: we5', 'Nota Obtida: 34, Observação do desempenho: Balada, Avaliação: Conceição Luís Maurício, Máxima nota: 2025, Observação da avaliação: 1, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, C´podigo do funcionário: we5', 'Moisés', '2024-06-05 21:31:21'),
(2985, 'Desempenho', 'eliminar', '', '', 'Moisés', '2024-06-05 21:38:16'),
(2986, 'Desempenho', 'inserir', '', 'Nota Obtida: 23, Observação do desempenho: Olá Pessoal, Avaliação: Conceição Luís Maurício, Máxima nota: 2025, Observação da avaliação: 1, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, C´podigo do funcionário: we5', 'Moisés', '2024-06-05 21:38:44'),
(2987, 'Desempenho', 'eliminar', '', '', 'Moisés', '2024-06-05 21:40:58'),
(2988, 'Desempenho', 'eliminar', '', '', 'Moisés', '2024-06-05 21:46:04'),
(2989, 'Desempenho', 'eliminar', '', '', 'Moisés', '2024-06-05 21:48:11'),
(2990, 'Tipo de período', 'inserir', '', 'Designação: 2, Observação: 2', 'Moisés', '2024-06-05 22:35:33'),
(2991, 'Tipo de período', 'inserir', '', 'Designação: Colonia, Observação: 2', 'Moisés', '2024-06-05 22:36:03'),
(2992, 'Tipo de período', 'alterar', '', '', 'Moisés', '2024-06-05 22:36:44'),
(2993, 'Tipo de período', 'alterar', 'Designação: Colonia, Observação: 2', 'Designação: Arroz, Observação: Amaral', 'Moisés', '2024-06-05 22:37:27'),
(2994, 'Tipo de período', 'eliminar', '', '', 'Moisés', '2024-06-05 22:38:48'),
(2995, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:08'),
(2996, 'Tipo de período', 'eliminar', 'Designação: Arroz, Observação: Amaral', '', 'Moisés', '2024-06-05 22:39:27'),
(2997, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:41'),
(2998, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:44'),
(2999, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:45'),
(3000, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:46'),
(3001, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:46'),
(3002, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:47'),
(3003, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:47'),
(3004, 'Tipo de período', 'eliminar', 'Designação: 2, Observação: 2', '', 'Moisés', '2024-06-05 22:39:48'),
(3005, 'Período', 'inserir', '', 'Período: São, Início: 0, Fim: 0, Observação: 2, Observação do tipo de período 2', 'Moisés', '2024-06-05 23:21:25'),
(3006, 'Período', 'alterar', 'Período: São, Início: 0, Fim: 0, Observação: 2, Observação do tipo de período 2', 'Período: Meu amor, Início: 0, Fim: 0, Observação: 2, Observação do tipo de período 2', 'Moisés', '2024-06-05 23:22:03'),
(3007, 'Período', 'eliminar', 'Período: Meu amor, Início: 0, Fim: 0, Observação: 2, Observação do tipo de período 2', '', 'Moisés', '2024-06-05 23:24:31'),
(3008, 'Comparecimento', 'inserir', '', 'Movimento do comparecimento: São Miguel, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 01:30:57'),
(3009, 'Comparecimento', 'alterar', 'Movimento do comparecimento: São Miguel, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Movimento do comparecimento: Mingadas, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 01:32:04'),
(3010, 'Comparecimento', 'eliminar', '', '', 'Moisés', '2024-06-06 01:42:09'),
(3011, 'Comparecimento', 'eliminar', 'Movimento do comparecimento: Mingadas, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-06 01:42:38'),
(3012, 'Tipo de formação', 'inserir', '', 'Designação: 3, Observação: 1', 'Moisés', '2024-06-06 07:50:54'),
(3013, 'Tipo de formação', 'inserir', '', 'Designação: Arroz a feijão, Observação: É bom', 'Moisés', '2024-06-06 07:51:36'),
(3014, 'Tipo de formação', 'inserir', '', 'Designação: Arroz a feijão, Observação: É bom', 'Moisés', '2024-06-06 07:51:38'),
(3015, 'Tipo de formação', 'inserir', '', 'Designação: Arroz a feijão, Observação: É bom', 'Moisés', '2024-06-06 07:51:39'),
(3016, 'Tipo de formação', 'alterar', 'Designação: 3, Observação: 1', 'Designação: Papa, Observação: É bom', 'Moisés', '2024-06-06 07:53:08'),
(3017, 'Tipo de formação', 'alterar', 'Designação: Papa, Observação: É bom', 'Designação: Papa, Observação: É bom', 'Moisés', '2024-06-06 07:53:51'),
(3018, 'Tipo de formação', 'eliminar', 'Designação: Arroz a feijão, Observação: É bom', '', 'Moisés', '2024-06-06 07:57:08'),
(3019, 'Formação', 'inserir', '', 'Designação: Mica, Observação: 0000-00-00, Designação: 0000-00-00, Observação: É bom, Observação: Arroz a feijão, Observação: Papa, Observação: É bom', 'Moisés', '2024-06-06 08:12:18'),
(3020, 'Formação', 'inserir', '', 'Designação: Mica, Observação: 2024-07-19, Designação: 2025-07-02, Observação: É bom, Observação: Arroz a feijão, Observação: Papa, Observação: É bom', 'Moisés', '2024-06-06 08:13:03'),
(3021, 'Formação', 'inserir', '', 'Designação: São mora, Observação: 2024-07-19, Designação: 2025-07-02, Observação: É bom, Observação: Arroz a feijão, Observação: Papa, Observação: É bom', 'Moisés', '2024-06-06 08:13:24'),
(3022, 'Formação', 'alterar', 'Designação: Mica, Observação: 0000-00-00, Designação: 0000-00-00, Observação: É bom, Observação: Arroz a feijão, Observação: Papa, Observação: É bom', 'Designação: Micassa, Observação: 0000-00-00, Designação: 2022-09-09, Observação: Luanda, Observação: 2026-06-04, Observação: Papa, Observação: É bom', 'Moisés', '2024-06-06 08:17:59'),
(3023, 'Formação', 'eliminar', 'Designação: Micassa, Observação: 0000-00-00, Designação: 2022-09-09, Observação: Luanda, Observação: 2026-06-04, Observação: Papa, Observação: É bom', '', 'Moisés', '2024-06-06 08:31:39'),
(3024, 'Capacitação', 'inserir', '', 'Observação da capacitação: Moisés Laurindo, Formação: Mica, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 10:34:36'),
(3025, 'Capacitação', 'inserir', '', 'Observação da capacitação: Moisés Laurindo, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 10:35:05'),
(3026, 'Capacitação', 'alterar', '', 'Observação da capacitação: Moisés Laurindo, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 10:39:41'),
(3027, 'Capacitação', 'alterar', 'Observação da capacitação: Moisés Laurindo, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Observação da capacitação: Fuba, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 10:40:58'),
(3028, 'Capacitação', 'eliminar', '', '', 'Moisés', '2024-06-06 10:46:31'),
(3029, 'Tipo de folga', 'inserir', '', 'Designação: Azar, Observação: Hoje', 'Moisés', '2024-06-06 20:16:18'),
(3030, 'Tipo de folga', 'alterar', 'Designação: São Mora, Observação: Hoje', 'Designação: Mambos, Observação: Hoje', 'Moisés', '2024-06-06 20:17:14'),
(3031, 'Tipo de folga', 'eliminar', 'Designação: Azar, Observação: Hoje', '', 'Moisés', '2024-06-06 20:18:37'),
(3032, 'Folga', 'inserir', '', 'Início: 2024-08-23, Fim: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: Mambos, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-06 20:21:39'),
(3033, 'Folga', 'alterar', 'Início: 2024-08-23, Fim: 2025-05-22, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: Mambos, Observação do tipo de folga: Hoje', 'Início: 2024-08-25, Fim: 2025-05-23, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: Mambos, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-06 20:23:13'),
(3034, 'Folga', 'alterar', 'Início: 2024-08-25, Fim: 2025-05-23, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: Mambos, Observação do tipo de folga: Hoje', 'Início: 2024-08-25, Fim: 2025-05-13, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: Mambos, Observação do tipo de folga: Hoje', 'Moisés', '2024-06-06 20:30:54'),
(3035, 'Folga', 'eliminar', 'Início: 2024-08-25, Fim: 2025-05-13, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5, Tipo de folga: Mambos, Observação do tipo de folga: Hoje', '', 'Moisés', '2024-06-06 20:32:02'),
(3036, 'Comparecimento', 'inserir', '', 'Movimento do comparecimento: Mira dos Santos, Data de registo: 2024-06-06 00:00:00, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 21:03:04'),
(3037, 'Comparecimento', 'alterar', 'Movimento do comparecimento: Mira dos Santos, Data de registo: 2024-06-06 00:00:00, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Movimento do comparecimento: Mingadas, Data de registo: 2024-06-06 00:00:00, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-06 21:04:09'),
(3038, 'Comparecimento', 'eliminar', 'Movimento do comparecimento: Mingadas, Data de registo: 2024-06-06 00:00:00, Tempo do comprometimento: 0, Período: São, Início: 0, Fim: 0, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-06 21:05:12'),
(3039, 'Dedução', 'inserir', '', 'Designação: Morada, Observação: 2025-05-22', 'Moisés', '2024-06-07 00:43:40'),
(3040, 'Dedução', 'inserir', '', 'Designação: Arroz com feijão, Observação: 2025-05-22', 'Moisés', '2024-06-07 00:44:37'),
(3041, 'Dedução', 'alterar', 'Designação: Amada, Observação: 2025-05-22', 'Designação: Amada, Observação: 2025-05-22', 'Moisés', '2024-06-07 00:49:47'),
(3042, 'Dedução', 'alterar', 'Designação: Amada, Observação: 2025-05-22', 'Designação: mamã, Observação: 2025-05-22', 'Moisés', '2024-06-07 00:50:19'),
(3043, 'Dedução', 'eliminar', 'Designação: Arroz com feijão, Observação: 2025-05-22', '', 'Moisés', '2024-06-07 00:51:53'),
(3044, 'cargo', 'inserir', '', 'Descrição: Morada, Acrónym: 2025-05-22', 'Moisés', '2024-06-07 01:09:15'),
(3045, 'cargo', 'alterar', 'Descrição: Morada, Acrónym: 2025-05-22', 'Descrição: Conceição Luís Maurício, Acrónym: 2025-05-22', 'Moisés', '2024-06-07 01:09:48'),
(3046, 'cargo', 'eliminar', 'Descrição: Conceição Luís Maurício, Acrónym: 2025-05-22', '', 'Moisés', '2024-06-07 01:12:36'),
(3047, 'Processamento', 'inserir', '', '5, Morada, , 2025-05-22', 'Moisés', '2024-06-07 01:46:00'),
(3048, 'Processamento', 'inserir', '', '', 'Moisés', '2024-06-07 01:47:48'),
(3049, 'Processamento', 'alterar', '1, mamã, , 2025-05-22', '1, mamã, , 2025-05-22', 'Moisés', '2024-06-07 01:48:07'),
(3050, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:48:31'),
(3051, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:48:40'),
(3052, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:48:51'),
(3053, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:49:03'),
(3054, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:49:03'),
(3055, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:49:04'),
(3056, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:49:05'),
(3057, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:49:05'),
(3058, 'Processamento', 'alterar', '', '', 'Moisés', '2024-06-07 01:49:06'),
(3059, 'Processamento', 'eliminar', '', '', 'Moisés', '2024-06-07 01:49:44'),
(3060, 'Processamento da dedução', 'alterar', '796, 0.00, Conceição Maurício, 2024-06-03, Morada, mamã, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', '796000, 0.00, Conceição Maurício, 2024-06-03, Morada, mamã, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', 'Moisés', '2024-06-07 06:05:46'),
(3061, 'Processamento da dedução', 'inserir', '', '796, 0.00, Conceição Maurício, 2024-06-03, Morada, Morada, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', 'Moisés', '2024-06-07 06:06:29'),
(3062, 'Processamento da dedução', 'eliminar', '796000, 0.00, Conceição Maurício, 2024-06-03, Morada, mamã, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', '', 'Moisés', '2024-06-07 06:18:30'),
(3063, 'Dependente', 'inserir', '', 'Nome completo do depende: Moisés Domingos, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-07 09:43:26'),
(3064, 'Dependente', 'inserir', '', 'Nome completo do depende: Moisés Domingos, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-07 09:46:10'),
(3065, 'Dependente', 'inserir', '', 'Nome completo do depende: Moisés Domingos, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-07 09:46:11'),
(3066, 'Dependente', 'inserir', '', 'Nome completo do depende: Moisés Domingos, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-07 09:46:12'),
(3067, 'Dependente', 'inserir', '', 'Nome completo do depende: Débora Valentina, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-07 09:47:51'),
(3068, 'Dependente', 'alterar', 'Nome completo do depende: Débora Valentina, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Nome completo do depende: São mora, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-07 09:49:30'),
(3069, 'Dependente', 'eliminar', 'Nome completo do depende: São mora, Local de nascimento do dependente: Camacupa, Bié, Data de nascimento do dependente: 2022-05-09, Sexo do dependente: Masculino, Grau de parentesco: Sanguíneo, Funcionário: Moisés Félix Henda Laurindo, Local do nascimento do funcionário: Camacupa, Bié, Data de nascimento do funcionário: 2024-08-05, Nacionalidade do funcionário: Angolana, Sexo do funcionário: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-07 09:52:32');
INSERT INTO `auditing` (`id`, `system_element`, `action_executed`, `data_before`, `data_after`, `user_name`, `execution_date`) VALUES
(3070, 'Salário', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:11:48'),
(3071, 'Salário', 'alterar', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Designação: Débora Bartolomeu, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:13:04'),
(3072, 'Salário', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:13:14'),
(3073, 'Salário', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:13:14'),
(3074, 'Salário', 'alterar', '', '', 'Moisés', '2024-06-08 00:13:44'),
(3075, 'Salário', 'eliminar', 'Designação: Débora Valentina, Observação: Camacupa, Bié', '', 'Moisés', '2024-06-08 00:19:26'),
(3076, 'Linguagem', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:21:44'),
(3077, 'Linguagem', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:21:51'),
(3078, 'Linguagem', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:21:51'),
(3079, 'Linguagem', 'inserir', '', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:21:52'),
(3080, 'Linguagem', 'alterar', '', '', 'Moisés', '2024-06-08 00:22:36'),
(3081, 'Linguagem', 'alterar', 'Designação: Débora Valentina, Observação: Camacupa, Bié', 'Designação: Português, Observação: Camacupa, Bié', 'Moisés', '2024-06-08 00:22:51'),
(3082, 'Linguagem', 'eliminar', 'Designação: Débora Valentina, Observação: Camacupa, Bié', '', 'Moisés', '2024-06-08 00:25:56'),
(3083, 'Unidade de medida da dedução', 'inserir', '', 'Identificador1, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 00:28:29'),
(3084, 'Unidade de medida da dedução', 'inserir', '', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 00:29:02'),
(3085, 'Unidade de medida da dedução', 'inserir', '', 'Identificador3, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 00:29:03'),
(3086, 'Unidade de medida da dedução', 'inserir', '', 'Identificador4, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 00:29:03'),
(3087, 'Unidade de medida da dedução', 'alterar', '', '', 'Moisés', '2024-06-08 00:29:55'),
(3088, 'Unidade de medida da dedução', 'alterar', '', '', 'Moisés', '2024-06-08 00:30:15'),
(3089, 'Unidade de medida da dedução', 'alterar', 'Identificador4, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Identificador4, SímboloCamacupa, Bié, DesignaçãoDébora malabalara', 'Moisés', '2024-06-08 00:31:11'),
(3090, 'Unidade de medida da dedução', 'eliminar', 'Identificador4, SímboloCamacupa, Bié, DesignaçãoDébora malabalara', '', 'Moisés', '2024-06-08 00:36:09'),
(3091, 'Unidade de medida da dedução', 'inserir', '', 'Identificador5, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:35:50'),
(3092, 'Unidade de medida do salário', 'inserir', '', 'Identificador1, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:37:34'),
(3093, 'Unidade de medida do salário', 'inserir', '', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:38:29'),
(3094, 'Unidade de medida do salário', 'inserir', '', 'Identificador3, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:38:30'),
(3095, 'Unidade de medida do salário', 'inserir', '', 'Identificador4, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:38:30'),
(3096, 'Unidade de medida do salário', 'inserir', '', 'Identificador5, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:38:30'),
(3097, 'Unidade de medida da dedução', 'alterar', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Moisés', '2024-06-08 01:39:03'),
(3098, 'Unidade de medida da dedução', 'alterar', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Moisés', '2024-06-08 01:40:00'),
(3099, 'Unidade de medida da dedução', 'alterar', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Moisés', '2024-06-08 01:41:29'),
(3100, 'Unidade de medida da dedução', 'alterar', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Moisés', '2024-06-08 01:41:30'),
(3101, 'Unidade de medida da dedução', 'alterar', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Moisés', '2024-06-08 01:41:59'),
(3102, 'Unidade de medida do salário', 'inserir', '', 'Identificador6, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Moisés', '2024-06-08 01:42:34'),
(3103, 'Unidade de medida do salário', 'alterar', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', 'Identificador2, SímboloCamacupa, Bié, DesignaçãoDebate', 'Moisés', '2024-06-08 01:42:47'),
(3104, 'Unidade de medida do salário', 'eliminar', 'Identificador6, SímboloCamacupa, Bié, DesignaçãoDébora Valentina', '', 'Moisés', '2024-06-08 01:53:26'),
(3105, 'perfil', 'inserir', '', 'Valor da dedução fixada: 65.00, Observação da dedução fixada: Bié,Andulo, Cargo: Morada, Observação do cargo: 2025-05-22, Dedução: mamã, Observação da dedução: 2025-05-22, Medida da dedução: Débora Valentina, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 04:45:23'),
(3106, 'perfil', 'inserir', '', 'Valor da dedução fixada: 65.00, Observação da dedução fixada: Bié,Andulo, Cargo: Morada, Observação do cargo: 2025-05-22, Dedução: mamã, Observação da dedução: 2025-05-22, Medida da dedução: Débora Valentina, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 04:45:40'),
(3107, 'perfil', 'inserir', '', 'Valor da dedução fixada: 65.00, Observação da dedução fixada: Bié,Andulo, Cargo: Morada, Observação do cargo: 2025-05-22, Dedução: Morada, Observação da dedução: 2025-05-22, Medida da dedução: Débora Valentina, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 04:45:54'),
(3108, 'Dedução fixada', 'alterar', 'Valor da dedução fixada: 65.00, Observação da dedução fixada: Bié,Andulo, Cargo: Morada, Observação do cargo: 2025-05-22, Dedução: mamã, Observação da dedução: 2025-05-22, Medida da dedução: Débora Valentina, Símbolo da medida: Camacupa, Bié', 'Valor da dedução fixada: 77.00, Observação da dedução fixada: Bié,Andulo, Cargo: Morada, Observação do cargo: 2025-05-22, Dedução: mamã, Observação da dedução: 2025-05-22, Medida da dedução: Débora Valentina, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 04:49:43'),
(3109, 'Dedução fixada', 'eliminar', 'Valor da dedução fixada: 77.00, Observação da dedução fixada: Bié,Andulo, Cargo: Morada, Observação do cargo: 2025-05-22, Dedução: mamã, Observação da dedução: 2025-05-22, Medida da dedução: Débora Valentina, Símbolo da medida: Camacupa, Bié', '', 'Moisés', '2024-06-09 04:57:21'),
(3110, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 11:01:57'),
(3111, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 11:02:32'),
(3112, 'Unidade de medida do salário', 'alterar', '', '', 'Moisés', '2024-06-09 11:08:44'),
(3113, 'Unidade de medida do salário', 'alterar', '', '', 'Moisés', '2024-06-09 11:11:49'),
(3114, 'Salário fixado', 'alterar', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 11:11:49'),
(3115, 'Unidade de medida do salário', 'alterar', '', '', 'Moisés', '2024-06-09 11:12:25'),
(3116, 'Salário fixado', 'alterar', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-09 11:12:25'),
(3117, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 08:27:38'),
(3118, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 08:29:07'),
(3119, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 08:52:25'),
(3120, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 08:56:42'),
(3121, 'Unidade de medida do salário', 'inserir', '', 'Identificador7, Símbolo, Designação', 'Moisés', '2024-06-10 08:58:40'),
(3122, 'Unidade de medida do salário', 'inserir', '', 'Identificador8, Símbolo, Designação', 'Moisés', '2024-06-10 08:59:56'),
(3123, 'Unidade de medida do salário', 'inserir', '', 'Identificador9, Símbolo, Designação', 'Moisés', '2024-06-10 09:00:22'),
(3124, 'Unidade de medida do salário', 'inserir', '', 'Identificador10, Símbolo, Designação', 'Moisés', '2024-06-10 09:14:15'),
(3125, 'Unidade de medida do salário', 'inserir', '', 'Identificador11, Símbolo, Designação', 'Moisés', '2024-06-10 09:15:30'),
(3126, 'Unidade de medida do salário', 'inserir', '', 'Identificador12, Símbolo, Designação', 'Moisés', '2024-06-10 09:34:35'),
(3127, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Medida do salário: , Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 09:42:16'),
(3128, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 09:44:26'),
(3129, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 20:36:33'),
(3130, 'Salário fixado', 'inserir', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 20:37:01'),
(3131, 'Salário fixado', 'alterar', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 20:37:47'),
(3132, 'Salário fixado', 'alterar', '', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 20:38:06'),
(3133, 'Salário fixado', 'alterar', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Valor do salário fixado: 66.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', 'Moisés', '2024-06-10 20:40:36'),
(3134, 'Salário fixado', 'eliminar', 'Valor do salário fixado: 66.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Bartolomeu, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', '', 'Moisés', '2024-06-10 20:45:39'),
(3135, 'Salário fixado', 'eliminar', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', '', 'Moisés', '2024-06-10 20:45:54'),
(3136, 'Salário fixado', 'eliminar', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', '', 'Moisés', '2024-06-10 20:46:46'),
(3137, 'Salário fixado', 'eliminar', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', '', 'Moisés', '2024-06-10 20:47:22'),
(3138, 'Salário fixado', 'eliminar', 'Valor do salário fixado: 65.00, Observação do salário fixado: Bié,Andulo, Salário: Débora Valentina, Observação do salário: Camacupa, Bié, Cargo: Morada, Observação do cargo: 2025-05-22, Símbolo da medida: Camacupa, Bié', '', 'Moisés', '2024-06-10 20:50:07'),
(3139, 'Outra formação', 'inserir', '', 'Outra formação: 0, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', 'Moisés', '2024-06-10 20:55:36'),
(3140, 'Outra formação', 'inserir', '', 'Outra formação: Agricultura, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', 'Moisés', '2024-06-10 20:56:21'),
(3141, 'Outra formação', 'inserir', '', 'Outra formação: Agricultura, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', 'Moisés', '2024-06-10 20:56:22'),
(3142, 'Outra formação', 'inserir', '', 'Outra formação: Agricultura, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', 'Moisés', '2024-06-10 20:56:23'),
(3143, 'Outra formação', 'alterar', 'Outra formação: 0, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', 'Outra formação: Pesca, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', 'Moisés', '2024-06-10 20:57:23'),
(3144, 'Outra formação', 'eliminar', 'Outra formação: Agricultura, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', '', 'Moisés', '2024-06-10 21:05:41'),
(3145, 'Outra formação', 'eliminar', 'Outra formação: Agricultura, Início: 65, Fim: 1, Observação da outra formação: Bié,Andulo, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário we5', '', 'Moisés', '2024-06-10 21:08:49'),
(3146, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:42:35'),
(3147, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:43:01'),
(3148, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:44:25'),
(3149, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:44:41'),
(3150, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:44:42'),
(3151, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:44:45'),
(3152, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:44:51'),
(3153, 'Processamento da dedução', 'inserir', '', '796, 0.00, Conceição Maurício, 2024-06-03, Morada, Morada, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', 'Moisés', '2024-06-10 21:45:51'),
(3154, 'Processamento da dedução', 'alterar', '', '', 'Moisés', '2024-06-10 21:46:13'),
(3155, 'Processamento da dedução', 'alterar', '796, 0.00, Conceição Maurício, 2024-06-03, Morada, Morada, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', '234, 0.00, Conceição Maurício, 2024-06-03, Morada, Morada, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', 'Moisés', '2024-06-10 21:46:49'),
(3156, 'Processamento da dedução', 'alterar', '234, 0.00, Conceição Maurício, 2024-06-03, Morada, Morada, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', '234, 0.00, Conceição Maurício, 2024-06-03, Morada, Morada, Moisés Félix Henda Laurindo, Angolana, Masculino, we5', 'Moisés', '2024-06-10 21:48:08'),
(3157, 'Remuneração', 'inserir', '', 'Número do salário: 796, Valor: 0.00, Processamento: Conceição Maurício, Data do processamento: 2024-06-03, Cargo: Morada, Salário: Débora Bartolomeu, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-10 22:47:01'),
(3158, 'Remuneração', 'inserir', '', 'Número do salário: 796, Valor: 0.00, Processamento: Conceição Maurício, Data do processamento: 2024-06-03, Cargo: Morada, Salário: Débora Valentina, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-10 22:47:12'),
(3159, 'Remuneração', 'alterar', 'Número do salário: 796, Valor: 0.00, Processamento: Conceição Maurício, Data do processamento: 2024-06-03, Cargo: Morada, Salário: Débora Valentina, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Número do salário: 456, Valor: 9999.00, Processamento: Conceição Maurício, Data do processamento: 2024-06-03, Cargo: Morada, Salário: Débora Valentina, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-10 22:53:45'),
(3160, 'Processamento da dedução', 'eliminar', 'Número do salário: 456, Valor: 9999.00, Processamento: Conceição Maurício, Data do processamento: 2024-06-03, Cargo: Morada, Salário: Débora Valentina, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-10 22:59:24'),
(3161, 'Linguagem falada', 'inserir', '', 'Nível: Advanced, Observação da Linguagem falada: Não foi fácil, Linguagem: Débora Valentina, Observação da linguagem: Camacupa, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-11 00:34:18'),
(3162, 'Linguagem falada', 'inserir', '', 'Nível: Advanced, Observação da Linguagem falada: Não foi fácil, Linguagem: Débora Valentina, Observação da linguagem: Camacupa, Bié, Funcionário: Pedal, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-11 00:34:39'),
(3163, 'Linguagem falada', 'alterar', 'Nível: Advanced, Observação da Linguagem falada: Não foi fácil, Linguagem: Débora Valentina, Observação da linguagem: Camacupa, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Nível: Advanced, Observação da Linguagem falada: Foi fácil, Linguagem: Débora Valentina, Observação da linguagem: Camacupa, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-11 00:36:18'),
(3164, 'Linguagem falada', 'eliminar', 'Nível: Advanced, Observação da Linguagem falada: Foi fácil, Linguagem: Débora Valentina, Observação da linguagem: Camacupa, Bié, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-11 00:40:50'),
(3165, 'Tipo de empréstimo', 'inserir', '', 'Designação: 2, Installment: , Observação: Não foi fácil', 'Moisés', '2024-06-11 04:54:28'),
(3166, 'Tipo de empréstimo', 'inserir', '', 'Designação: 2, Installment: , Observação: Não foi fácil', 'Moisés', '2024-06-11 04:55:36'),
(3167, 'Tipo de empréstimo', 'inserir', '', 'Designação: 2, Installment: , Observação: Não foi fácil', 'Moisés', '2024-06-11 04:55:36'),
(3168, 'Tipo de empréstimo', 'inserir', '', 'Designação: 2, Installment: 1, Observação: Não foi fácil', 'Moisés', '2024-06-11 04:58:51'),
(3169, 'Tipo de empréstimo', 'alterar', 'Designação: 2, Installment: 1, Observação: Não foi fácil', 'Designação: Amílcar Cabral, Installment: 1, Observação: Manal', 'Moisés', '2024-06-11 05:07:35'),
(3170, 'Tipo de empréstimo', 'eliminar', 'Designação: 2, Installment: 1, Observação: Não foi fácil', '', 'Moisés', '2024-06-11 05:11:30'),
(3171, 'Empréstimo', 'inserir', '', '', 'Moisés', '2024-06-11 07:09:16'),
(3172, 'Empréstimo', 'inserir', '', 'Emprèstimo: , Data de registo: , Data do primeiro pagamento: , Data do ú8ltimo pagamento: , Valor: , Estado: , Observação: Amílcar Cabral, Número de prestação: 1, Observação: Manal, Funcionário: , Data de nascimento: , Nacionalidade: , Sexo: , Código do funcionário: ', 'Moisés', '2024-06-11 07:09:37'),
(3173, 'Empréstimo', 'inserir', '', 'Emprèstimo: , Data de registo: , Data do primeiro pagamento: , Data do ú8ltimo pagamento: , Valor: , Estado: , Observação: 2, Número de prestação: 1, Observação: Não foi fácil, Funcionário: , Data de nascimento: , Nacionalidade: , Sexo: , Código do funcionário: ', 'Moisés', '2024-06-11 07:09:38'),
(3174, 'Empréstimo', 'inserir', '', 'Emprèstimo: , Data de registo: , Data do primeiro pagamento: , Data do ú8ltimo pagamento: , Valor: , Estado: , Observação: 2, Número de prestação: 1, Observação: Não foi fácil, Funcionário: , Data de nascimento: , Nacionalidade: , Sexo: , Código do funcionário: ', 'Moisés', '2024-06-11 07:10:14'),
(3175, 'Empréstimo', 'inserir', '', '', 'Moisés', '2024-06-11 07:11:58'),
(3176, 'Tipo de empréstimo', 'alterar', '', '', 'Moisés', '2024-06-11 07:13:54'),
(3177, 'Empréstimo', 'alterar', '', '', 'Moisés', '2024-06-11 07:16:52'),
(3178, 'Empréstimo', 'eliminar', '', '', 'Moisés', '2024-06-11 07:20:21'),
(3179, 'Empréstimo', 'eliminar', 'Observação: 2, Data de registo: , Data do primeiro pagamento: , Data do ú8ltimo pagamento: , Valor: , Estado: , Número de prestação: 1, Observação: Não foi fácil, Funcionário: , Data de nascimento: , Nacionalidade: , Sexo: , Código do funcionário: ', '', 'Moisés', '2024-06-11 07:20:21'),
(3180, 'Empréstimo', 'eliminar', '', '', 'Moisés', '2024-06-11 07:21:35'),
(3181, 'Empréstimo', 'eliminar', 'Observação: 2, Data do primeiro pagamento: , Data do ú8ltimo pagamento: , Valor: , Estado: , Número de prestação: 1, Observação: Não foi fácil, Funcionário: , Data de nascimento: , Nacionalidade: , Sexo: , Código do funcionário: ', '', 'Moisés', '2024-06-11 07:21:35'),
(3182, 'Empréstimo', 'eliminar', '', '', 'Moisés', '2024-06-11 07:50:25'),
(3183, 'Empréstimo', 'eliminar', '', '', 'Moisés', '2024-06-11 07:50:25'),
(3184, 'Pagamento do Empréstimo', 'inserir', '', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Moisés', '2024-06-11 19:57:46'),
(3185, 'Pagamento do Empréstimo', 'inserir', '', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Moisés', '2024-06-11 19:58:11'),
(3186, 'Pagamento do Empréstimo', 'inserir', '', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Moisés', '2024-06-11 19:58:12'),
(3187, 'Pagamento do Empréstimo', 'inserir', '', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Moisés', '2024-06-11 19:58:13'),
(3188, 'Pagamento do Empréstimo', 'inserir', '', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Moisés', '2024-06-11 19:58:13'),
(3189, 'Pagamento do Empréstimo', 'alterar', '', '', 'Moisés', '2024-06-11 20:13:43'),
(3190, 'Pagamento do Empréstimo', 'alterar', '', '', 'Moisés', '2024-06-11 20:14:20'),
(3191, 'Pagamento do Empréstimo', 'alterar', '', '', 'Moisés', '2024-06-11 20:14:23'),
(3192, 'Pagamento do Empréstimo', 'alterar', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2000.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', 'Moisés', '2024-06-11 20:16:27'),
(3193, 'Pagamento Empréstimo', 'eliminar', 'Data do pagamento do Emprèstimo: 24, Valor pago: 2024.00, Empréstimo: Conceição Minga, Data do empréstimo: 2024-06-11 00:00:00, Estado: Em curso, Data do primeiro pagamento: 1996-03-05, Data do úlltimo pagamento: 2024-05-06, Valor de esmpréstimo: 4556.00', '', 'Moisés', '2024-06-11 20:20:08'),
(3194, 'Tipo de folga', 'inserir', '', 'Sigla: acr1, Designação: Azar, Observação: Hoje', 'Moisés', '2024-06-12 16:24:46'),
(3195, 'Tipo de folga', 'inserir', '', 'Sigla: acr1, Designação: Azar, Observação: Hoje', 'Moisés', '2024-06-12 16:24:50'),
(3196, 'Tipo de folga', 'inserir', '', 'Sigla: acr1, Designação: Azar, Observação: Hoje', 'Moisés', '2024-06-12 16:24:50'),
(3197, 'Tipo de folga', 'inserir', '', 'Sigla: acr1, Designação: Azar, Observação: Hoje', 'Moisés', '2024-06-12 16:24:51'),
(3198, 'Tipo de folga', 'alterar', 'Sigla: acr1, Designação: Azar, Observação: Hoje', 'Sigla: acr1, Designação: Azar2, Observação: Hoje', 'Moisés', '2024-06-12 16:27:23'),
(3199, 'Tipo de folga', 'eliminar', 'Designação: São, Observação: Hoje', '', 'Moisés', '2024-06-12 16:29:39'),
(3200, 'Tipo de folga', 'eliminar', '', '', 'Moisés', '2024-06-12 16:31:03'),
(3201, 'Tipo de folga', 'eliminar', 'Sigla: acr1, Designação: Azar, Observação: Hoje', '', 'Moisés', '2024-06-12 16:31:35'),
(3202, 'Tipo de folga', 'eliminar', '', '', 'Moisés', '2024-06-12 16:31:54'),
(3203, 'Funcionário', 'inserir', '', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-12 16:34:57'),
(3204, 'Funcionário', 'alterar', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-12 16:35:46'),
(3205, 'Funcionário', 'inserir', '', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-13 10:54:09'),
(3206, 'Funcionário', 'inserir', '', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-13 10:55:00'),
(3207, 'Funcionário', 'inserir', '', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-13 10:56:33'),
(3208, 'Funcionário', 'inserir', '', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-13 10:56:54'),
(3209, 'Funcionário', 'inserir', '', 'SQLSTATE[HY093]: Invalid parameter number: number of bound variables does not match number of tokens', 'Moisés', '2024-06-13 10:57:52'),
(3210, '../documents/employeePhoto/12/', 'image', '', '', 'Moisés', '2024-06-13 12:30:30'),
(3211, 'Falta', 'inserir', '', 'Data Registo: , Data de Falta: , Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Moisés', '2024-06-13 14:43:38'),
(3212, 'Falta', 'inserir', '', 'Data Registo: 2024-06-13 14:44:50, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Moisés', '2024-06-13 14:44:50'),
(3213, 'Falta', 'alterar', 'Data Registo: 2024-06-13 14:43:38, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Data Registo: 2024-06-13 14:43:38, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: , Observação: obs11 obs11', 'Moisés', '2024-06-13 14:48:18'),
(3214, 'Falta', 'inserir', '', 'Data Registo: 2024-06-13 14:49:12, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Moisés', '2024-06-13 14:49:12'),
(3215, 'Falta', 'inserir', '', 'Data Registo: 2024-06-13 14:49:13, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Moisés', '2024-06-13 14:49:13'),
(3216, 'Falta', 'eliminar', 'Data Registo: 2024-06-13 14:43:38, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: , Observação: obs11 obs11', '', 'Moisés', '2024-06-13 14:49:52'),
(3217, 'Falta', 'alterar', '', '', 'Moisés', '2024-06-13 14:59:46'),
(3218, 'Falta', 'alterar', 'Data Registo: 2024-06-13 14:44:50, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Data Registo: 2024-06-13 14:44:50, Data de Falta: 0000-00-00, Funcionário: Moisés Félix Henda Laurindo, Justificada: N, Observação: obs1 obs1', 'Moisés', '2024-06-13 15:54:36'),
(3219, 'Falta', 'alterar', '', '', 'Moisés', '2024-06-13 15:57:12'),
(3220, 'Falta', 'alterar', '', '', 'Moisés', '2024-06-13 15:57:37'),
(3221, 'Falta', 'alterar', '', '', 'Moisés', '2024-06-14 09:05:53'),
(3222, 'Capacitação', 'inserir', '', 'Observação da capacitação: Formation a iniciar, Estado: Pendente, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-14 10:34:58'),
(3223, 'Capacitação', 'inserir', '', 'Observação da capacitação: Formation a iniciar, Estado: Pendente, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Pedal, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-14 10:35:36'),
(3224, 'Capacitação', 'inserir', '', 'Observação da capacitação: Formation a iniciar, Estado: Pendente, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', 'Moisés', '2024-06-14 10:35:36'),
(3225, 'Empresa', 'inserir', '', '31, Designação comercialLazanha, Designação oficialccc, abreviação3, Taxa de identificaçãoLazanha, Endereçoccc, Telefone3, EmailLazanha, Número do registoccc', 'Moisés', '2024-06-14 16:00:36'),
(3226, '../documents/enterpriseLogo/28/', 'image', '', '', 'Moisés', '2024-06-14 16:03:33'),
(3227, '../documents/enterpriseLogo/28/', 'image', '', '', 'Moisés', '2024-06-14 16:04:09'),
(3228, '../documents/enterpriseLogo/28/', 'image', '', '', 'Moisés', '2024-06-14 16:05:41'),
(3229, 'Empresa', 'eliminar', '', '', 'Moisés', '2024-06-14 16:26:02'),
(3230, 'Empresa', 'inserir', '', '32, Designação comercialLazanha, Designação oficialccc, abreviação3, Taxa de identificaçãoLazanha, Endereçoccc, Telefone3, EmailLazanha, Número do registoccc', 'Moisés', '2024-06-14 16:27:11'),
(3231, 'Empresa', 'inserir', '', '33, Designação comercialLazanha, Designação oficialccc, abreviação3, Taxa de identificaçãoLazanha, Endereçoccc, Telefone3, EmailLazanha, Número do registoccc', 'Moisés', '2024-06-14 16:27:12'),
(3232, '../documents/enterpriseLogo/33/', 'image', '', '', 'Moisés', '2024-06-14 16:27:25'),
(3233, 'Empresa', 'eliminar', '33, Designação comercialLazanha, Designação oficialccc, abreviação3, Taxa de identificaçãoLazanha, Endereçoccc, Telefone3, EmailLazanha, Número do registoccc', '', 'Moisés', '2024-06-14 16:28:25'),
(3234, '../documents/enterpriseLogo/28/', 'image', '', '', 'Moisés', '2024-06-14 16:32:48'),
(3235, '../documents/enterpriseLogo/28/', 'image', '', '', 'Moisés', '2024-06-14 16:33:05'),
(3236, '../documents/enterpriseLogo/28/', 'image', '', '', 'Moisés', '2024-06-14 16:33:45'),
(3237, 'Falta', 'alterar', '', '', 'Moisés', '2024-06-14 16:53:35'),
(3238, 'Actualização de estado da Capacitação', 'alterar', 'Observação da capacitação: Formation a iniciar, Estado: Pendente, Formação: São mora, Início: 2024-07-19, Fim: 2025-07-02, Local: É bom, Objectivo: Arroz a feijão, Funcionário: Moisés Félix Henda Laurindo, Nacionalidade: Angolana, Sexo: Masculino, Código do funcionário: we5', '', 'Moisés', '2024-06-17 08:39:36'),
(3239, 'Vacation', 'inserir', '', 'Ano:: 0, Início: , Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 09:38:40'),
(3240, 'Vacation', 'inserir', '', 'Ano:: 0, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 09:39:08'),
(3241, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 10:58:04'),
(3242, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 10:58:06'),
(3243, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 10:58:07'),
(3244, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 10:58:08'),
(3245, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 10:58:10'),
(3246, 'Férias', 'alterar', 'Ano:: 0, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Ano:: 2024, Início: 2024-07-16, Fim: 2024-08-09, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 10:59:54'),
(3247, 'Férias', 'alterar', 'Ano:: 2024, Início: 2024-07-16, Fim: 2024-08-09, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Ano:: 2024, Início: 2024-07-16, Fim: 2024-08-09, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 11:00:20'),
(3248, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 11:03:54'),
(3249, 'Vacation', 'inserir', '', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 11:03:54'),
(3250, 'Férias', 'eliminar', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', '', 'Moisés', '2024-06-17 11:04:16'),
(3251, 'Férias', 'eliminar', 'Ano:: 2024, Início: 2024-07-12, Fim: 2024-08-05, Estado: Pendente, Observação: Valeu, Funcionário: Pedal', '', 'Moisés', '2024-06-17 11:04:16'),
(3252, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:52:07'),
(3253, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:52:16'),
(3254, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:52:17'),
(3255, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:52:18'),
(3256, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:52:18'),
(3257, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:52:19'),
(3258, 'carreira', 'alterar', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Description: Boa Minga, Início: 2024-07-16, Fim: 2024-08-09, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:53:52'),
(3259, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:55:54'),
(3260, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:55:55'),
(3261, 'carreira', 'inserir', '', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', 'Moisés', '2024-06-17 15:55:56'),
(3262, 'carreira', 'eliminar', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', '', 'Moisés', '2024-06-17 15:56:10'),
(3263, 'carreira', 'eliminar', 'Description: Olá, Início: 2024-07-12, Fim: 2024-08-05, Observação: Valeu, Funcionário: Pedal', '', 'Moisés', '2024-06-17 15:56:10'),
(3264, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3265, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3266, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3267, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3268, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3269, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3270, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3271, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3272, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3273, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3274, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3275, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:21'),
(3276, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3277, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3278, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3279, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3280, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3281, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3282, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3283, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3284, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3285, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3286, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3287, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3288, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3289, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3290, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3291, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3292, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3293, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3294, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3295, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3296, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3297, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3298, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3299, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3300, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3301, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3302, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3303, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:36:22'),
(3304, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3305, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3306, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3307, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3308, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3309, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3310, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3311, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3312, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:26'),
(3313, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3314, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3315, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3316, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3317, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3318, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3319, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3320, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3321, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3322, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27'),
(3323, 'Comparecimento', 'inserir', '', 'SQLSTATE[42S22]: Column not found: 1054 Unknown column \'period.initial\' in \'field list\'', 'Moisés', '2024-06-19 17:39:27');

-- --------------------------------------------------------

--
-- Estrutura da tabela `capacitation`
--

DROP TABLE IF EXISTS `capacitation`;
CREATE TABLE IF NOT EXISTS `capacitation` (
  `id_training` int NOT NULL,
  `id_employee` int NOT NULL,
  `status` varchar(20) NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id_training`,`id_employee`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `capacitation`
--

INSERT INTO `capacitation` (`id_training`, `id_employee`, `status`, `obs`) VALUES
(2, 1, '', 'Moisés Laurindo'),
(3, 1, 'Concluido', 'Formation a iniciar'),
(3, 2, 'Pendente', 'Formation a iniciar'),
(3, 3, 'Pendente', 'Formation a iniciar');

-- --------------------------------------------------------

--
-- Estrutura da tabela `career`
--

DROP TABLE IF EXISTS `career`;
CREATE TABLE IF NOT EXISTS `career` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` varchar(120) NOT NULL,
  `initial_date` date NOT NULL,
  `end_date` date NOT NULL,
  `obs` varchar(120) NOT NULL,
  `id_employee` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `career`
--

INSERT INTO `career` (`id`, `description`, `initial_date`, `end_date`, `obs`, `id_employee`) VALUES
(1, 'Olá', '2024-07-12', '2024-08-05', 'Valeu', 2),
(2, 'Boa Minga', '2024-07-16', '2024-08-09', 'Valeu', 2),
(3, 'Olá', '2024-07-12', '2024-08-05', 'Valeu', 2),
(4, 'Olá', '2024-07-12', '2024-08-05', 'Valeu', 2),
(5, 'Olá', '2024-07-12', '2024-08-05', 'Valeu', 2),
(6, 'Olá', '2024-07-12', '2024-08-05', 'Valeu', 2),
(8, 'Olá', '2024-07-12', '2024-08-05', 'Valeu', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contract`
--

DROP TABLE IF EXISTS `contract`;
CREATE TABLE IF NOT EXISTS `contract` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(90) NOT NULL,
  `initial_date` date NOT NULL,
  `end_date` date NOT NULL,
  `contract_code` varchar(15) NOT NULL,
  `obs` varchar(120) NOT NULL,
  `id_contract_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_contract_type` (`id_contract_type`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `contract`
--

INSERT INTO `contract` (`id`, `designation`, `initial_date`, `end_date`, `contract_code`, `obs`, `id_contract_type`) VALUES
(2, 'Boa Minga', '2024-07-16', '2024-08-09', 'Olá', 'Valeu', 2),
(3, 'Olá', '2024-07-12', '2024-08-05', 'Olá', 'Valeu', 2),
(4, 'Olá', '2024-07-12', '2024-08-05', 'Olá', 'Valeu', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contract_type`
--

DROP TABLE IF EXISTS `contract_type`;
CREATE TABLE IF NOT EXISTS `contract_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(90) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `contract_type`
--

INSERT INTO `contract_type` (`id`, `designation`, `obs`) VALUES
(2, 'Débora Bartolomeu', 'Valeu'),
(3, 'Olá', 'Valeu');

-- --------------------------------------------------------

--
-- Estrutura da tabela `day_off`
--

DROP TABLE IF EXISTS `day_off`;
CREATE TABLE IF NOT EXISTS `day_off` (
  `id` int NOT NULL AUTO_INCREMENT,
  `initial_date` varchar(120) NOT NULL,
  `end_date` varchar(120) NOT NULL,
  `id_employee` int NOT NULL,
  `id_day_off_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`),
  KEY `id_day_off_type` (`id_day_off_type`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `day_off`
--

INSERT INTO `day_off` (`id`, `initial_date`, `end_date`, `id_employee`, `id_day_off_type`) VALUES
(1, '2024-08-12', '2025-05-22', 1, 1),
(2, '2024-08-23', '2025-05-22', 1, 1),
(3, '2024-08-23', '2025-05-22', 1, 1),
(4, '2024-08-23', '2025-05-22', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `day_off_type`
--

DROP TABLE IF EXISTS `day_off_type`;
CREATE TABLE IF NOT EXISTS `day_off_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `day_off_type`
--

INSERT INTO `day_off_type` (`id`, `designation`, `obs`) VALUES
(1, 'Mambos', 'Hoje'),
(3, 'São', 'Hoje');

-- --------------------------------------------------------

--
-- Estrutura da tabela `deduction`
--

DROP TABLE IF EXISTS `deduction`;
CREATE TABLE IF NOT EXISTS `deduction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `deduction`
--

INSERT INTO `deduction` (`id`, `designation`, `obs`) VALUES
(1, 'IRT', '2025-05-22'),
(3, 'Segurança Social', '2025-05-22'),
(4, 'Morada', '2025-05-22'),
(5, 'Morada', '2025-05-22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `deduction_processing`
--

DROP TABLE IF EXISTS `deduction_processing`;
CREATE TABLE IF NOT EXISTS `deduction_processing` (
  `id_processing` int NOT NULL,
  `id_office` int NOT NULL,
  `id_deduction` int NOT NULL,
  `id_employee` int NOT NULL,
  `number_deduction` int NOT NULL,
  `value` decimal(14,2) NOT NULL,
  PRIMARY KEY (`id_processing`,`id_office`,`id_deduction`,`id_employee`),
  KEY `id_office` (`id_office`),
  KEY `id_salary` (`id_deduction`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `deduction_processing`
--

INSERT INTO `deduction_processing` (`id_processing`, `id_office`, `id_deduction`, `id_employee`, `number_deduction`, `value`) VALUES
(1, 2, 4, 1, 234, '0.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `acronym` varchar(20) NOT NULL,
  `description` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `department`
--

INSERT INTO `department` (`id`, `acronym`, `description`, `obs`) VALUES
(1, 'dep1', 'dep1 dep1', 'dep1 dep1'),
(2, 'acr1', 'Azar2', 'Hoje'),
(3, 'acr1', 'Azar', 'Hoje'),
(5, 'acr1', 'Azar', 'Hoje');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dependent`
--

DROP TABLE IF EXISTS `dependent`;
CREATE TABLE IF NOT EXISTS `dependent` (
  `id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(120) NOT NULL,
  `birth_place` varchar(100) NOT NULL,
  `birth_date` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `kinship_degree` varchar(30) NOT NULL,
  `id_employee` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `dependent`
--

INSERT INTO `dependent` (`id`, `full_name`, `birth_place`, `birth_date`, `gender`, `kinship_degree`, `id_employee`) VALUES
(1, 'Moisés Domingos', 'Camacupa, Bié', '2022-05-09', 'Masculino', 'Sanguíneo', 1),
(2, 'Moisés Domingos', 'Camacupa, Bié', '2022-05-09', 'Masculino', 'Sanguíneo', 1),
(3, 'Moisés Domingos', 'Camacupa, Bié', '2022-05-09', 'Masculino', 'Sanguíneo', 1),
(4, 'Moisés Domingos', 'Camacupa, Bié', '2022-05-09', 'Masculino', 'Sanguíneo', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `education`
--

DROP TABLE IF EXISTS `education`;
CREATE TABLE IF NOT EXISTS `education` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `education`
--

INSERT INTO `education` (`id`, `designation`, `obs`) VALUES
(1, 'Amor ao dinheiro', 'Andulo, Bié'),
(2, 'Amor à Pátria', 'Camacupa, Bié'),
(3, 'Amor à Pátria', 'Camacupa, Bié');

-- --------------------------------------------------------

--
-- Estrutura da tabela `employee`
--

DROP TABLE IF EXISTS `employee`;
CREATE TABLE IF NOT EXISTS `employee` (
  `id` int NOT NULL AUTO_INCREMENT,
  `full_name` varchar(100) NOT NULL,
  `birth_place` varchar(60) NOT NULL,
  `birth_date` date NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `employee_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `other_id` varchar(20) NOT NULL,
  `father_name` varchar(90) NOT NULL,
  `mother_name` varchar(90) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `email` varchar(90) NOT NULL,
  `address` varchar(120) NOT NULL,
  `id_contract` int NOT NULL,
  `id_department` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_contract` (`id_contract`),
  KEY `id_department` (`id_department`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `employee`
--

INSERT INTO `employee` (`id`, `full_name`, `birth_place`, `birth_date`, `nationality`, `gender`, `employee_code`, `other_id`, `father_name`, `mother_name`, `email`, `address`, `id_contract`, `id_department`) VALUES
(1, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', 'A2', 'Moisés Laurindo', 'Isabel Henda', '', '', 2, 1),
(2, 'Pedal', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'A3', 'A3', 'Moisés Laurindo', 'Isabel Henda', '', '', 2, 2),
(3, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', 'A6', 'Moisés Laurindo', 'Isabel Henda', '', '', 2, 1),
(6, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', 'A7', 'Moisés Laurindo', 'Isabel Henda', '', '', 2, 1),
(7, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', 'A8', 'Moisés Laurindo', 'Isabel Henda', '', '', 2, 2),
(8, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', '85f', 'Moisés Laurindo', 'Isabel Henda', 'email1', 'ad1 ad1', 2, 2),
(9, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', '85f', 'Moisés Laurindo', 'Isabel Henda', 'email11', 'ad1 ad1', 2, 2),
(10, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', '85f', 'Moisés Laurindo', 'Isabel Henda', 'email111', 'ad1 ad1', 2, 2),
(11, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', '85f', 'Moisés Laurindo', 'Isabel Henda', 'email112', 'ad1 ad1', 2, 2),
(12, 'Moisés Félix Henda Laurindo', 'Camacupa, Bié', '2024-08-05', 'Angolana', 'Masculino', 'we5', '85f', 'Moisés Laurindo', 'Isabel Henda', 'email113', 'ad1 ad1', 2, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `enterprise`
--

DROP TABLE IF EXISTS `enterprise`;
CREATE TABLE IF NOT EXISTS `enterprise` (
  `id` int NOT NULL AUTO_INCREMENT,
  `commercial_designation` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `oficial_designation` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `abbreviation` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tax_identification_number` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `address` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telephone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `register_number` varchar(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `enterprise`
--

INSERT INTO `enterprise` (`id`, `commercial_designation`, `oficial_designation`, `abbreviation`, `tax_identification_number`, `address`, `telephone`, `email`, `register_number`) VALUES
(28, 'Massano', 'ccc', 'ent', 'Lazanha', 'ccc', '3', 'Lazanha', 'ccc'),
(29, 'Lazanha', 'ccc', 'ent', 'Lazanha', 'ccc', '3', 'Lazanha', 'ccc'),
(31, 'Lazanha', 'ccc', '3', 'Lazanha', 'ccc', '3', 'Lazanha', 'ccc'),
(32, 'Lazanha', 'ccc', '3', 'Lazanha', 'ccc', '3', 'Lazanha', 'ccc');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fixed_deduction`
--

DROP TABLE IF EXISTS `fixed_deduction`;
CREATE TABLE IF NOT EXISTS `fixed_deduction` (
  `id_office` int NOT NULL,
  `id_deduction` int NOT NULL,
  `value` decimal(14,2) NOT NULL,
  `id_measurement_unit_deduction` int NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id_office`,`id_deduction`),
  KEY `id_salary` (`id_deduction`),
  KEY `id_measutement_unit_salary` (`id_measurement_unit_deduction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `fixed_deduction`
--

INSERT INTO `fixed_deduction` (`id_office`, `id_deduction`, `value`, `id_measurement_unit_deduction`, `obs`) VALUES
(3, 1, '65.00', 1, 'Bié,Andulo'),
(3, 3, '65.00', 1, 'Bié,Andulo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fixed_salary`
--

DROP TABLE IF EXISTS `fixed_salary`;
CREATE TABLE IF NOT EXISTS `fixed_salary` (
  `id_office` int NOT NULL,
  `id_salary` int NOT NULL,
  `value` decimal(14,2) NOT NULL,
  `id_measutement_unit_salary` int NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id_office`,`id_salary`),
  KEY `id_salary` (`id_salary`),
  KEY `id_measutement_unit_salary` (`id_measutement_unit_salary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `fixed_salary`
--

INSERT INTO `fixed_salary` (`id_office`, `id_salary`, `value`, `id_measutement_unit_salary`, `obs`) VALUES
(3, 1, '66.00', 1, 'Bié,Andulo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `language`
--

DROP TABLE IF EXISTS `language`;
CREATE TABLE IF NOT EXISTS `language` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `language`
--

INSERT INTO `language` (`id`, `designation`, `obs`) VALUES
(1, 'Débora Valentina', 'Camacupa, Bié'),
(2, 'Português', 'Camacupa, Bié'),
(3, 'Débora Valentina', 'Camacupa, Bié');

-- --------------------------------------------------------

--
-- Estrutura da tabela `license`
--

DROP TABLE IF EXISTS `license`;
CREATE TABLE IF NOT EXISTS `license` (
  `id` int NOT NULL AUTO_INCREMENT,
  `initial_date` date NOT NULL,
  `end_date` date NOT NULL,
  `id_employee` int NOT NULL,
  `id_license_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`),
  KEY `id_license_type` (`id_license_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `license_type`
--

DROP TABLE IF EXISTS `license_type`;
CREATE TABLE IF NOT EXISTS `license_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `loan`
--

DROP TABLE IF EXISTS `loan`;
CREATE TABLE IF NOT EXISTS `loan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `register_date` datetime NOT NULL,
  `date_first_payment` date NOT NULL,
  `date_last_payment` date NOT NULL,
  `value` decimal(14,2) NOT NULL,
  `id_employee` int NOT NULL,
  `id_type_loan` int NOT NULL,
  `state` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`),
  KEY `id_type_loan` (`id_type_loan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `loan`
--

INSERT INTO `loan` (`id`, `designation`, `register_date`, `date_first_payment`, `date_last_payment`, `value`, `id_employee`, `id_type_loan`, `state`) VALUES
(1, 'Conceição Minga', '2024-06-11 00:00:00', '1996-03-05', '2024-05-06', '4556.00', 1, 2, 'Em curso'),
(2, 'Conceição Maurício', '2024-06-11 00:00:00', '1996-03-05', '2024-05-06', '4556.00', 2, 2, 'Em curso'),
(3, 'Conceição Maurício', '2024-06-11 00:00:00', '1996-03-05', '2024-05-06', '4556.00', 2, 2, 'Em curso');

-- --------------------------------------------------------

--
-- Estrutura da tabela `loan_payment`
--

DROP TABLE IF EXISTS `loan_payment`;
CREATE TABLE IF NOT EXISTS `loan_payment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `register_date` int NOT NULL,
  `value` decimal(14,2) NOT NULL,
  `id_loan` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_loan` (`id_loan`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `loan_payment`
--

INSERT INTO `loan_payment` (`id`, `register_date`, `value`, `id_loan`) VALUES
(1, 24, '2000.00', 1),
(2, 24, '2024.00', 1),
(3, 24, '2024.00', 1),
(4, 24, '2024.00', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `loan_type`
--

DROP TABLE IF EXISTS `loan_type`;
CREATE TABLE IF NOT EXISTS `loan_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `installment_number` int NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `loan_type`
--

INSERT INTO `loan_type` (`id`, `designation`, `installment_number`, `obs`) VALUES
(2, 'Amílcar Cabral', 1, 'Manal'),
(3, '2', 1, 'Não foi fácil'),
(4, '2', 1, 'Não foi fácil');

-- --------------------------------------------------------

--
-- Estrutura da tabela `measurement_unit_deduction`
--

DROP TABLE IF EXISTS `measurement_unit_deduction`;
CREATE TABLE IF NOT EXISTS `measurement_unit_deduction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `symbol` varchar(20) NOT NULL,
  `designation` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `measurement_unit_deduction`
--

INSERT INTO `measurement_unit_deduction` (`id`, `symbol`, `designation`) VALUES
(1, 'Camacupa, Bié', 'Débora Valentina'),
(2, 'Camacupa, Bié', 'Debate'),
(3, 'Camacupa, Bié', 'Débora Valentina'),
(5, 'Camacupa, Bié', 'Débora Valentina');

-- --------------------------------------------------------

--
-- Estrutura da tabela `measurement_unit_salary`
--

DROP TABLE IF EXISTS `measurement_unit_salary`;
CREATE TABLE IF NOT EXISTS `measurement_unit_salary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `symbol` varchar(20) NOT NULL,
  `designation` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `measurement_unit_salary`
--

INSERT INTO `measurement_unit_salary` (`id`, `symbol`, `designation`) VALUES
(1, 'Camacupa, Bié', 'Débora Valentina'),
(2, 'Camacupa, Bié', 'Debate'),
(3, 'Camacupa, Bié', 'Débora Valentina');

-- --------------------------------------------------------

--
-- Estrutura da tabela `missing`
--

DROP TABLE IF EXISTS `missing`;
CREATE TABLE IF NOT EXISTS `missing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration_date` datetime NOT NULL,
  `missing_date` date NOT NULL,
  `id_employee` int NOT NULL,
  `justified` varchar(5) NOT NULL,
  `obs` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `missing`
--

INSERT INTO `missing` (`id`, `registration_date`, `missing_date`, `id_employee`, `justified`, `obs`) VALUES
(2, '2024-06-13 14:44:50', '0000-00-00', 12, 'S', 'obs11 obs11'),
(3, '2024-06-13 14:49:12', '0000-00-00', 12, 'S', 'obs11 obs11'),
(4, '2024-06-13 14:49:13', '0000-00-00', 12, 'S', 'obs11 obs11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `office`
--

DROP TABLE IF EXISTS `office`;
CREATE TABLE IF NOT EXISTS `office` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `office`
--

INSERT INTO `office` (`id`, `designation`, `obs`) VALUES
(2, 'Morada', '2025-05-22'),
(3, 'Morada', '2025-05-22'),
(4, 'Morada', '2025-05-22'),
(5, 'Morada', '2025-05-22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `other_formation`
--

DROP TABLE IF EXISTS `other_formation`;
CREATE TABLE IF NOT EXISTS `other_formation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `initial_year` int NOT NULL,
  `end_year` int NOT NULL,
  `obs` varchar(120) NOT NULL,
  `id_employee` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `other_formation`
--

INSERT INTO `other_formation` (`id`, `designation`, `initial_year`, `end_year`, `obs`, `id_employee`) VALUES
(1, 'Pesca', 65, 1, 'Bié,Andulo', 1),
(2, 'Agricultura', 65, 1, 'Bié,Andulo', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `performance`
--

DROP TABLE IF EXISTS `performance`;
CREATE TABLE IF NOT EXISTS `performance` (
  `id_assessment` int NOT NULL,
  `id_employee` int NOT NULL,
  `grade_obtained` decimal(10,0) NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id_assessment`,`id_employee`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `performance`
--

INSERT INTO `performance` (`id_assessment`, `id_employee`, `grade_obtained`, `obs`) VALUES
(3, 2, '23', 'Olá Pessoal'),
(3, 3, '23', 'Olá Pessoal');

-- --------------------------------------------------------

--
-- Estrutura da tabela `period`
--

DROP TABLE IF EXISTS `period`;
CREATE TABLE IF NOT EXISTS `period` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `initial_period` time NOT NULL,
  `end_period` time NOT NULL,
  `id_period_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_period_type` (`id_period_type`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `period`
--

INSERT INTO `period` (`id`, `designation`, `initial_period`, `end_period`, `id_period_type`) VALUES
(1, 'Manhã', '08:00:00', '12:00:00', 2),
(2, 'Tarde', '13:00:00', '17:00:00', 3),
(3, 'Pausa1', '12:00:00', '13:00:00', 6),
(8, 'Noite', '18:00:00', '22:00:00', 4),
(9, 'Pausa2', '22:00:00', '23:00:00', 6),
(10, 'Diário', '08:00:00', '15:00:00', 7),
(11, 'Noturno', '23:00:00', '03:00:00', 9),
(12, 'Pausa3', '03:00:00', '04:00:00', 6),
(13, 'Madrugada', '04:00:00', '08:00:00', 8);

-- --------------------------------------------------------

--
-- Estrutura da tabela `period_type`
--

DROP TABLE IF EXISTS `period_type`;
CREATE TABLE IF NOT EXISTS `period_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `period_type`
--

INSERT INTO `period_type` (`id`, `designation`, `obs`) VALUES
(2, 'Manhã', '2'),
(3, 'Tarde', '2'),
(4, 'Noite', '2'),
(6, 'Pausa', 'Pausa'),
(7, 'Diário', 'Diário'),
(8, 'Madrugada', 'Madrugada'),
(9, 'Noturno', 'Noturno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `permition`
--

DROP TABLE IF EXISTS `permition`;
CREATE TABLE IF NOT EXISTS `permition` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `comment` varchar(90) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `permition`
--

INSERT INTO `permition` (`id`, `designation`, `comment`) VALUES
(49, 'fff', 'ggg'),
(51, 'Lazanha', 'ccc'),
(52, 'Lazanha', 'ccc'),
(53, 'Lazanha', 'ccc'),
(54, 'Lazanha', 'ccc'),
(55, 'Lazanha', 'ccc');

-- --------------------------------------------------------

--
-- Estrutura da tabela `processing`
--

DROP TABLE IF EXISTS `processing`;
CREATE TABLE IF NOT EXISTS `processing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `processing_date` date NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `processing`
--

INSERT INTO `processing` (`id`, `designation`, `processing_date`, `obs`) VALUES
(1, 'Conceição Maurício', '2024-06-03', '2025-05-22'),
(2, 'Conceição Luís Maurício', '2024-06-03', '2025-05-22'),
(3, 'Conceição Luís Maurício', '2024-06-03', '2025-05-22'),
(5, 'Conceição Luís Maurício', '0000-00-00', '2025-05-22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id_user` int NOT NULL,
  `id_permition` int NOT NULL,
  `obs` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_user`,`id_permition`),
  KEY `id_elemento_sistema_perfil` (`id_permition`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registo do perfil do utilizador';

--
-- Extraindo dados da tabela `profile`
--

INSERT INTO `profile` (`id_user`, `id_permition`, `obs`) VALUES
(43, 49, 'Perfil criado automaticamente'),
(43, 52, 'Perfil criado automaticamente'),
(43, 53, 'Perfil criado automaticamente');

-- --------------------------------------------------------

--
-- Estrutura da tabela `remuneration`
--

DROP TABLE IF EXISTS `remuneration`;
CREATE TABLE IF NOT EXISTS `remuneration` (
  `id_processing` int NOT NULL,
  `id_office` int NOT NULL,
  `id_salary` int NOT NULL,
  `id_employee` int NOT NULL,
  `number_salary` int NOT NULL,
  `value` decimal(14,2) NOT NULL,
  PRIMARY KEY (`id_processing`,`id_office`,`id_salary`,`id_employee`),
  KEY `id_employee` (`id_employee`),
  KEY `id_office` (`id_office`),
  KEY `id_salary` (`id_salary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `remuneration`
--

INSERT INTO `remuneration` (`id_processing`, `id_office`, `id_salary`, `id_employee`, `number_salary`, `value`) VALUES
(1, 2, 1, 1, 796, '0.00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `salary`
--

DROP TABLE IF EXISTS `salary`;
CREATE TABLE IF NOT EXISTS `salary` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(120) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `salary`
--

INSERT INTO `salary` (`id`, `designation`, `obs`) VALUES
(1, 'Débora Bartolomeu', 'Camacupa, Bié'),
(2, 'Débora Valentina', 'Camacupa, Bié');

-- --------------------------------------------------------

--
-- Estrutura da tabela `spoken_language`
--

DROP TABLE IF EXISTS `spoken_language`;
CREATE TABLE IF NOT EXISTS `spoken_language` (
  `id_employee` int NOT NULL,
  `id_language` int NOT NULL,
  `level` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id_employee`,`id_language`),
  KEY `id_language` (`id_language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `spoken_language`
--

INSERT INTO `spoken_language` (`id_employee`, `id_language`, `level`, `obs`) VALUES
(2, 1, 'Advanced', 'Não foi fácil');

-- --------------------------------------------------------

--
-- Estrutura da tabela `store`
--

DROP TABLE IF EXISTS `store`;
CREATE TABLE IF NOT EXISTS `store` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telephone` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `address` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_enterprise` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_enterprise` (`id_enterprise`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `store`
--

INSERT INTO `store` (`id`, `designation`, `telephone`, `address`, `email`, `id_enterprise`) VALUES
(28, 'Mohamede', '932204920', 'Luanda', 'moises@gmail.com', 28),
(30, 'Conceição', '7', 'concei@gmail.com', '123', 28),
(31, 'Conceição', '7', 'concei@gmail.com', '123', 28);

-- --------------------------------------------------------

--
-- Estrutura da tabela `training`
--

DROP TABLE IF EXISTS `training`;
CREATE TABLE IF NOT EXISTS `training` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `initial_date` date NOT NULL,
  `end_date` date NOT NULL,
  `local` varchar(60) NOT NULL,
  `goal` text NOT NULL,
  `id_training_type` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_training_type` (`id_training_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `training`
--

INSERT INTO `training` (`id`, `designation`, `initial_date`, `end_date`, `local`, `goal`, `id_training_type`) VALUES
(2, 'Mica', '2024-07-19', '2025-07-02', 'É bom', 'Arroz a feijão', 1),
(3, 'São mora', '2024-07-19', '2025-07-02', 'É bom', 'Arroz a feijão', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `training_type`
--

DROP TABLE IF EXISTS `training_type`;
CREATE TABLE IF NOT EXISTS `training_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `obs` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `training_type`
--

INSERT INTO `training_type` (`id`, `designation`, `obs`) VALUES
(1, 'Papa', 'É bom'),
(2, 'Arroz a feijão', 'É bom'),
(3, 'Arroz a feijão', 'É bom');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `complete_name` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telephone` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(90) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `license` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_store` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tab_utilizador_ibfk_1` (`id_store`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `complete_name`, `name`, `telephone`, `email`, `password`, `license`, `id_store`) VALUES
(43, 'Moisés Félix Henda Laurindo', 'Moisés', '932204920', 'cleuber@gmail.com', '$2y$10$M7G5vaJr.v2QkyqznU4aVObs3MLoGkhfy2Vb4/eRf23b6DmIZ1qn2', 'mmmm', 28);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_connected`
--

DROP TABLE IF EXISTS `user_connected`;
CREATE TABLE IF NOT EXISTS `user_connected` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token_created` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date_created` date NOT NULL,
  `id_user` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=926 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `user_connected`
--

INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(644, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNTUzOTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xdrfoV2ciw1oqYEyCu9kUxxJzCRPzkKXMM26qXYonb8=', '2024-05-30', 43),
(645, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.H6Hz1fyoED2wwJ04cmflf1K1yvwJ2psj+2/UH4JObTc=', '2024-05-30', 43),
(646, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.A0omtRbATC5jnUX/BGoHXS06v6mHXq1NL0EXgi1X8B8=', '2024-05-30', 43),
(647, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.oGykod9pHHDDSlODN3tZdRHBGEI25iTerzJ7Lt+QWC0=', '2024-05-30', 43),
(648, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.p3qb8tZrO60z54xCXUI7+xZehhXQkCeWkH97fvs9wdw=', '2024-05-30', 43),
(649, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.iwoW+3OI4IUotAIF9Ufao9jdBWVs9sUIQfb9qkcXp7g=', '2024-05-30', 43),
(650, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.iwoW+3OI4IUotAIF9Ufao9jdBWVs9sUIQfb9qkcXp7g=', '2024-05-30', 43),
(651, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.PfOmbRQkQJyyUz/Y6AHfBLLDzOdPx4K8rrF5oYJe3bM=', '2024-05-30', 43),
(652, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.aEdl0Z4/oCjSWr0HEj4aCcJCFlVzd1W8Edlmrs0gUhY=', '2024-05-30', 43),
(653, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.oZyDdUlV4xdm4lNIfIZKBrIHzMbgtQdrjQZVxU+A97M=', '2024-05-30', 43),
(654, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0MzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.oZyDdUlV4xdm4lNIfIZKBrIHzMbgtQdrjQZVxU+A97M=', '2024-05-30', 43),
(655, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNjk0NDAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.3Hxv50EekErTIzDgpXWnyL3kztPPBPKR/T2sSQQctlI=', '2024-05-30', 43),
(656, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzIyMjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cZhWLbOO7ilp+X60KvsuxbATx6vlrxJT7dAsjP3LunE=', '2024-05-30', 43),
(657, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzI2OTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.30pQnTDP2uHyn12du4U1rKCKtddS+tb7Rc+BD6XzJTE=', '2024-05-30', 43),
(658, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzI3MDAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.grJ27dnSg1ZZ6xZt8095KueoO2toci0aeh2+6NbrqRc=', '2024-05-30', 43),
(659, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzMwMjYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.0gpGrvsJTj1tuGqVEJmuhvo190ASUbyqxP31aN/JH+k=', '2024-05-30', 43),
(660, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzMwMjYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.0gpGrvsJTj1tuGqVEJmuhvo190ASUbyqxP31aN/JH+k=', '2024-05-30', 43),
(661, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzUyNDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.yQSZnXxCHeoj00ir5bxO0hM+vZR5lqhbEXJayGMh2ok=', '2024-05-30', 43),
(662, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzUzMjEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.q44fVjbICwAlHPtBjviCKlDmCr7phKOXk7KDp44cSH4=', '2024-05-30', 43),
(663, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzUzMjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.rhAHdQsOx1kCoXEQBHqQ/1sDRv00beovIk3UQTfASrk=', '2024-05-30', 43),
(664, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzYzMDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.uYdTjviUWSU/GCgiJ3t+cOfn4HgWCh+FEV91g6eP4t8=', '2024-05-30', 43),
(665, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzYzMDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.g/unMXTkVflnDz1pvg8eEZk+UkKC3PCEgyI4TjFGKdY=', '2024-05-30', 43),
(666, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzcyNTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QPym170mpasGWVJz/iyVVtl7iBBmViNxjeL/xucWhTg=', '2024-05-30', 43),
(667, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcwNzczMjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Eqgfd+8km7crI80Z/lc+/L7vk6E7OoxBS6PcSSAs6BU=', '2024-05-30', 43),
(668, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNTExNTgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Mu12QwWomPpn0n18mxzfIawpnSiRGDeXk7paqWST5sU=', '2024-05-31', 43),
(669, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNTExNTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.rDwSz1zVuo0ObL+UOlRICS6tPLQVURgu2CTnJqFPWLk=', '2024-05-31', 43),
(670, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNTM5MTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.hTOfAXX27wyAx8i2SkC9M1yQtAccRsJXeKkR+i9g6N4=', '2024-05-31', 43),
(671, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNzk1MDMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.+LmJOx4lqMFxCz+hg70Orz5FyXH8iRSATTtXczglnP4=', '2024-05-31', 43),
(672, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNzk1MDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.2tULnoNBd25zIVapj+85Npu3PMGcaAtx2l7C1Rq4JSc=', '2024-05-31', 43),
(673, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNzk1MDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.HsNp46SEdDFJgPqx8u8e0wO/OnrwQ3oD2SyfSdmpAj0=', '2024-05-31', 43),
(674, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNzk1MDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xrsUGXhYlsuWSfeiy3saQhfIB9ihavkq5TWc/p/fTyw=', '2024-05-31', 43),
(675, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxNzk4NjQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.nLpDh+IrUmccTyPhpYvWdvoB1qtas6phEpdAfe5BIII=', '2024-05-31', 43),
(676, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxODA2NDksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xwv6hDBDZQoh30OTrc/oXP6mMxru3m1nuh2hqkt20hc=', '2024-05-31', 43),
(677, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxODQ4MTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.3gDQNl+I0qXKR+4nEtf9IpH0r9fMSGKF/3ja35mjpG4=', '2024-05-31', 43),
(678, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyMjk3MjcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.zHir3uJC4INxuaYD1fmE5V6QSpppGatuTngjZtBiAqA=', '2024-06-01', 43),
(679, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyMjk3MjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lV7ZcUHJqW62cMVM2pS6fyrXwNXSOxtgut/vknNBkaY=', '2024-06-01', 43),
(680, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyMzA0NTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.TVkdJEaJGch10Ttd+SqSGcb9L1aRUHK4qqPPtWWKM54=', '2024-06-01', 43),
(681, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyMzA0NTYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.rGQv84yAbfxGeU6gd0aFqY370Agojp1Sn7hcSI7Ky9Q=', '2024-06-01', 43),
(682, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyMzA4NjAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.hTE92+knGRF4FdYFHKOtKhHBxR3YHHr5GJFKgex4OgE=', '2024-06-01', 43),
(683, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcxODg2MTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xsh+mAXrG4XQrSZLTPHPwHmtjZ9uAK4Yn450IuhRJW8=', '2024-05-31', 43),
(684, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyMzIzODYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.AryqYxdMyU0fp9qeFqcrtNPbr/uE1yeD+hWw3Lhws9I=', '2024-06-01', 43),
(685, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyNzc4NDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.LDaD+yJfhQNTLT9HD4LYrN8geUlPxvS9AiBjQwLOY+4=', '2024-06-01', 43),
(686, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyNzc4NTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QXQxxA0+rwj3dvZoOBA49LO693oLvD0jWcIkhwjrw78=', '2024-06-01', 43),
(687, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyODMzNjMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.vBU0qzuuJxeTCB+VbSizXHlIRxAUx8UTZ+pggbL64nE=', '2024-06-01', 43),
(688, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyODMzNjQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.26h1Rvot+roMLcPXsC1QJ2x89A8dBXvDKrNf6BNhNMQ=', '2024-06-01', 43),
(689, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTcyODM2NzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lFOuw7CDdwexNZhcevX/Ig6mMxcfAMYm2x8OdpeR4i0=', '2024-06-01', 43),
(690, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczMTU0MjMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.B+E4lWSk/YaJHfv7ECDCxHXlWg+iZqrXQJ7kaVC0WyY=', '2024-06-02', 43),
(691, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczMTU0MjYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xSH5oKAQRJFh22anO23yzp5SBe6kcbOy56xEPXp17h4=', '2024-06-02', 43),
(692, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczMTU0MzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.fsSt816UreSD93xhY2vs/u0nIBeiDX5DmEeJjoYXORw=', '2024-06-02', 43),
(693, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczMTU0MzYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.HUqcwYaumDrlJ+XDV/k8YjtdGRm9HfetNHss3oKMko0=', '2024-06-02', 43),
(694, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczMTU0MzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.MKUUgtmF1D6exRqUHcKVSZTRgP2KyvQcsrzcmJWZzZ4=', '2024-06-02', 43),
(695, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjI5MzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ttOG99e9YV17D1UBe/RqLYXmsc3ws8+JnIxRgq1guoE=', '2024-06-02', 43),
(696, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjI5MzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.rpZXvsO2LK84y4s8WFOOW9P2+RTPNcxaxHkJEr84au8=', '2024-06-02', 43),
(697, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjI5MzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.gIhAP9ADQCYo9g57wxMWvhUpsxjrLNwhxa6YLzCzZas=', '2024-06-02', 43),
(698, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjM5NDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.K8mgcGRONkUJGbCs9VDDHsCYXK351KG7HcN9Ltqlgk4=', '2024-06-02', 43),
(699, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjM5NTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Q5LysvohT0jNZdyn2Bxu64Zvi12ITfF3UJEMLH5UD1U=', '2024-06-02', 43),
(700, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjM5NjEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.1DYVEtz1FEnYN8n/FVdsxSiUuVZAUT47xxIimdo6Rm0=', '2024-06-02', 43),
(701, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNjUwNzEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cen9qq2h+qODeSC+mqsVAzSz543Yazw0fg+/nnz6eug=', '2024-06-02', 43),
(702, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzAwMzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.n8jdr6v+8OgxPEvukj5KTEcJ4MojsbkRGtPb6bu1vvg=', '2024-06-02', 43),
(703, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzAwNjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Gqypb9Zw11iIgmcOPIo65sO2mgITPnfTRGAT4o1dakw=', '2024-06-02', 43),
(704, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzM4MzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.CoSSy7fgx/ER+X/1DL0/vUcki4uq5Jj8gXbWXxH8Wd0=', '2024-06-03', 43),
(705, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzM4NzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.wIO8n90Gr6HhB7rY6CpzrdbLEC3cDvO0WkBFRqCGXDQ=', '2024-06-03', 43),
(706, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzM5NTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.AfpCwRo7d9H0tE85E0PFhbL2lZosPw7JRLogx3zD5dg=', '2024-06-03', 43),
(707, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzQwODAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.nSFHyRtH3vMGKYEYdqj1wSZMliYDo76BR85vTdBhos8=', '2024-06-03', 43),
(708, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzQzNjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.TzzbiTeIQqNX4HJnijWC/Sje0muu41qDrFZms0l78Gs=', '2024-06-03', 43),
(709, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzQzNjMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.k7AIwDa/qjd9ih5+/uWcWd4qbQj2BJN1DcUj6QcnmYw=', '2024-06-03', 43),
(710, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczNzQzNjQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.5SHepHeFJoWtI1A9BugGaPiJ6wUZR1zPTd/KKPeHz34=', '2024-06-03', 43),
(711, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTczODQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ojyH7y3hyyjZBvAkx4ZVFubP0729Cv86vdv+F6pxSDA=', '2024-06-03', 43),
(712, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTczODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.gQdB9O5Vjo4G82DXbp4o4BKpTsUawUZPFNtsucHJCuo=', '2024-06-03', 43),
(713, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTg4MTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.czWzIkJfuHqHT7c2ppkjxE9cymov+wOFeM2wkaWSfHw=', '2024-06-03', 43),
(714, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTg4MTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.BT9FJVZ8nZUq17rAvI31W6nJiuLQGfhB3M09vgFaHkE=', '2024-06-03', 43),
(715, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTg4MTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ZEDOMYkgIKSX9qPNeSdbgUNsIWrW/3w3zX80CzSbDXY=', '2024-06-03', 43),
(716, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTg4NDIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.jXiNFy3OicJtNl/e57hNg2KtRZp0RtMiiu9IoXjAg9U=', '2024-06-03', 43),
(717, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTczOTg4NDMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.zFzla5auUIHaNoDRDOVNPol/tc2lXia1WZYh2jSaluQ=', '2024-06-03', 43),
(718, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDA3NzYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ftIFuoVGLORxl3icpT0tr6k9MmBHyfnO02CDv+G4mMk=', '2024-06-03', 43),
(719, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDA4ODcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.RP6NQ9M/aoIbo2UY5H2p1hwG5JqBPkbbnLmrZbotIQY=', '2024-06-03', 43),
(720, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ0NzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.2XVy09VhrvcnTNy/cuuPCUXBzY/cpcFn+3qwZ+fi7R8=', '2024-06-03', 43),
(721, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ0NzQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.C4Kb+dJFgQjjUm1IyIrd0+bBu7sr5+aU4DpJCH+B2nc=', '2024-06-03', 43),
(722, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ1OTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.WekQXw9nWoTDrSirPE88JoaTE3+O9vom/zffMnTFvdE=', '2024-06-03', 43),
(723, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ1OTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xsAzRE7SqwxaVCgjgRjbdLRu8SS4AFOgC/0Ifaqz7IY=', '2024-06-03', 43),
(724, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ2MDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.jOlVzg7ZjEu5FGHSpt+su2Ez4XjuEHyfzial1NSEiU8=', '2024-06-03', 43),
(725, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ2MDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.L9nXByNPvkE7LcY29AD3Y+V9a4xpfZZZdVXuAoZ5KF4=', '2024-06-03', 43),
(726, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ5NTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.bGSLhhKtFvWv0kWNDneeWWWS5vQCK1Qe9b676Qkf1/c=', '2024-06-03', 43),
(727, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ5NTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.8BLq5Th+kWjch2lzKxEq64rnj8bP7lw3jhd1dbJxJq4=', '2024-06-03', 43),
(728, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDQ5NjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.O+t+uuhiAT0RE7KEcNES+x92AMPlgeDgGGnaQ1l7eig=', '2024-06-03', 43),
(729, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDUwMzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.LhX0ouU1fedKmxvxum72J/OH1xlmfSoJ1NpxcUzfAOw=', '2024-06-03', 43),
(730, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0MDU0MDEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.XqkhdiKFsqF+h6Sbz2q+9PTyzQsrB92y6wX1grLvsXc=', '2024-06-03', 43),
(731, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NTUzOTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.vjUDmkHUP1YAoJECvOZ6wVXT/OXHqI1Jtq6pwPj2qQY=', '2024-06-03', 43),
(732, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMxOTYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.pkNqvya1P4mpdXczrs1puzveySDpnQoeDgVvdKgPZVs=', '2024-06-04', 43),
(733, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMxOTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.DcTbsnoAuG6B3OCt6BTwArjpaGLyZEQuKai3shXQYrw=', '2024-06-04', 43),
(734, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMxOTgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.StkqOWMybBwQZjucPzfQJOK+zMxCscHJcM46mlfGeoY=', '2024-06-04', 43),
(735, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMxOTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lp1/AJi+On+62hTsglkg6GopMqEUMN9G/WX44WhS+Yo=', '2024-06-04', 43),
(736, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMyMDAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.2lkNzmdgMpa6xOX3R1q6pQcFcGGQcrgBo2CmxDNRImI=', '2024-06-04', 43),
(737, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMyMDEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.V1lJBECSzkp5lZAtMK0+IYHnW032cd+vGLXAizO6kJc=', '2024-06-04', 43),
(738, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMyMDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.BSGXxXYXXPVW8tCQlQ4IY8zx5S+3JEbH29lnDXEEOFQ=', '2024-06-04', 43),
(739, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMyMDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.BSGXxXYXXPVW8tCQlQ4IY8zx5S+3JEbH29lnDXEEOFQ=', '2024-06-04', 43),
(740, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMyMDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.sQcYV2wgyJx1AEGU6DzaSlc5kBIoVXmaDckJO7jaaeM=', '2024-06-04', 43),
(741, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjMyMjQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cOh85IiTn2u3BWr6CMcg1iXHbfQ+O/WE24qwYS01hJU=', '2024-06-04', 43),
(742, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjM0NzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.JQ/+9fNTWc3Bx+7rpgeao89cE9fb0a2oCUWSBLE2o9o=', '2024-06-04', 43),
(743, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjY3MzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.7rRq8b4SOzNQ0LUpl+yQzNE7x4ahE6vRnee9Pv8ub9A=', '2024-06-04', 43),
(744, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjY3MzYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.wEFzsRBpobhmiZ5V70dWP2aRBEESb4fbAozVPTJlPDE=', '2024-06-04', 43),
(745, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0NjY5NTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ZHgUpV56KlRT3IpChtigjDUII7bbdIJMtcmxVY4L7as=', '2024-06-04', 43),
(746, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0ODkxMzEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.aIZI9KLBPHxT4fjbnUB48kJKHNqnbrjtLCEArCouFcc=', '2024-06-04', 43),
(747, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0ODkxMzQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ISlzc9JdGtFu1hFOS6W5ZkmHrCuEqtriaMVvFN9zV0g=', '2024-06-04', 43),
(748, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0ODk0MDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.SlFylDAeBdkPYVlFMlahotzqQFqgKHcHX5bHU7Vff4k=', '2024-06-04', 43),
(749, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0OTAxNzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.OtflumQmBn251VZkPCZgf9JI74duIdDVMlPteZ/iEkE=', '2024-06-04', 43),
(750, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0OTAzNDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.CxHGoGF2QJskCaIip9VMXcSjyoexRitb6Uocb99zVeo=', '2024-06-04', 43),
(751, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc0OTQ4NDQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.AQaQtwCqmMv6xV/bfNIIyJz4p/eXklaiUIaSD3AyZzs=', '2024-06-04', 43),
(752, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cMI3+x1RsdRZ/na2+oSEVjpSa2RgKlEgD5yaW4Gc5tU=', '2024-06-05', 43),
(753, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MzEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.GaiOYrwNxdixVdwA0+3XvJKltqMf51DTSHtRQNkuwNM=', '2024-06-05', 43),
(754, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.MKq2f5a77ElYg3WD0lfRXzHmhEFXuVmgUmSZvkxx1kM=', '2024-06-05', 43),
(755, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MzcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.XGkGGnCRV8aQqhIICNDI4B+Y1urY3/egnxw6JToazsA=', '2024-06-05', 43),
(756, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.gQqxbbUMNhEesfLR0JAYVgE2iTted2BmmX+uXYYlPxo=', '2024-06-05', 43),
(757, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.gQqxbbUMNhEesfLR0JAYVgE2iTted2BmmX+uXYYlPxo=', '2024-06-05', 43),
(758, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDY2MzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.k/U5E34+6FqH+PKavKBNIu9TPeiZji1hRsawibladuo=', '2024-06-05', 43),
(759, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDcxNjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.1bgK75+sLCkoqe800tHNUjWFffYBi5liEgigdqqK//Y=', '2024-06-05', 43),
(760, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDcxNzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.9nOSBYAMm1FTAcRdVOpRLcuKQ/FcGudhAqCk4J8h6Sg=', '2024-06-05', 43),
(761, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDcxNzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.9nOSBYAMm1FTAcRdVOpRLcuKQ/FcGudhAqCk4J8h6Sg=', '2024-06-05', 43),
(762, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NDcxNzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.eyowXJ9bikotoGcbhUuQG+nP9sOyqNrL08X2zAM8Rwc=', '2024-06-05', 43),
(763, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjQzODYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.bY66bNUP0u3nGpnKLHZKvbzZ9fqdOt/wUf4f8qDptwc=', '2024-06-05', 43),
(764, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjQzODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.hYwkvkYH5fOx4s2+n86piuAvAuHEBBFZA+C0ZN9F7As=', '2024-06-05', 43),
(765, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjQzOTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.k+KYUlAxBK5LUkgdVB8Dzx1vvpW//gimaLKyvrLRJ24=', '2024-06-05', 43),
(766, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjQ3MjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.wnH1yWDL0e4Uq5jukeCSRV8DfVA/BvJAuV+KKr06JVw=', '2024-06-05', 43),
(767, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjUwMjYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.9FmEH3piKgm5T2BjQVuA3+SBrn5fDJdgzJLS9AAulaw=', '2024-06-05', 43),
(768, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjUwMjcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.yZ4FqVg66ISgzrrVkBfQqsEUuzdWnU56tEE0s9y7VCI=', '2024-06-05', 43),
(769, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjUwMjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.g4ICxF3o9XY7jTFgKKiWd8mSZACDm7CPIdt6c6lNXus=', '2024-06-05', 43),
(770, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NjY1NjYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.p+oADO4nmoTnehwO7ihHQ8LZYG62F/tRNQtOBuBptZw=', '2024-06-05', 43),
(771, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1Njc3MjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.kx5WhG5BybaSvCaiKW3ukLfLM4RDTJK8/wlZCSTxiDk=', '2024-06-05', 43),
(772, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1Njg1NTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.KQq/Ct/IH3zw98MVEaX4fEMeG17sPzws2Hec42hOccQ=', '2024-06-05', 43),
(773, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1Njg1NTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.TFIqh48muqDD1QM29NJ0FFtXkTqzajBXV4BGgs0UKMU=', '2024-06-05', 43),
(774, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1Njg4OTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cCsmDtnKuBa8hT9TXfKJHa/8Ws5aYCffmHIzkK3boSI=', '2024-06-05', 43),
(775, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NzM3ODQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.FHyqF94MlnDcl8iWyCBf5+1EpKQ36m1EpW2mbqpZlJo=', '2024-06-05', 43),
(776, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1NzU2MjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.GpwCT2cy28JgMhiWaIFuI9LRVTGZoBwDNZ9Wct2U2h8=', '2024-06-05', 43),
(777, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc1Nzk0ODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.BZZXVoBk7prFz0WdW+cPnHI4eKhT8ZBbSwLC7wskc4k=', '2024-06-05', 43),
(778, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA2ODYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.h7PzXth408dIE3SR2ISuUmP0ZU+ofZ1r4xo9OwlfTH0=', '2024-06-05', 43),
(779, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA2ODgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.KxHkjz9GxvarX564RkmMMNJce2Eld7Umy07FBxv01aM=', '2024-06-05', 43),
(780, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA2ODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cgL+cgTv+9OVowk4u1Y14SoiUGPEw89D12PZ3MzhxUU=', '2024-06-05', 43),
(781, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA2OTAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.9FwHqUaL4yLy9cMP+wGvlgDDKWOSdYsdIXB6JhxDOj8=', '2024-06-05', 43),
(782, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA2OTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.BLNNHJNHOqczJr5UdAweu6dt0tykORK6HfYVy2HH8N0=', '2024-06-05', 43),
(783, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA2OTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QWe2hBphZXWZGFp06uMt0+jLUtvZ39bRy+edDrm1TWQ=', '2024-06-05', 43),
(784, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA4MzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.6IVdSZnIy3iLZoerCrhwHjNg660iZsgJHzefM4oF/W0=', '2024-06-05', 43),
(785, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjA4NjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.CXMod91L33lb1JKxxV4GmFaJJPF7TtDa17t8pUD7Wps=', '2024-06-05', 43),
(786, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjQwOTgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.XMN52i3iwz49Ov4cKFZDH8P2rQp8QFtIeRJnaSw7zUQ=', '2024-06-05', 43),
(787, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjQwOTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.sNvj2khMchinXuGMM4oivKU1GrCAiBFQfc/Sftv76cw=', '2024-06-05', 43),
(788, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjQxMDEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.D+SVUj7wzPeVj/7J4HElmSOoN3OlKP1XKWij9LMgqDE=', '2024-06-05', 43),
(789, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjQxMDIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Ky3rwqgYmF6EzxoSzv1yWbZBP4zs9y+r2j4Z7FQ0cYU=', '2024-06-05', 43),
(790, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjY4NzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.X6gAJGNouw6tiGvzl2zILA7o6o6/hX6uLfqXqKWXhY8=', '2024-06-05', 43),
(791, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MjcwNzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.iqy4IHNhDk+nB4gxZ+b3gnY/lnCDWOFmDYA2n56WvXs=', '2024-06-05', 43),
(792, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Mjk1NTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cRLQESoCf5tFeKJLPt3wiLoCP9dYtEuA1io+sa+W3gI=', '2024-06-05', 43),
(793, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Mjk1ODgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lykRNLB7V085lehdwtNz1QeUOAZrE8Xf84RiMX21ep8=', '2024-06-05', 43),
(794, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Mjk3OTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Fr6pLI/Y66qVPpBGtk9XDdk2PBZHti+2gIb0GNCzENE=', '2024-06-05', 43),
(795, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Mjk4MTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ikcSqGt95RneFrLVdz0LcNEwO63Odz7NsHOKcjfNSyA=', '2024-06-05', 43),
(796, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Mjk4MzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.uNHQYzCwuW1njCciQUmRmZZZvBoIN1uFhiBccPRqbZE=', '2024-06-05', 43),
(797, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MzYxODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.k99eHMWa3oh1rkEY1eGJ/PUzEj778UNyqzYkU+LKMJw=', '2024-06-06', 43),
(798, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2MzczODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lPg0FUge50R96Sho42J4JrdjcyHxALsuX5/doeG3MKQ=', '2024-06-06', 43),
(799, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Mzc0NjYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.o7WJpFhaoV8gBFbmyxU+jvmwnmdXp7UxQP2vOlmfIMc=', '2024-06-06', 43),
(800, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2NTk5NzEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.4M2F01QgGNypphzznTQ47OaQ81RLgPLdz6RIvGzBBo8=', '2024-06-06', 43),
(801, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2NjAxMTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.WqyJXbD3eKTpgBXWo/TtI/A8kuQNXkdzCL4wXPeu/8k=', '2024-06-06', 43),
(802, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2NjA4OTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.MCaO4wjn2WDcJM5ydU1oyGkUD40c0XHV9rj9+bK0dHw=', '2024-06-06', 43),
(803, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2NjExNzQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.h0WZH00RarlvD6L1ynayTAQxc7QP9w7k7XNAG8laahw=', '2024-06-06', 43),
(804, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Njk2MDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.jyPh0KUYB+wOHumcMb2WdqIJNSnAe1t03pBJ8DE4M2s=', '2024-06-06', 43),
(805, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc2Njk2MDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.sjRWIQEkwYTQdkrL1+CirvZxwTalX7Dg8M6+kPvJfeI=', '2024-06-06', 43),
(806, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxMzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.r7g4WLU0Ch1/F21tOzlsgiXIB8rLdjBjdOrCoskA3ys=', '2024-06-06', 43),
(807, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxMzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Rc93uy7YcJGKcbQq0aJQDKhV5lxPaVN5OIROtst4lDA=', '2024-06-06', 43),
(808, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxMzQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.kwuiJ6COYV+UMOSkbqC5WF8hWYbfmkzpZqWFkRtmggg=', '2024-06-06', 43),
(809, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxMzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.VkovpHxClpbNw+8G5FtkuHXNUOwNzNguR9KboOsRLNs=', '2024-06-06', 43),
(810, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxMzUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.VkovpHxClpbNw+8G5FtkuHXNUOwNzNguR9KboOsRLNs=', '2024-06-06', 43),
(811, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxNDIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.KyVRnJD0hViBJZio6xxeYA+AF7VaIK5p5icB5cPTmto=', '2024-06-06', 43),
(812, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxNDMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.NclZZ3dUkX+BK8KEURC1g24YbkliweiAcQ21f6cup5Y=', '2024-06-06', 43),
(813, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxNDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9./Ea2rJ0M9b63aZPZ0UN78a7uS+OMI8EZ/dxQMmeJcsE=', '2024-06-06', 43),
(814, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxNDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.C4qQ870yB6FLqI5legRpgyc1Eh5FQh5w+jna3mzaGqw=', '2024-06-06', 43),
(815, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxNDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.WSHLFGQm6x8J1CT18j2gc1mJsexiwtnZN3GO+o6RjrU=', '2024-06-06', 43),
(816, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQxNTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.zAUrFQG03tQKLbM3DvmsKKMTcQ+j1g6AHlZnhbvaOAs=', '2024-06-06', 43),
(817, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQ0ODEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.YWdArr/isDMPm4DVvLFfXqso5p9Oh+58WWjuYz+kaaM=', '2024-06-06', 43),
(818, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDQ5ODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.yXs+uJRPsU9TcK890tZwcnsVkCJyrVVoFWcoqio+btk=', '2024-06-06', 43),
(819, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDUxNDIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.IYKqBdD+6M6Un3/ZuwnHJbuNfjYk+w4L8exxkmpFO/c=', '2024-06-06', 43),
(820, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDUzNjUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.E0vFMgFHodrgPmSWf5Az8GAn/CJeNiMYPhyZNurtr2E=', '2024-06-06', 43),
(821, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDY0ODQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.roBUrHJrVkmeLd4v+Q1i8bTumf9ANxFo4KYxv9YWoic=', '2024-06-06', 43),
(822, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDc2NTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.POrFMCNnWRBniW1kjvEAdXBxvS1JNk90yNIFbnMqaEQ=', '2024-06-06', 43),
(823, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MDc4MTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.v8OJuUQbqA/Oppvs/gbZsKlyU01P9HMnM8PVJtRZ9C4=', '2024-06-06', 43),
(824, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjA3ODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.DHiG82EngwpxBNjR8gIMF9vVWlihFs8GkO9USjHcEAg=', '2024-06-07', 43),
(825, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjA3OTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.OJZvq1Rzno2k9M+z8U9yJnC4Ct4X48dyLkBVK6jzp50=', '2024-06-07', 43),
(826, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjEwODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.7842NUHyiLxOUXEC7aeNjTiW7DpH3Y9AwgeWEqDmtIo=', '2024-06-07', 43),
(827, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjExNTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.op9pxZkNXYZVrHYqTO14IGR0u/lXXbH1uDh/1UhXLYo=', '2024-06-07', 43),
(828, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjE0MzMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.WUhG6lQ4+oZMkiaKGvmv1NmVnPWW+2zrW81ooUtJ3xQ=', '2024-06-07', 43),
(829, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjE1MjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Iiw+xr7BFoZ8xxe+/+3X7jIo4hFgZUUcfU8bF3JGHWU=', '2024-06-07', 43),
(830, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjE1MjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Iiw+xr7BFoZ8xxe+/+3X7jIo4hFgZUUcfU8bF3JGHWU=', '2024-06-07', 43),
(831, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjIxNTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.YgS4pjNr3/YmWw0Xael7vv7+E4Nt0t/GuLOmznbx79Y=', '2024-06-07', 43),
(832, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjIxNTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lybWgyusPIJ8C0thwb2KsmKpRh4XmD9ibQrRDUUMr5k=', '2024-06-07', 43),
(833, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjI1NjQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.5UpmeAJ9JsNeNed6TdhPPtyYheI+C6S2ZDnx4IhUm9o=', '2024-06-07', 43),
(834, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjQ3MTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.5VwEodE9WAuF0yYJsJtTzCj6Xse+HF/lF1Ez7TuhSVI=', '2024-06-07', 43),
(835, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MjQ3MjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.DSQzkQvoP7U3fJsGaB9AR67BkOVLjPRDgxvxJHxxyXk=', '2024-06-07', 43),
(836, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MzkyNzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.J8jSI7axgcfdREnnvoZk7FscHpX6VRJPuCKzgO0KIiQ=', '2024-06-07', 43),
(837, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3MzkyODAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.L/4pgRQ3wUQMOkKW/FM5FSUxunin0jPqPr24t5a3MIg=', '2024-06-07', 43),
(838, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3Mzk0MzYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.L23jFyGHY4Q2C+DmJwbdbU6aLO50x7IREnI/VajbPiw=', '2024-06-07', 43),
(839, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3NTIyOTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ku38SOQ3lx81ZObuVCHM6pQo9CP3kdgLdEhGaIdM/OE=', '2024-06-07', 43),
(840, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3NTIyOTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.VI6q5SMSzGF2iykf+EsmSHkfsw6iYDBHuVGEnk2Zv1E=', '2024-06-07', 43),
(841, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc3NTM1MDAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.u76/76kGm9mSf9ilFTYvjrITGXnuWWjhnkwCvR2r0SU=', '2024-06-07', 43),
(842, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDM3OTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.kD217zrovJXRgdCd8l79+HNvQ5YnrRLjHDMVCMgJLkM=', '2024-06-07', 43),
(843, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDM3OTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.P29gkMVfYQTos0MQBsAei/Wr1xLCslyB06aYJJrxhFE=', '2024-06-07', 43),
(844, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDM3OTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.P29gkMVfYQTos0MQBsAei/Wr1xLCslyB06aYJJrxhFE=', '2024-06-07', 43),
(845, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDM3OTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.GjnPyIGeJA62Y2wvFAwTa8X7J5x58ExXG4migQpw4wc=', '2024-06-07', 43),
(846, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDM3OTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ilNHL+y50gym20H0B3VO0vYKSnd9RhVheYnoC/4aHXU=', '2024-06-07', 43),
(847, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDM3OTQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ilNHL+y50gym20H0B3VO0vYKSnd9RhVheYnoC/4aHXU=', '2024-06-07', 43),
(848, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDU0ODQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Kwu/T1TwdL7NWe9WOSDQbqZdnzoI6HZhYTbPOV0Io7g=', '2024-06-08', 43),
(849, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDYwMTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.oCAyoTUd93xV8cvlkM51ZNwE5eaoghgBEnZ8z8amF9U=', '2024-06-08', 43),
(850, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDYwMTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.U4i3u3iatdJiNGAqV8JLFIFf78UIARZJ0p08FiHI3nA=', '2024-06-08', 43),
(851, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDYwMTgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.1UIv4j0pRF6uY/QOtxYJvlTYU6y7uboassMU7RY78Ug=', '2024-06-08', 43),
(852, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDYwMTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.sUgw1EdK2l1D5mtUovjOLiVuN4hNPaGhm3qyyXgFbLE=', '2024-06-08', 43),
(853, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDYwMTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.sUgw1EdK2l1D5mtUovjOLiVuN4hNPaGhm3qyyXgFbLE=', '2024-06-08', 43),
(854, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDYwMjAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.4CvwY9byin/cV6a+3AwpA7uNQqLvsI+JTdAvrgP8YOQ=', '2024-06-08', 43),
(855, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MDY0MTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.YYh59xiRXj5787vd0moP20cKkRR/h64vR5xEFO+GsH8=', '2024-06-08', 43),
(856, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzNzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.DfEsnY6F1ddLABfqWap5F4OhqygRRMyX2hhh/vP8BbQ=', '2024-06-08', 43),
(857, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.sObvC0oIR/6vIodDeOUPwfGFi6fB9V40e2PQVHEFVBQ=', '2024-06-08', 43),
(858, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.eGoyi7CNpGcAcsxTLDAQqAH5PGEDrFIx4LpcnPAgOZg=', '2024-06-08', 43),
(859, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.RgycLxXvD/re6UTWv23RMx0aYWdjokbNv5xDxmhKYxY=', '2024-06-08', 43),
(860, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.0Ven0izg4esuSgykJ3TW7itYcvEyUjEIZ8MObIVtmWo=', '2024-06-08', 43),
(861, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.FzXf+v6eZKAdRi2bV4XJulTu5pCzT0nrLxPyn2toTpw=', '2024-06-08', 43),
(862, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.PEsu/fz8IavkqTpYRt1rgV86w5Jl+KAcxZ5sCriSYwA=', '2024-06-08', 43),
(863, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.PEsu/fz8IavkqTpYRt1rgV86w5Jl+KAcxZ5sCriSYwA=', '2024-06-08', 43),
(864, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.UULjmLZzi3Nis4ySu7Z3m/uIOCcRcBZFhbHpogISCgE=', '2024-06-08', 43),
(865, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.jgc/BwmMoqiUFO3dhAUlX+uJufrJq9tLqJxSbizRxps=', '2024-06-08', 43),
(866, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.yFppFOJoIdOCPkVr+NtBtdYucSDYhxWZXTaIITANBJo=', '2024-06-08', 43),
(867, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.lqZDJGneV6HgdDtTsJtWpzcdyzqr+lbTNguFIYZexB4=', '2024-06-08', 43),
(868, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTAzOTAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ZE1VZWM2jgIRgLhiM15sHQKBpo20Q5rGHLPiCBxa318=', '2024-06-08', 43),
(869, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc4MTA0MjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.t0ux5hSWH9adwtAsWVBOttq7O6sEtaribntTz+HrTl4=', '2024-06-08', 43),
(870, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc5MDc2NjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.viwEleJkB/JiwrlJxjYjUog5oykp+BW0KeCvi8EBieU=', '2024-06-09', 43),
(871, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc5MDc2NzAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.UmscvgiJtAmq07Tt4x3Xn9OAPguEj51q5UEITw2bkpI=', '2024-06-09', 43),
(872, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc5MzA3NDksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.i+3zp8Vq3hOs51lrd7JJzfFAF2Zk2mqj5USPuN8jqwg=', '2024-06-09', 43),
(873, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc5MzA3NTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.2KpJT+jjZbfVZqMlFBTtErjigR/ZmV3eWuTeu5Mg1U0=', '2024-06-09', 43),
(874, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTc5MzA3NTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.ZRtqHNzNSnmtwEzeee7vK1PwD1xyRIcfp/CArb/Nt28=', '2024-06-09', 43),
(875, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMDc1ODQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.2LB2mSnE840T542PZYPjxeYuValwxY54c3dll7CIusw=', '2024-06-10', 43),
(876, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMDc1ODUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.emIeeiUdzLIjHGRPgIjHgQ6lQNJ4Si9QPI+HEHx/QzE=', '2024-06-10', 43),
(877, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMDc3NDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.wID8hbCESA0UmuPmm6J9C2PoSOLmOygSUYyYKplRi5A=', '2024-06-10', 43),
(878, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMDc3NDksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.FoLatuNPtM93q6FqmM6ObSg0QFGSBUiwpOZpLhQhZXk=', '2024-06-10', 43),
(879, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMDc3NTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.+w197o6TyQ3WwVwiiDEweKNzigYUMxiTfRG5X7ug/+k=', '2024-06-10', 43),
(880, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMTE1MTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.xWyPzhY7aHJI45rDhZFdcS1jD/7eSm4DM/MazCv4scE=', '2024-06-10', 43),
(881, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwMTE1MTYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.CrFD/f0/p8UY7LmBF9lVkC4vMdUwAZRwJqas+ivuuWY=', '2024-06-10', 43),
(882, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTE0MzcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.8oeU1uZJLm4YSZMC9MJ+WWBq5clLPLUh7LpYd3eNU0U=', '2024-06-10', 43),
(883, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTE0MzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.P+BBpOqZ2xkfxadr6kb+4dUZWZNuxunnHKoagVo6Phg=', '2024-06-10', 43),
(884, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTE0MzksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Dy6g0/YeTm877Oa4Bsf30NMz/1SyVQVy5QU+XCNNeBE=', '2024-06-10', 43),
(885, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTE0NDAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.frHTJ0AWBdkSOjJmtgkDpN7zHbPXSbFvlboVNEESCtw=', '2024-06-10', 43),
(886, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTE0NTYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.XmXa6EoeMf0zC8AgGKw7V3oQYDQKa8+KWJVutPJnfHg=', '2024-06-10', 43),
(887, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTM3NDMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.YuXXiPqhrXfopLzI/DnA8Ru7LszDSd52CXOQLWt38DM=', '2024-06-10', 43),
(888, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTM3NDQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9./4xVX7xBHVwjPGTPK0498qRwDhtlEc79aTM4x/5SwRk=', '2024-06-10', 43),
(889, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTU2NDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.wICDWRJFovLgP4pGOmfX3xcd5wV3JbbRtvEcEO7+kSg=', '2024-06-10', 43),
(890, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTU5MjgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.yZxioDI3UUgB2FIKDNzUeWnHWrrFHeJUuQzsXFUbMSU=', '2024-06-10', 43),
(891, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTkwNTEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.FDr7SbxbWa83dGmQQDLyICAwaG9NHDpWJCy75roq3SA=', '2024-06-10', 43),
(892, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTkwNTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.D8cazTa7ANU7vxCa26wdVczeveq8Phhb6K2WFfn7pzg=', '2024-06-10', 43),
(893, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNTkwOTIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QWddTEtrSCkruEyj5YtLfRblOO3CuI0BYjzr8xUrVe8=', '2024-06-10', 43),
(894, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNjU4NjAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.B9mKrjYZEHuCTd+YCfIqztIjYdlCYVv7XqfTGDiC3Es=', '2024-06-11', 43),
(895, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNzg2NzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QmEgpNnv0vKyoFfJG5Y31QN73U//VBdw7xLkC4WyrR4=', '2024-06-11', 43),
(896, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNzg2ODAsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.fdRlcuOtx1mgmDS3fXPUZutK+CvTLCrs0RWvwqrasS8=', '2024-06-11', 43),
(897, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwNzg2ODIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.JUcxOD4+7L2c17G65PC2Gi9Q2ey2sAd9hSQsfhJuRd8=', '2024-06-11', 43),
(898, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwODAyNDksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.3dQTlc+/zlxamZffiCXLvCRXro8CK1WjD40yXpMX7Rc=', '2024-06-11', 43),
(899, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwODA4MTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.5XP8hVQX+6HJZyaC60bn8pYDRf8/sbaqU7AjWInhZos=', '2024-06-11', 43),
(900, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwODEzMTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.EYO2HmO7x3tyzzeQgUCHM/2G1N0G+MUeZypp3ZxT9xw=', '2024-06-11', 43),
(901, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwODg4NDUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.FPsAsZ6ZBj/RuN3rhNZyXJn/kyorI8saPI2xTawfscw=', '2024-06-11', 43),
(902, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwODg4NDYsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.qnFjrx2sJ+OYyMMYrPyaVv5WzvxJlXYCZdXYuwLyVmo=', '2024-06-11', 43),
(903, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgwODkzNjksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QbZxwgVxyUSnr54mJbhZwfD/W6kOYFz02Pg46HCLQDY=', '2024-06-11', 43),
(904, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgxMzUyMDcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.JchlIzr5+O1sadvSDuhHHreSCyzV0PpRQgf9EH0xicY=', '2024-06-11', 43),
(905, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgxMzUyMDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.RciOQVuIdTCMhJ5R0JRdz4uNsKsri4o6o4s7S9/0aTs=', '2024-06-11', 43),
(906, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgxMzUyMTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.FvNRvQNar/7X9KFiEY5qh3W5L34ZX8uPGDzlhJ4Dw+Y=', '2024-06-11', 43),
(907, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgxMzUyMzIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.WLAXVjA2wthRD/xMfYa3bF6p4qQVDx4Pw2+ic6Jg1sk=', '2024-06-11', 43),
(908, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgxMzUyNTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.9hRkZD1abZd1BGUbMwtCkKltAb0KBswEnIKCd4mJ1Ak=', '2024-06-11', 43),
(909, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgxMzUzMDMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.TOf7TjigoyPw2YcSKYSvfSws2iex40N10Vgkh9UBKqk=', '2024-06-11', 43),
(910, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgyMDkxNjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.cN7Q0sAzxTgAYQT2RyiXaKEG95kaV1itiM4fhLcdIKQ=', '2024-06-12', 43),
(911, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgyNjQ4NTcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.QSzVtJNkOGXnX9xpY5iJIz0JRB9FLZtSuI4J9vYbspk=', '2024-06-13', 43),
(912, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgyNzUyNjMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.T779XbL3OJUGXGoUn19JsJ4Lr0Z2j6NqcxOvSoKjfek=', '2024-06-13', 43);
INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(913, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgyODA2NDMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.M/lQgbn9NFSkAiONX9sSSO26qcgAfbNGXqy0N1P7hUA=', '2024-06-13', 43),
(914, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgyODk3ODksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.65ZAzrNKuAuMyv2hULQGKBlC8Z7KvXKuNj8+jiDLzQo=', '2024-06-13', 43),
(915, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgyOTM5ODMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.AFuKYLpSo1xR7c1/EapaY4VersDq9rZ+Hbcw9bc8O9c=', '2024-06-13', 43),
(916, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgzNTU5MTksInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.2ATptpJbs7kQe/6yIxU7ZXq2lZE0K8cO2RiBLHbWt20=', '2024-06-14', 43),
(917, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgzNjEyNzgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.BGl950wG+i2tHQLJz87UdEGcsOY4jZVOoVoJXPWdcUQ=', '2024-06-14', 43),
(918, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgzNzkxMjcsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.bWyy65pmu4tRB31z1RU5wxs1axrQMFrppSp0TnMdBxQ=', '2024-06-14', 43),
(919, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgzODA4MTMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.rmqnQGsge7jYrCkjLFS3je0qAHxhIZ7n8mChy/7xuSM=', '2024-06-14', 43),
(920, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTgzODI2MjEsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.S4knbD1CO3NtXO9KwFMv7ImMz9LoOQ0ZhFgF3ZZGDDc=', '2024-06-14', 43),
(921, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTg2MTM1NTUsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.Fe8QcQYX+KcazmGyowcFljd0pLj+RVyGsFoVSC8T0mY=', '2024-06-17', 43),
(922, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTg2MjE4NjIsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.46jISop3Cn+2RtegnaS79/z1MYwNAMHVMXHvny3lhy4=', '2024-06-17', 43),
(923, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTg2Mzk0MDQsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.7bvPo8KhnbCrlSrJQMvyrpYPlSwHsLN0R/BaKafEMvo=', '2024-06-17', 43),
(924, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTg4MTIyNjMsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.mGcC/fFzEI1HKOdvulu/iUZEteHJV/gxgZH6fqic3ts=', '2024-06-19', 43),
(925, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MTg4MTU5MDgsInVpZCI6IjQzIiwiZW1haWwiOiJjbGV1YmVyQGdtYWlsLmNvbSJ9.SaksKgoO3gxMTe+/lqfoXnA1bEuv6Fakfrlb9oaOHdo=', '2024-06-19', 43);

-- --------------------------------------------------------

--
-- Estrutura da tabela `vacation`
--

DROP TABLE IF EXISTS `vacation`;
CREATE TABLE IF NOT EXISTS `vacation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `year` int NOT NULL,
  `initial_date` date NOT NULL,
  `end_date` date NOT NULL,
  `id_employee` int NOT NULL,
  `status` varchar(15) NOT NULL,
  `obs` varchar(120) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `vacation`
--

INSERT INTO `vacation` (`id`, `year`, `initial_date`, `end_date`, `id_employee`, `status`, `obs`) VALUES
(1, 0, '2024-07-12', '2024-08-05', 2, 'Pendente', 'Valeu'),
(2, 2024, '2024-07-16', '2024-08-09', 2, 'Pendente', 'Valeu'),
(3, 2024, '2024-07-12', '2024-08-05', 2, 'Pendente', 'Valeu'),
(4, 2024, '2024-07-12', '2024-08-05', 2, 'Pendente', 'Valeu'),
(5, 2024, '2024-07-12', '2024-08-05', 2, 'Pendente', 'Valeu'),
(7, 2024, '2024-07-12', '2024-08-05', 2, 'Pendente', 'Valeu'),
(9, 2024, '2024-07-12', '2024-08-05', 2, 'Pendente', 'Valeu');

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `academic_education`
--
ALTER TABLE `academic_education`
  ADD CONSTRAINT `academic_education_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `academic_education_ibfk_2` FOREIGN KEY (`id_education`) REFERENCES `education` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `academic_education_ibfk_3` FOREIGN KEY (`id_academic_degree`) REFERENCES `academic_degree` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `assessment`
--
ALTER TABLE `assessment`
  ADD CONSTRAINT `assessment_ibfk_1` FOREIGN KEY (`id_assessment_type`) REFERENCES `assessment_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`id_period`) REFERENCES `period` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `capacitation`
--
ALTER TABLE `capacitation`
  ADD CONSTRAINT `capacitation_ibfk_1` FOREIGN KEY (`id_training`) REFERENCES `training` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `capacitation_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `career`
--
ALTER TABLE `career`
  ADD CONSTRAINT `career_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `contract`
--
ALTER TABLE `contract`
  ADD CONSTRAINT `contract_ibfk_1` FOREIGN KEY (`id_contract_type`) REFERENCES `contract_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `day_off`
--
ALTER TABLE `day_off`
  ADD CONSTRAINT `day_off_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `day_off_ibfk_2` FOREIGN KEY (`id_day_off_type`) REFERENCES `day_off_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `deduction_processing`
--
ALTER TABLE `deduction_processing`
  ADD CONSTRAINT `deduction_processing_ibfk_1` FOREIGN KEY (`id_deduction`) REFERENCES `deduction` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `deduction_processing_ibfk_3` FOREIGN KEY (`id_office`) REFERENCES `office` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `deduction_processing_ibfk_4` FOREIGN KEY (`id_processing`) REFERENCES `processing` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `deduction_processing_ibfk_5` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `dependent`
--
ALTER TABLE `dependent`
  ADD CONSTRAINT `dependent_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`id_contract`) REFERENCES `contract` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `employee_ibfk_2` FOREIGN KEY (`id_department`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `fixed_deduction`
--
ALTER TABLE `fixed_deduction`
  ADD CONSTRAINT `fixed_deduction_ibfk_1` FOREIGN KEY (`id_deduction`) REFERENCES `deduction` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fixed_deduction_ibfk_3` FOREIGN KEY (`id_measurement_unit_deduction`) REFERENCES `measurement_unit_deduction` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fixed_deduction_ibfk_4` FOREIGN KEY (`id_office`) REFERENCES `office` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `fixed_salary`
--
ALTER TABLE `fixed_salary`
  ADD CONSTRAINT `fixed_salary_ibfk_2` FOREIGN KEY (`id_office`) REFERENCES `office` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fixed_salary_ibfk_3` FOREIGN KEY (`id_salary`) REFERENCES `salary` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fixed_salary_ibfk_4` FOREIGN KEY (`id_measutement_unit_salary`) REFERENCES `measurement_unit_salary` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `license`
--
ALTER TABLE `license`
  ADD CONSTRAINT `license_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `license_ibfk_2` FOREIGN KEY (`id_license_type`) REFERENCES `license_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `loan`
--
ALTER TABLE `loan`
  ADD CONSTRAINT `loan_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `loan_ibfk_2` FOREIGN KEY (`id_type_loan`) REFERENCES `loan_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `loan_payment`
--
ALTER TABLE `loan_payment`
  ADD CONSTRAINT `loan_payment_ibfk_1` FOREIGN KEY (`id_loan`) REFERENCES `loan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `other_formation`
--
ALTER TABLE `other_formation`
  ADD CONSTRAINT `other_formation_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `performance`
--
ALTER TABLE `performance`
  ADD CONSTRAINT `performance_ibfk_1` FOREIGN KEY (`id_assessment`) REFERENCES `assessment` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `performance_ibfk_2` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `period`
--
ALTER TABLE `period`
  ADD CONSTRAINT `period_ibfk_1` FOREIGN KEY (`id_period_type`) REFERENCES `period_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`id_permition`) REFERENCES `permition` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `remuneration`
--
ALTER TABLE `remuneration`
  ADD CONSTRAINT `remuneration_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `remuneration_ibfk_2` FOREIGN KEY (`id_office`) REFERENCES `office` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `remuneration_ibfk_3` FOREIGN KEY (`id_processing`) REFERENCES `processing` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `remuneration_ibfk_4` FOREIGN KEY (`id_salary`) REFERENCES `salary` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `spoken_language`
--
ALTER TABLE `spoken_language`
  ADD CONSTRAINT `spoken_language_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `spoken_language_ibfk_2` FOREIGN KEY (`id_language`) REFERENCES `language` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_ibfk_1` FOREIGN KEY (`id_enterprise`) REFERENCES `enterprise` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `training`
--
ALTER TABLE `training`
  ADD CONSTRAINT `training_ibfk_1` FOREIGN KEY (`id_training_type`) REFERENCES `training_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id_store`) REFERENCES `store` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `user_connected`
--
ALTER TABLE `user_connected`
  ADD CONSTRAINT `user_connected_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `vacation`
--
ALTER TABLE `vacation`
  ADD CONSTRAINT `vacation_ibfk_1` FOREIGN KEY (`id_employee`) REFERENCES `employee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
